$(document).ready(function () {

	// Modal dialog appear when user disconnect
    $("#dialog-disconnect-user").dialog({
        resizable: false,
        height: "auto",
        width: 400,
        autoOpen: false,
        buttons: {
            "Déconnecter": function () {
                window.location.href = "/signout";
            },
            "Annuler": function () {
                $(this).dialog("close");
            }
        }
    });
    
    $("#dialog-diactivate-service").dialog({
        resizable: false,
        height: "auto",
        width: 400,
        autoOpen: false,
        buttons: {
            "Supprimer": function () {
                window.location.href = "/merchant/service/delete";
            },
            "Annuler": function () {
                $(this).dialog("close");
            }
        }
    });
    
    /*
    $('#btn-disconnect-user').click(function () {
        $("#dialog-disconnect-user").dialog("open");
    });
    */

    $('#btn-disconnect-user').click(app.disconnect);

});


