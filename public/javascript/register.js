//////////
//GLOBAL//
//////////

// External paths
var JSON_PATH_CITIES = "/data/canada_cities.json";
var JSON_PATH_PROVINCES= "/data/canada_provinces.json";
// Towns data
var cities_data = "";

////////
//MAIN//
////////

$(document).ready(function () 
{
    
    // DatePicker from jquery-ui
    $(".date-picker").datepicker({dateFormat: "yy-mm-dd" }); 
    $(".date-picker").datepicker( "option", "changeYear", true );    
    $(".date-picker").datepicker( "option", "yearRange", "1900:c" );
    
    $("#formUser").on("submit", function(event){
       if (validateForm(this) === false) {
           event.preventDefault();
       }
       console.log("résultat de validation du formulaire est:");
       console.log(validateForm(this));
    });

    $.getJSON(JSON_PATH_PROVINCES, function (data) {
        populateSelectProvince($(".slc_prov"), data.provinces);
    });

            
    $.getJSON(JSON_PATH_CITIES, function (data) {
        cities_data = data;
    });
    
    $(document).on('change', '.slc_prov', function () {        
        updateSelectCity(this, cities_data);
    });
    
    $( "input[name='same_adresse']" ).on("change", adresseRepeat);
    $( "input[name='same_phone']" ).on("change", phoneRepeat);
    $( "input[name='same_mail']" ).on("change", mailRepeat);
});

////////////
//FUNCTION//
////////////

function populateSelectProvince(select, array_data) {
    if (select !== "null") {
        for (i = 0; i < array_data.length; i++) {
            select.append("<option value=" + array_data[i]["name"] + ">" + array_data[i]["name"] + "</option>");
        }
    }
}

function updateSelectCity(select, array_data){
    var $select = $(select);
    var $city = $($select.data('target'));

    province = $select.val();
    $city.empty();
    
    if (province !== "null") {
        $city.prop("disabled", false);
        for (i = 0; i< array_data[province].length; i++) {
            $city.append("<option value="+array_data[province][i]+">"+array_data[province][i]+"</option>");
        }
    } else {
        $city.prop("disabled", true);
        $city.append("<option value='null'>Ville</option>");
    }
}

function validateForm(form){
    var $errors = $("#errors");
    var mailOk = verifyMail(form.mail, form.mailConfirm, $errors);
    var zipCodeOk = zipCodeValidation(form.zip_code, $errors);
    var passWordOk = verifyPassWord(form.password, form.passwordConfirm, $errors);
    var usernameOk = verifyUsername(form.username, $errors);
    //var birthdayOk = verifyDate(form.birthdate, $errors);
    var birthdayOk = true;
    console.log(mailOk, zipCodeOk, passWordOk, usernameOk, birthdayOk);
    return (mailOk && zipCodeOk && passWordOk && usernameOk && birthdayOk);    
}

/// Source: https://itx-technologies.com/blog/376-validation-dun-code-postal-en-javascript
// valide un code postal canadien
// input: code postal
// output: vrai ou faux
function zipCodeValidation(zipCode, $errors) { 
    var entry=zipCode.value;
    var length =entry.length;
    if(length!==6) {
        app.displayError("<p>Code postal invalide !</p>");
        return false;
    }
    // au cas où le code postale été entré en minuscules
    entry = entry.toUpperCase();
    
    // Les valeurs permises pour chaque caractère
    var str1='ABCEGHJKLMNPRSTVXY';
    var str2=str1+'WZ';
    var numbers='0123456789';
    
    // compare, une par une, les valeurs entrées à celles permises
    if(str1.indexOf(entry.charAt(0))<0){
        app.displayError("<p>Code postal invalide !</p>");
        return false;
    }
    if(numbers.indexOf(entry.charAt(1))<0){
        app.displayError("<p>Code postal invalide !</p>");
        return false; 
    }
    if(str2.indexOf(entry.charAt(2))<0){
        app.displayError("<p>Code postal invalide !</p>");
        return false;
    }
    if(numbers.indexOf(entry.charAt(3))<0){
        app.displayError("<p>Code postal invalide !</p>");
        return false;
    }
    if(str2.indexOf(entry.charAt(4))<0){
        app.displayError("<p>Code postal invalide !</p>");
        return false;
    }
    if(numbers.indexOf(entry.charAt(5))<0){
        //surligne(entry, true);
        app.displayError("<p>Code postal invalide !</p>");
        return false;
    }
      
    // le code postal est valide !
    return true;
}

function verifyMail(mail, mailConfirm, $errors){
    if ( mail.value === mailConfirm.value ){
        return true;
    }
    // TO DO
    // validation si l'email existe déjà ou non
    app.displayError("<p>Vous n'avez pas saisi le même courriel !</p>");
    return false;
}

function verifyPassWord(password, passwordConfirm, $errors){
    if (( password.value === passwordConfirm.value )&&(password.value.length>5 && password.value.length<13)){
        return true;
    }

    app.displayError("<p>Vous n'avez pas saisi le même mot de passe ou il ne contient pas entre 6 et 12 caractères !</p>");
    return false;
}

//source: https://openclassrooms.com/forum/sujet/validation-d-un-champ-date-en-javascript
function verifyDate(birthday, $errors){
    var str= birthday.value, month, date, year;
    var rgx=/(\d(?=\D)|\d\d)[\D]*(\d(?=\D)|\d\d)[\D]*([1-9][0-9]{3})/;
    // A replace method with an anonymous function to define month, date and year
    str.replace(rgx,
        // This function arguments are defined by the system
        // the matched string and the sub-patterns delimited by brackets (corresponds to $0, $1 $2...)
        function(allPat,b1,b2,b3){
            month=parseInt(b2,10);date=parseInt(b1,10);year=+b3;
            console.log(date, month, year);
        }
                );
    if (isNaN(month)||isNaN(date)||isNaN(year)) {
        app.displayError("<p>S'il vous plait, entrez une date valide avec des chiffres, séparateurs ou laissez sans séparateurs (J/M/AAAA or JJMMAAAA</p>");
        return false;
    }
    var dat=new Date(year, month-1, date);
    console.log(dat);
    console.log(dat.getDay(), dat.getMonth(), dat.getYear());
    console.log(dat.getMonth()!==month-1);
    if (dat.getMonth()!==month-1) {
        app.displayError('<p>Désolé, cette date parait non valide !</p>');
        return false;
    } else {
        return true;
    }
}

function verifyUsername (username, $errors){
    if (( username.value.length < 4 )||(username.value.length > 25 )){
        app.displayError("<p>Veuillez entrer un nom d'utilisateur valide!</p>");
        return false;
    } else {
        return true;
    }
    // TO DO
    // validation si username existe déjà ou non
    return false;
}

function adresseRepeat(){
    console.log("On est là pour afficher les blocks:");
    form = document.forms[0];
    var $adresse = $("#adresse_repeat");
    var $answer_adresse = $("#answer_adresse");
    var adresse_perso = form.adresse.value;
    var street_perso = form.street.value;
    var app_perso = form.app.value;
    var zip_code_perso = form.zip_code.value;
    var province_perso = form.province.value;
    var town_perso = form.town.value;
    if (form.same_adresse[0].checked){
        form.business_adresse.value = adresse_perso;
        form.business_street.value = street_perso;
        form.business_app.value = app_perso;
        form.business_zip_code.value = zip_code_perso;
        form.business_province.value = province_perso;
        updateSelectCity(form.business_province, cities_data);
        form.business_town.value = town_perso;
        $answer_adresse.css("display","none");
        $adresse.show();
    };
    if (form.same_adresse[1].checked){
        $answer_adresse.css("display","none");
        $adresse.show();
    };
    
}

 function phoneRepeat(){
    form = document.forms[0];
    var $phone = $("#phone_repeat");
    var phone_perso = form.phone.value;
    var $answer_phone = $("#answer_phone");
 
    if (form.same_phone[0].checked){
        form.business_phone.value = phone_perso;
        $answer_phone.css("display","none");
        $phone.show();
    };
    if (form.same_phone[1].checked){
        $answer_phone.css("display","none");
        $phone.show();
    };
}  

function mailRepeat(){
    form = document.forms[0];
    var $mail = $("#mail_repeat");
    var mail_perso = form.mail.value;
    var $answer_mail = $("#answer_mail");
 
    if (form.same_mail[0].checked){
        form.business_mail.value = mail_perso;
        $answer_mail.css("display","none");
        $mail.show();
    };
    if (form.same_mail[1].checked){
        $answer_mail.css("display","none");
        $mail.show();
    };
}  