function getMaxDate(nbYear)
{
    var d = new Date(); 
    var year = d.getFullYear(); 
    var month = d.getMonth(); 
    var day = d.getDate(); 
    return new Date(year + nbYear, month, day); 
}

function getParam(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function StatusController()
{
	var self = {};

	var $row = $('#layout-message .row');
	var $col = $('<div class="col-xs-12"></div>');
	var $alert = $('<div></div>');

	$col.html($alert);

	self.display = function(status)
	{
		if (status.type == 'success') {
			$alert.addClass('alert-success');
		}

		if (status.type == 'danger') {
			$alert.addClass('alert-danger');
		}

		if (status.message !== undefined) {
			$alert.html("<p>"+status.message+"</p>");
		}

		$row.html($col);

		setTimeout(function(){
			self.init();
		}, 5000);
	}

	self.init = function()
	{
		$row.html('');
		$alert.attr('class', 'alert').html('');
	}

	self.init();

	return self;
}

function AppEvent(obj)
{
	var events = {};

	obj.trigger = function(eName, data) {
		if (events[eName] !== undefined) {
			for (var i = 0; i < events[eName].length; i++) {
				events[eName][i](data);
			}
		}
	}

	obj.on = function(eName, action) {
		if (events[eName] === undefined) {
			events[eName] = [];
		}
		events[eName].push(action);
	}

	return obj;
}

function ModalContainer(selector)
{
	var self = $(selector);

	self.title 		= self.find('.modal-title');
	self.body 		= self.find('.modal-body');
	self.content 	= self.find('.modal-content');
	self.confirm 	= self.find('.modal-confirm');
	self.close 		= self.find('.modal-close');

	return self;
}

function ModalController()
{
	var self = this;

	var $modal 	= undefined;
	var $confirm 	= new ModalContainer('#modal-confirm');
	var $basic 		= new ModalContainer('#modal-basic');
	var $calendar 	= new ModalContainer('#modal-calendar');

	self.loading = function()
	{
		$modal.body.html('<div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div></div>');
	}

	self.success = function(msg)
	{
		$modal.body.html('<p id="errors" class="alert alert-danger">'+msg+'</p>');
	}

	self.error = function(msg)
	{	
		$modal.body.html('<p id="errors" class="alert alert-danger">'+msg+'</p>');
	}

	self.calendar = function(options)
	{	
		$modal = $calendar;

		self.setOptions(options);

		$modal.modal();
	}
	
	self.confirm = function(options)
	{	
		$modal = $confirm;

		self.setOptions(options);

		$modal.modal();
	}

	self.setOptions = function(options)
	{
		if (options.title !== undefined) {
			self.setTitle(options.title);
		}

		if (options.calendar !== undefined) {
			self.setCalendar(options.calendar);
		}

		if (options.body !== undefined) {
			self.setBody(options.body);
		}

		if (options.btnConfirm !== undefined) {
			self.setBtnConfirm(options.btnConfirm);
		}

		if (typeof options.onConfirm === "function") {
			$modal.confirm.one('click', options.onConfirm);
		}

		if (typeof options.onShow === "function") {
			$modal.one("shown.bs.modal", options.onShow);
		}
	}

	self.close = function()
	{
		$modal.modal('hide');
	}

	self.setBtnConfirm = function(btnConfirm)
	{
		if (typeof btnConfirm === 'string') {
			$modal.confirm.html(btnConfirm);
		} else {
			$modal.confirm.html('Confirmer');
		}
	}

	self.setCalendar = function(calendar)
	{
		if (typeof calendar === 'string') {
			$modal.content.html(calendar);
		} else {
			$modal.content.html('Invalid calendar format');
		}
	}

	self.setTitle = function(title)
	{
		if (typeof title === 'string') {
			$modal.title.html(title);
		} else {
			$modal.title.html('Modal Body');
		}
	}

	self.setBody = function(body)
	{
		if (typeof body === 'string') {
			$modal.body.html(body);
		} else {
			$modal.body.html('Modal Title');
		}
	}

	return self;
}

function Request() 
{
    var self = this;

    self.post = function(route, data, callback) 
    {
        self.send('POST', route, data, callback);
	}

    self.get = function(route, data, callback)
    {
        self.send('GET', route, data, callback);
    }

    self.send = function(method, url, params, callback) 
    {
        $.ajax({
            url : url,
            data : params,
            type : method,
            success : function(response) {
            	return callback(true, response);
            },
            error : function(jqXHR, status, error) {
                return callback(false, {
                    status : status,
                    error : error
                });
            }
        });
    }

	$.ajaxSetup({ cache : false });

	return self;
}

function DomainCategory(selectorDomain, selectorCategory)
{
	var self = this;
	var request = new Request();
	var $domains = $(selectorDomain);
	var $categories = $(selectorCategory);

	self.categories = {};
	self.domains = {};

	self.construct = function()
	{
		request.get("/merchant/service/domains", {}, function(success, response){
			if (success) {
				self.domains = response;
			}
			self.iniDomains($domains.data('selected'));
		});

		request.get("/merchant/service/categories", {}, function(success, response){
			if (success) {
				self.categories = response;
			}
			self.initCategories($domains.data('selected'), $categories.data('selected'));
		});

		$domains.on('change', function(){
			$categories.trigger('changeDomain', $domains.find('option:selected').val());
		});

		$categories.on('changeDomain', function(event, domainId){
			self.initCategories(domainId, 0);
		});
	}

	self.buildOptions = function(selectValue, list)
	{
		var options = '<option value="0">Choisissez</option>';

		for (var id in list) {
			var current = list[id];
			var selected = (id == selectValue) ? "selected" : "";
			options += '<option value="'+current.id+'" '+selected+'>'+current.name+'</option>'
		}

		return options;
	}

	self.iniDomains = function(domainId)
	{
		var options = self.buildOptions(domainId, self.domains);
		$domains.html(options);
	}

	self.initCategories = function(domainId, categoryId)
	{
		var categories = self.categories[domainId];
		var options = self.buildOptions(categoryId, categories);
		$categories.html(options);
		$categories.prop("disabled", domainId == 0);
	}

	self.getDomainId = function(categoryId){
		for (var i in self.categories) {
			if (categoryId in self.categories[i]) {
				return i;
			}
		}
		return 0;
	};

	self.construct();
}