//////////
//GLOBAL//
//////////

// external datas path
var JSON_PATH_CITIES = "/data/canada_cities.json";
var JSON_PATH_PROVINCES = "/data/canada_provinces.json";
// JSON object that will contains datas with a xhr call after page is loaded
PROVINCES = CITIES = SELECTED_PROV = "";

////////
//MAIN//
////////

$(document).ready(function () {

    // DatePicker set up
    $(".date-picker").datepicker({dateFormat: "yy-mm-dd"});
    $(".date-picker").datepicker("option", "changeYear", true);
    $(".date-picker").datepicker("option", "yearRange", "1900:c");
    $(".date-picker").datepicker("hide");
    $("#btnShowHideDatePicker, .date-picker").click(function () {
        var visible = $('#ui-datepicker-div').is(':visible');
        $('.date-picker').datepicker(visible ? 'hide' : 'show');
    });

    // Configuration to make the custom file input 
    $(document).on('click', '.browse', function () {
        var file = $(this).parent().parent().parent().find('.file');
        file.trigger('click');
    });
    
    $(document).on('change', '.file', function () {
        $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });

    // --------------------------SELECT PROVINCE AND CITIES ---------------------------------------------------->

    // Feed the data provinces when the document is loaded.
    // Feed the input select provinces and match his value with the his hidden textfield.
    $.ajax({
        dataType: "json",
        url: JSON_PATH_PROVINCES,
        mimeType: "application/json",//Data type from the server
        success: function(data){
            PROVINCES = data.provinces;
            populateProvinces(PROVINCES, $(".slc-prov"));
            selectValueFromTextValue($("#slc-prov-a"), $("#txt-prov-a").val());
            if(typeof $("#slc-prov-b").val() !== "undefined"){
                selectValueFromTextValue($("#slc-prov-b"), $("#txt-prov-b").val());
            }
            populateCities();
        }
    });

    
     $(document).on('change', '#slc-prov-a', function (){
        $("#slc-city-a").empty();
        selectedProv = $(this).val();
        populateCitiesFromValue(CITIES, $("#slc-city-a"), selectedProv);
    });
    
     $(document).on('change', '#slc-prov-b', function (){
        $("#slc-city-b").empty();
        selectedProv = $(this).val();
        populateCitiesFromValue(CITIES, $("#slc-city-b"), selectedProv);
    });
    
    $("#frm-update-member").submit(function(){
        updateTextValueFromSelectedValue($("#slc-prov-a"), $("#txt-prov-a"));
        updateTextValueFromSelectedValue($("#slc-city-a"), $("#txt-city-a"));      
        if(typeof $("#slc-prov-b").val() !== "undefined" && typeof $("#slc-city-b").val() !== "undefined"){
            updateTextValueFromSelectedValue($("#slc-prov-b"), $("#txt-prov-b"));
            updateTextValueFromSelectedValue($("#slc-city-b"), $("#txt-city-b"));
        }
    });

});

////////////
//FUNCTION//
////////////

// Fill up the input select provinces.
// jsonData: provinves data
// select: the input select provinces
function populateProvinces(jsonData, select){   
    for (i = 0; i < jsonData.length; i++) {
        select.append("<option>" + jsonData[i]["name"] + "</option>");
     }    
}

// Fill up the input select cities from the input select provinces selected option.
// jsonData: cities datas
// select: the select input for the cities
// value: the selected province value
function populateCitiesFromValue(jsonData, select, value){  
    cities = jsonData[value]; 
    for (i = 0; i < cities.length; i++) {
        select.append("<option>" + cities[i] + "</option>");
      }
}

// Match and select the input selected option with the textfield value
// select: the select input that gonna be matched
// value: the value that it's going to match with
function selectValueFromTextValue(select, value){
     $(select).find("option").each(function () {
        if ($(this).val() === value ) {
            $(this).prop("selected", true);
            return $(this).val();
        }
    });
}

// Feed the data cities when the data province has been loaded.
// Match the input select cities option with the hidden textfield.
function populateCities(){    
    $.ajax({
        dataType: "json",
        url: JSON_PATH_CITIES,
        mimeType: "application/json",
        success: function(data){
            CITIES = data;
            populateCitiesFromValue(CITIES, $("#slc-city-a"), $("#slc-prov-a").val());
            selectValueFromTextValue($("#slc-city-a"), $("#txt-city-a").val());
            if(typeof $("#slc-prov-b").val() !== "undefined" && typeof $("#slc-city-b").val() !== "undefined"){
                 populateCitiesFromValue(CITIES, $("#slc-city-b"), $("#slc-prov-b").val());
                 selectValueFromTextValue($("#slc-city-b"), $("#txt-city-b").val());
            }
        }
    });
}

// Update the hidden textbox value from the selected value before the submit event
// select: the select input
// textbox: the hidden textfield
function updateTextValueFromSelectedValue(select, textbox){
    textbox.val(select.val());
}