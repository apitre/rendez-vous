$(document).ready(function () {

    // toggle fullscreeen button
    $("#scheduler-btn-fullscreen").click(function () {
        var element = document.getElementById("scheduler_here");

        if (!isFullScreen())
            goInFUllScreen(element);
        else
            quitFullScreen();
    });
}); 

function CalendarController(settings, calendar)
{
    var self = this;

    self.init = function ()
    {
        calendar.config.xml_date = "%Y-%m-%d %H:%i";

        calendar.load(settings.loadRoute, "json");

        calendar.init('scheduler_here', new Date(), "week");

        calendar.attachEvent("onEventAdded", self.onEventAdded);
        calendar.attachEvent("onBeforeEventDelete", self.onBeforeEventDelete);
        calendar.attachEvent("onEventLoading", self.onEventLoading);

        var dp = new dataProcessor(settings.editRoute);

        dp.init(calendar);
    };

    self.onEventAdded = function (id, service)
    {
        service.service_id = settings.service_id;
        service.consumer_id = 1;
        service.status = 1;
        return true;
    };

    self.onBeforeEventDelete = function (id, e)
    {
        return true;
    };

    self.onEventLoading = function (service)
    {
        if (getParam('today') == 1) {
            $('.dhx_cal_tab_first').click();
        }

        return true;
    };

    return self;
}

function isFullScreen() {
    if (
            document.fullscreenElement ||
            document.webkitFullscreenElement ||
            document.mozFullScreenElement ||
            document.msFullscreenElement
            )
    {
        return true;
    }
    return false;
}


function goInFUllScreen(i)
{
    if (
            document.fullscreenEnabled ||
            document.webkitFullscreenEnabled ||
            document.mozFullScreenEnabled ||
            document.msFullscreenEnabled
            )
    {
        if (i.requestFullscreen) {
            i.requestFullscreen();
        } else if (i.webkitRequestFullscreen) {
            i.webkitRequestFullscreen();
        } else if (i.mozRequestFullScreen) {
            i.mozRequestFullScreen();
        } else if (i.msRequestFullscreen) {
            i.msRequestFullscreen();
        }
    } else
        alert("fullscreen not allowed");
}

function quitFullScreen() {

    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
    }

}