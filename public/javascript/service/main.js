$(document).ready(function(){
	
	var domainCategory = new DomainCategory('#domain', '#category');

	// DatePicker from jquery-ui
    $("#date_from").datepicker('option', 'minDate', 0);
    $("#date_to").datepicker('option', 'minDate', 0);
    $("#date_to").datepicker('option', 'maxDate', getMaxDate(1));

});