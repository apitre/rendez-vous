$(document).ready(function()
{
    console.log('create service');

    $("#formService").on("submit", function(event)
    {
        console.log("résultat de validation du formulaire est:");
        console.log(validateForm());

        if (validateForm() === false) {
           event.preventDefault();
        }

    });
       
});

function validateForm()
{
    console.log(validateDates() && validateDomain() && validateCategory() && validatePrice() && validateDays());
    return (validateDates() && validateHours() && validateDays() && validateDuration());
};

function validateDates()
{  
    var $dateFrom = $("#date_from").val();
    var $dateTo = $("#date_to").val();
    var dateFrom = document.getElementById("date_from");
    var dateTo = document.getElementById("date_to");
    if ($dateFrom === "" || $dateFrom === null){
        $("#date_from").addClass("has-error");
        dateFrom.style.border="1px solid #a94442";
        $("#date_from").css("box-shadow" ,"inset 0 1px 1px rgba(0,0,0,.075)");
        app.displayError("<p>Insérez la date du début du service !</p>");
        return false;
    }
    if($dateTo === null ||$dateTo === "") {
        $("#date_to").addClass("has-error");
        dateTo.style.border="1px solid #a94442";
        $("#date_to").css("box-shadow" ,"inset 0 1px 1px rgba(0,0,0,.075)");
        app.displayError("<p>Insérez la date de fin de service !</p>");
        return false;
    }
    if ( $dateTo < $dateFrom){
        dateFrom.style.border="1px solid #a94442";
        $("#date_from").css("box-shadow" ,"inset 0 1px 1px rgba(0,0,0,.075)");
        dateTo.style.border="1px solid #a94442";
        $("#date_to").css("box-shadow" ,"inset 0 1px 1px rgba(0,0,0,.075)");
        app.displayError("<p>la date de fin de service doit être supérieure à la date du début du service !</p>");
        return false;
    }
        dateFrom.style.border= "1px solid #ccc";
        dateTo.style.border= "1px solid #ccc";
    return true;
}

function validateDuration(){
     var $length = $("#length").val();

    if($length === ""){
        app.displayError("<p>Vous n'avez pas choisi une durée pour ce service !</p>");
        return false;
    }

    return true;
}



function validatePrice()
{
    var $price = $("#price").val();
    var regex = /[0-9 ]{1,}[,.]{0,1}[0-9]{0,2}/;
    regex.compile(regex);
    console.log("le regex", regex.exec($price));
    console.log('$price !== ""',$price !== "");
    var price = document.getElementById("price");
    var priceFloat = parseFloat($price);
    console.log(price.validity.valueMissing);
    console.log('regex.test($price)', regex.test($price));
    console.log('!isNaN(priceFloat)',!isNaN(priceFloat));

    if( $price !== "" && regex.test($price) && !isNaN(parseFloat($price)) ){
        console.log("price ",$price);
        if (priceFloat.toString() === $price) {
            return true; 
        }
    } 

    app.displayError("<p>Vous devez entrer un prix valide !</p>");
    $("#errorPrice").text = "invalide";
    //$("#errorPrice").style.color = "red";
    //$("#errorPrice").style.bold(true);
    return false;
}

function validateDays()
{
    var $days = $(":checkbox" );

    for(var i=0 ;i<$days.length; i++) {
        if ($days[i].checked === true){
            return true;
        }
    }

    app.displayError("<p>Veuillez renseigner les champs manquants");

    return false;
}

function validateHours()
{
    var $hourBegin = $("#hourBegin").val();
    var $hourEnd = $("#hourEnd").val();

    if($hourBegin === "" || $hourEnd === ""){
        app.displayError("<p>Vous n'avez pas choisi un horaire de travail pour ce service !</p>");
        return false;
    }

    return true;
}
