var POPULAR_ROUTE = "/popular";

$(document).ready(function () {

    $("button[name='btn_register']").click(function () {
        $(":text, :password").removeAttr("required");
    });

    $("button[name='btn_signin']").click(function () {
        $(":text, :password").attr("required", "true");
    });

    // Load popular services into the sliders
    $("#slider-popular-service").load(POPULAR_ROUTE, function () {
        initSliders();
    });


});

// 2 Slicks sliders is used and synchronised together.
// Do not change configurations unless you know what you are doing
// Otherwise they can be Out of Sync.
function initSliders() {
    var slidesPerRow = 1;
    var timesPerSlide = 3000;
    var slideScrolled = 1;
    var isSlideDraggable = false;

    // Configuration of the image slider
    $(".slider-image").slick({
        autoplaySpeed: timesPerSlide,
        slidesToShow: slidesPerRow,
        slidesToScroll: slideScrolled,
        draggable: isSlideDraggable,
        arrows: true,
        autoplay: true
    });
}