function App(status)
{
    var self = new AppEvent(this);

    var statusController = undefined;

    self.construct = function() 
    {
        statusController = new StatusController();

        self.setActiveMenu();

        if (status !== null) {
            statusController.display(status);
        }

        self.trigger('ready');
    }

    self.setActiveMenu = function()
    {
        $(".navbar-nav li").each(function(){
            var $li = $(this);
            if ($li.find('a').attr('href') == window.location.pathname) {
                $li.addClass('active');
                return true;
            }
        });
        return false;
    }

    self.scrollTo = function(selector)
    {
        window.scrollTo(0, $(selector).first().offset().top - 10);
    }

    self.calendar = function(serviceId, today)
    {
        var calendarUrl = '/merchant/calendar';

        if (serviceId > 0) {
            calendarUrl += ("/"+serviceId);
        }

        if (today) {
            calendarUrl += "?today=1";
        } 

        modal.calendar({
            calendar : '<iframe src="'+calendarUrl+'"></iframe>',
            btnConfirm : "Déconnecter",
            onConfirm : function () {
                window.location.href = "/signout";
            }
        });
    }

    self.disconnect = function ()
    {
        modal.confirm({
            title : "Déconnexion",
            body : "Ceci vous déconnectera de votre compte.<br><b>Etes-vous certain?</b>",
            btnConfirm : "Déconnecter",
            onConfirm: function () {
                window.location.href = "/signout";
            }
        });
    }
    
    self.deactivate_service = function (id)
    {
        console.log(id);
        modal.confirm({
            title: "Suppression du service",
            body: "<p>Ceci va déactiver votre service et ne sera plus visible sauf pour les clients qui ont dèjà pris des rendez-vous pour ce service. <br>Quand tous les rendez-vous associés à ce service soient passés il sera automatiquement supprimé de votre liste de services et ne figurera plus dans les recherches de services disponibles. <br><br><b>Etes-vous certain?</b></p>",
            btnConfirm: "Supprimer",
            onConfirm: function () {
                window.location.href = "/merchant/service/delete?id="+id;
            }
        });
    }
    
    self.takeBooking = function (data)
    {
        console.log(data);

        modal.confirm({
            title : "Réservation",
            btnConfirm : "Confirmer",
            onConfirm : function() {
                self.saveBooking(data);
            },
            onShow : function() {
                self.showService(data);
            }
        });
        modal.loading();
    }

    self.showService = function(data)
    {
        request.post('/consumer/service/view', data, function(success, response){
            if (success) {
                modal.setBody(response);
            } else {
                alert('error');
            }
        });
    }

    self.displaySuccess = function(msg)
    {
        statusController.display({
            'type' : 'success',
            'message' : msg
        });
    }

    self.displayError = function(msg)
    {
        statusController.display({
            'type' : 'danger',
            'message' : msg
        });
    }  

    self.saveBooking = function (data)
    {
        modal.loading();

        request.post("/consumer/service/booking", data, function (success, response){
            if (success && response.redirect != undefined) {
                window.location.href = response.redirect;
            } else {
                setTimeout(function () {
                    modal.error(response.errorMessage);
                }, 1000);
            }
        });
    }

    $(document).ready(self.construct);
    return self;
}



