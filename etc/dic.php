<?php

use \App\Core\SQLDatabase;
use \App\Core\Session;
use \App\Core\MailService;
use \App\Common\Configuration;
use \App\User\UserRepository;

$container = $app->getContainer();

$container['mailService'] = function ($container) 
{
    $mailer = new PHPMailer;

    $mailer->IsSMTP();
    $mailer->isHTML(true);
    $mailer->Mailer = 'smtp';
    $mailer->SMTPAuth = true;
    $mailer->Host = 'smtp.gmail.com'; 
    $mailer->Port = 587;
    $mailer->SMTPSecure = 'tls';
    $mailer->Username = "applicationrendezvous@gmail.com";
    $mailer->Password = "441155Ap";

    return new MailService($container->view, $mailer);
};

$container['view'] = function ($container) 
{
    $twigSettings = [
        'cache' => false,
        'debug' => true
    ];

    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');

    $view = new \Slim\Views\Twig(__ROOT__.'/public/templates', $twigSettings);

    $view->addExtension(new Teraone\Twig\Extension\StrftimeExtension());

    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));

    $view->addExtension(new \Twig_Extension_Debug());

    return $view;
};

$container['calendar'] = function($container) use ($settings) 
{
    require(__ROOT__."/etc/scheduler/scheduler_connector.php");
    require(__ROOT__."/etc/scheduler/db_pdo.php");
 
    $pdo = new PDO($settings['dns'], $settings['username'], $settings['password']);
      
    return new JSONSchedulerConnector($pdo, "PDO");
};

$container['db'] = function($container) use ($settings) 
{
    $opt = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false
    ];

    $pdo = new \PDO($settings['dns'], $settings['username'], $settings['password'], $opt);

    return new SQLDatabase($pdo);
};

$container['session'] = function ($container) 
{
    return new Session();
};

$container['configuration'] = function ($container) use ($settings) 
{
    return new Configuration($settings); 
};