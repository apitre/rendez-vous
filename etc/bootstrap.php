<?php

error_reporting(E_ALL);

ini_set('display_errors', 'On');

setlocale(LC_TIME, "fr_CA");

$settings = [
    'displayErrorDetails' => true
];

$app = new \Slim\App(["settings" => $settings]);

$settings = [];

$clearDb = parse_url(getenv("CLEARDB_DATABASE_URL"));

if ($clearDb['path'] == "") {
    $settings['host']       = "localhost";
    $settings['database']   = "rendez_vous";
    $settings['username']   = "root";
    $settings['password']   = "root";
    $settings['env']        = "local";
    $settings['domain']     = "http://localhost";
    $settings['dns']        = "mysql:host={$settings['host']};dbname={$settings['database']};charset=utf8";
} else {
    $settings['host']       = "us-cdbr-iron-east-03.cleardb.net";
    $settings['database']   = "heroku_551d5126f2af59f";
    $settings['username']   = "b09bb18632abb6";
    $settings['password']   = "687309bd";
    $settings['env']        = "heroku";
    $settings['domain']     = "http://app-rendez-vous.herokuapp.com";
    $settings['dns']        = "mysql:host={$settings['host']};dbname={$settings['database']};charset=utf8";
}

require __ROOT__.'etc/dic.php';

require __ROOT__.'etc/routes/main.php';

$app->run();
