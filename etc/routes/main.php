<?php

require __ROOT__.'etc/routes/sql.php';

require __ROOT__.'src/App/Main/MainRoute.php';

require __ROOT__.'src/App/User/UserRoute.php';

require __ROOT__.'src/App/Consumer/ConsumerRoute.php';

require __ROOT__.'src/App/Merchant/MerchantRoute.php';

require __ROOT__.'src/App/Service/ServiceRoute.php';
