<?php

use \App\Core\SQLAutoloader;
use App\Core\Middleware;

$app->get('/sql/init', function ($request, $response, $args) {
	
    $SQLAutoloader = new SQLAutoloader($this->db, __ROOT__);
    
    $files = [];

    #Place files in order of execution
    if ($this->configuration->isLocal()) {
        $files[] = "/etc/sql/local.sql";
    } else {
        $files[] = "/etc/sql/production.sql";
    }

    $files[] = "/etc/sql/create.sql";
    $files[] = "/etc/sql/service.sql";
    $files[] = "/etc/sql/populate.sql";       

    $SQLAutoloader->executeList($files);

})->add(Middleware::localRestriction());

$app->get('/sql/init2', function ($request, $response, $args) {
	
    $SQLAutoloader = new SQLAutoloader($this->db, __ROOT__);
    
    $files = [];

    #Place files in order of execution
    if ($this->configuration->isLocal()) {
        $files[] = "/etc/sql/local.sql";
    } else {
        $files[] = "/etc/sql/production.sql";
    }
   
    $files[] = "/etc/sql/create.sql";
    $files[] = "/etc/sql/service.sql";
    $files[] = "/etc/sql/populate_address.sql";       
    $files[] = "/etc/sql/populate_user.sql";
    $files[] = "/etc/sql/populate_services.sql";
    $files[] = "/etc/sql/populate_appointments.sql";
    $files[] = "/etc/sql/populate_ratings.sql";

    $SQLAutoloader->executeList($files);

})->add(Middleware::localRestriction());

$app->get('/sql/dump', function ($request, $response, $args) {
    
    $SQLAutoloader = new SQLAutoloader($this->db, __ROOT__);

    $files = [];
    
    #Place files in order of execution
    $files[] = "/etc/sql/production.sql";
    $files[] = "/etc/sql/create.sql";
    $files[] = "/etc/sql/service.sql";
    $files[] = "/etc/sql/populate_address.sql";       
    $files[] = "/etc/sql/populate_user.sql";
    $files[] = "/etc/sql/populate_services.sql";
    $files[] = "/etc/sql/populate_appointments.sql";
    $files[] = "/etc/sql/populate_ratings.sql";

    $SQLAutoloader->dump($files);

})->add(Middleware::localRestriction());