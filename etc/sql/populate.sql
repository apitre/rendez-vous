BEGIN;

INSERT INTO address VALUES
(1, 5000, 'labelle', 9, 'Montréal', 'Québec', 'H1C 2C2');

INSERT INTO user VALUES 
(1, 'Pitre', 'Alain', 'pita', 'test@gmail.com', '1', NOW(), '1', '5141234567', '2000-01-01', 1);

INSERT INTO consumer VALUES 
(1, 1);

INSERT INTO user_access VALUES 
(1, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');

INSERT INTO address VALUES
(2, 5111, 'Barclay', 9, 'Longueuil', 'Québec', 'H3W 1E3');
INSERT INTO address VALUES
(3, 777, 'Chartrand', 9, 'Longueuil', 'Québec', 'H3W 1E7');
INSERT INTO address VALUES
(4, 5999, 'Paul', 9, 'Laval', 'Québec', 'H3W 8F8');
INSERT INTO address VALUES
(5, 777, 'Paul', 9, 'Laval', 'Québec', 'H3W 1E7');

INSERT INTO user VALUES 
(2, 'Alex', 'Gravel', 'agmenard', 'mick_love2000@hotmail.com', '2', NOW(), '1', '5141234567', '2000-01-01', 2);

INSERT INTO user_access VALUES 
(2, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');

INSERT INTO user VALUES 
(3, 'bob', 'marchand', 'marchand', 'marchand@hotmail.com', '2', NOW(), '1', '5142224444', '2000-01-01', 4);

INSERT INTO user_access VALUES 
(3, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');

INSERT INTO merchant VALUES 
(1, 2, 'agminc', 3, 'mick_love2000@hotmail.com', '5141234567');

INSERT INTO merchant VALUES 
(2, 3, 'CompagnieBob', 5, 'marchand@hotmail.com', '5142224444');

INSERT INTO service VALUES (
	1, 
	12, 
	1, 
	'Cloture 1', 
	'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure.', 
	'24.99', 
	'2017-01-14 10:25:15', 
	'2017-04-14 10:25:15',
	'01:30:00',
	'08:30:00',
	'17:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
); 

INSERT INTO service VALUES (
	2, 
	12, 
	1, 
	'Vitre 2', 
	'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 
	'14.99', 
	'2017-04-14 10:25:15',
	'2017-04-30 12:25:15',
	'02:00:00',
	'08:30:00',
	'17:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	3, 
	2, 
	1, 
	'Vitre 3', 
	'Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 
	'14.99', 
	'2017-04-14 10:25:15',
	'2017-05-11 08:25:15', 
	'02:30:00',
	'08:30:00',
	'17:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	4, 
	12, 
	2, 
	'Porte 4', 
	'le service 1 du user marchand 2', 
	'24.99', 
	'2016-02-14 10:25:15', 
	'2017-04-30 5:25:15',
	'01:30:00',
	'08:30:00',
	'17:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
); 

INSERT INTO service VALUES (
	5, 
	12, 
	2, 
	'Voiture 5', 
	'le service 2 du user marchand 2', 
	'14.99', 
	'2017-04-14 10:25:15',
	'2018-03-31 20:25:15',
	'02:00:00',
	'08:30:00',
	'17:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	6, 
	2, 
	2, 
	'Balcon 6', 
	'le service 3 du user marchand 2', 
	'14.99', 
	'2017-02-14 10:25:15',
	'2018-04-05 08:25:15', 
	'02:30:00',
	'08:30:00',
	'17:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
('1', SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 1 pour commercant', '4', '1', '4');

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
('2', SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), 'message 22 pour commercant', '4', '1', '4');

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
('3', CURRENT_TIMESTAMP, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), 'message 33 pour commercant', '4', '1', '3');

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
('4', CURRENT_TIMESTAMP, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), 'message 55 pour commercant', '4', '1', '3');

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
('5', CURRENT_TIMESTAMP, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 5 HOUR), 'message 66 pour commercant', '4', '1', '3');

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
('6', ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 HOUR), 'message 77 pour commercant', '4', '1', '1');

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
('7', ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 4 HOUR), 'message 88 pour commercant', '4', '1', '1');

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES  
('8', ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 4 HOUR), 'message 99 pour commercant', '4', '1', '1');

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
('9', ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), 'message 111 pour commercant', '4', '1', '1');

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES  
('10', ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), 'message 222 pour commercant', '4', '1', '1');

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
('11', ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 3434 pour commercant', '4', '1', '2');

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES  
('12', ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), 'message 333 pour commercant', '4', '1', '2');

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
('13', ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), 'message 555 pour commercant', '4', '1', '0');

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES  
('14', ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 999 pour commercant', '4', '1', '0');

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES  
('15', ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 4 HOUR), 'message 788 pour commercant', '5', '1', '1');

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
('16', ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), 'message 6666 pour commercant', '5', '1', '1');

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES  
('17', ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), 'message 444 pour commercant', '5', '1', '1');

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES  
('18', "2017-04-01 08:30:00", "2017-04-01 10:00:00", 'Message test1 conflict', '1', '1', '1');

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES  
('19', "2017-04-01 10:00:00", "2017-04-01 11:30:00", 'message test2 conflict', '1', '1', '1');

INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES  
('20', "2017-04-01 17:29:00", "2017-04-01 19:10:00", 'message test3 conflict', '1', '1', '1');

INSERT INTO rating VALUES 
(1, 1, 1, '3', 'Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');

INSERT INTO rating VALUES 
(2, 2, 1, '4', 'Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');

INSERT INTO rating VALUES 
(3, 3, 1, '2', 'Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');

INSERT INTO rating VALUES 
(4, 4, 1, '3', 'excellent.');

INSERT INTO rating VALUES 
(5, 4, 1, '4', 'dans la moyenne');

INSERT INTO rating VALUES 
(6, 5, 1, '2', 'non recommandé');

COMMIT;