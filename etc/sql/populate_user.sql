BEGIN;

INSERT INTO user VALUES 
(1, 'Pitre', 'Alain', 'pita', 'test@gmail.com', '1', NOW(), '1', '5141234567', '2000-01-01', 1);
INSERT INTO user_access VALUES 
(1, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO consumer VALUES 
(1, 1);

INSERT INTO user VALUES 
(2, 'Alex', 'Gravel', 'agmenard', 'mick_love2000@hotmail.com', '2', NOW(), '1', '5141234567', '2000-01-01', 2);
INSERT INTO user_access VALUES 
(2, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(1, 2, 'agminc', 50, 'mick_love2000@hotmail.com', '5141234567');

INSERT INTO user VALUES 
(3, 'beausejour', 'rick', 'marchand', 'marchand@hotmail.com', '2', NOW(), '1', '5142224444', '2001-01-01', 3);
INSERT INTO user_access VALUES 
(3, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(2, 3, 'rick et cie', 51, 'marchand@hotmail.com', '5142224444');

INSERT INTO user VALUES 
(4, 'bouchard', 'bob', 'marchand2', 'marchand2@hotmail.com', '2', NOW(), '1', '5142225555', '1990-01-01', 4);
INSERT INTO user_access VALUES 
(4, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(3, 4, 'bob et cie', 52, 'marchand2@hotmail.com', '5149994444');

INSERT INTO user VALUES 
(5, 'beauchamps', 'rose', 'marchand31', 'marchand31@hotmail.com', '2', NOW(), '1', '5142226666', '1991-01-01', 5);
INSERT INTO user_access VALUES 
(5, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(4, 5, 'rose et cie', 53, 'marchand31@hotmail.com', '5148884444');

INSERT INTO user VALUES 
(6, 'beland', 'mitch', 'marchand4', 'marchand4@hotmail.com', '2', NOW(), '1', '5142227777', '1992-01-01', 6);
INSERT INTO user_access VALUES 
(6, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(5, 6, 'mitch et cie', 54, 'marchand4@hotmail.com', '4507774444');

INSERT INTO user VALUES 
(7, 'tremblay', 'mike', 'marchand5', 'marchand5@hotmail.com', '2', NOW(), '1', '5142221111', '1993-01-01', 7);
INSERT INTO user_access VALUES 
(7, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(6, 7, 'mike et cie', 55, 'marchand5@hotmail.com', '4386664444');

INSERT INTO user VALUES 
(8, 'tremblay', 'rose', 'marchand6', 'marchand6@hotmail.com', '2', NOW(), '1', '5142222222', '1994-01-01', 8);
INSERT INTO user_access VALUES 
(8, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(7, 8, 'La belle fleur', 56, 'marchand6@hotmail.com', '4385554444');

INSERT INTO user VALUES 
(9, 'tremblay', 'bella', 'marchand7', 'marchand7@hotmail.com', '2', NOW(), '1', '5142223333', '1995-01-01', 9);
INSERT INTO user_access VALUES 
(9, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(8, 9, 'La belle fleur2', 57, 'marchand7@hotmail.com', '4384444444');

INSERT INTO user VALUES 
(10, 'tremblay', 'rosita', 'marchand8', 'marchand8@hotmail.com', '2', NOW(), '1', '5142224444', '1996-01-01', 10);
INSERT INTO user_access VALUES 
(10, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(9, 10, 'La belle province', 58, 'marchand8@hotmail.com', '8193334444');

INSERT INTO user VALUES 
(11, 'sicotte', 'michel', 'marchand9', 'marchand9@hotmail.com', '2', NOW(), '1', '5142225555', '1997-01-01', 11);
INSERT INTO user_access VALUES 
(11, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(10, 11, 'Orange', 59, 'marchand9@hotmail.com', '5142224444');

INSERT INTO user VALUES 
(12, 'zhang', 'tony', 'marchand10', 'marchand10@hotmail.com', '2', NOW(), '1', '5142226666', '1998-01-01', 12);
INSERT INTO user_access VALUES 
(12, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(11, 12, 'electro2', 60, 'marchand10@hotmail.com', '5141114444');

INSERT INTO user VALUES 
(13, 'sicotte', 'tony', 'client1', 'client1@gmail.com', '1', NOW(), '1', '5141237777', '2000-01-01', 13);
INSERT INTO user_access VALUES 
(13, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO consumer VALUES 
(2, 13);

INSERT INTO user VALUES 
(14, 'sicotte', 'tony', 'client2', 'client2@gmail.com', '1', NOW(), '1', '5141237777', '2000-01-01', 14);
INSERT INTO user_access VALUES 
(14, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO consumer VALUES 
(3, 14);

INSERT INTO user VALUES 
(15, 'sicotte', 'tony', 'client3', 'client3@gmail.com', '1', NOW(), '1', '5141237777', '2000-01-01', 15);
INSERT INTO user_access VALUES 
(15, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO consumer VALUES 
(4, 15);

INSERT INTO user VALUES 
(16, 'sicotte', 'tony', 'client4', 'client4@gmail.com', '1', NOW(), '1', '5141237777', '2000-01-01', 16);
INSERT INTO user_access VALUES 
(16, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO consumer VALUES 
(5, 16);

INSERT INTO user VALUES 
(17, 'sicotte', 'tony', 'client5', 'client5@gmail.com', '1', NOW(), '1', '5141237777', '2000-01-01', 17);
INSERT INTO user_access VALUES 
(17, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO consumer VALUES 
(6, 17);

INSERT INTO user VALUES 
(18, 'sicotte', 'tony', 'client6', 'client6@gmail.com', '1', NOW(), '1', '5141237777', '2000-01-01', 18);
INSERT INTO user_access VALUES 
(18, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO consumer VALUES 
(7, 18);

INSERT INTO user VALUES 
(19, 'sicotte', 'tony', 'client7', 'client7@gmail.com', '1', NOW(), '1', '5141237777', '2000-01-01', 19);
INSERT INTO user_access VALUES 
(19, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO consumer VALUES 
(8, 19);

INSERT INTO user VALUES 
(20, 'sicotte', 'tony', 'client8', 'client8@gmail.com', '1', NOW(), '1', '5141237777', '2000-01-01', 20);
INSERT INTO user_access VALUES 
(20, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO consumer VALUES 
(9, 20);


INSERT INTO user VALUES 
(21, 'beausejour', 'rita', 'marchand100', 'marchand100@hotmail.com', '2', NOW(), '1', '5142224444', '2001-01-01', 21);
INSERT INTO user_access VALUES 
(21, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(12, 21, 'rita et cie 100', 61, 'marchand100@hotmail.com', '5142224444');

INSERT INTO user VALUES 
(22, 'beausejour', 'rita', 'marchand101', 'marchand101@hotmail.com', '2', NOW(), '1', '5142224444', '2001-01-01', 22);
INSERT INTO user_access VALUES 
(22, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(13, 22, 'rita et cie 101', 62, 'marchand101@hotmail.com', '5142224444');

INSERT INTO user VALUES 
(23, 'beausejour', 'rita', 'marchand102', 'marchand102@hotmail.com', '2', NOW(), '1', '5142224444', '2001-01-01', 23);
INSERT INTO user_access VALUES 
(23, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(14, 23, 'rita et cie 102', 63, 'marchand102@hotmail.com', '5142224444');

INSERT INTO user VALUES 
(24, 'bouchard', 'bob', 'marchand103', 'marchand103@hotmail.com', '2', NOW(), '1', '5142225555', '1990-01-01', 24);
INSERT INTO user_access VALUES 
(24, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(15, 24, 'bob et cie 103', 64, 'marchand103@hotmail.com', '5149994444');

INSERT INTO user VALUES 
(25, 'beauchamps', 'rose', 'marchand130', 'marchand130@hotmail.com', '2', NOW(), '1', '5142226666', '1991-01-01', 25);
INSERT INTO user_access VALUES 
(25, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(16, 25, 'rose services 130', 65, 'marchand130@hotmail.com', '5148884444');

INSERT INTO user VALUES 
(26, 'beland', 'mitch', 'marchand104', 'marchand104@hotmail.com', '2', NOW(), '1', '5142227777', '1992-01-01', 26);
INSERT INTO user_access VALUES 
(26, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(17, 26, 'mitch services  104', 66, 'marchand104@hotmail.com', '4507774444');

INSERT INTO user VALUES 
(27, 'tremblay', 'mike', 'marchand105', 'marchand105@hotmail.com', '2', NOW(), '1', '5142221111', '1993-01-01', 27);
INSERT INTO user_access VALUES 
(27, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(18, 27, 'mike services  105', 67, 'marchand105@hotmail.com', '4386664444');

INSERT INTO user VALUES 
(28, 'tremblay', 'rose', 'marchand106', 'marchand106@hotmail.com', '2', NOW(), '1', '5142222222', '1994-01-01', 28);
INSERT INTO user_access VALUES 
(28, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(19, 28, 'La belle marchand106', 68, 'marchand106@hotmail.com', '4385554444');

INSERT INTO user VALUES 
(29, 'tremblay', 'bella', 'marchand107', 'marchand107@hotmail.com', '2', NOW(), '1', '5142223333', '1995-01-01', 29);
INSERT INTO user_access VALUES 
(29, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(20, 29, 'La belle marchand107', 69, 'marchand107@hotmail.com', '4384444444');

INSERT INTO user VALUES 
(30, 'beausejour', 'rita', 'marchand109', 'marchand109@hotmail.com', '2', NOW(), '1', '5142224444', '2001-01-01', 30);
INSERT INTO user_access VALUES 
(30, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(21, 30, 'rita services  109', 70, 'marchand109@hotmail.com', '5142224444');

INSERT INTO user VALUES 
(31, 'beausejour', 'rita', 'marchand110', 'marchand110@hotmail.com', '2', NOW(), '1', '5142224444', '2001-01-01', 31);
INSERT INTO user_access VALUES 
(31, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(22, 31, 'rita services  110', 71, 'marchand110@hotmail.com', '5142224444');

INSERT INTO user VALUES 
(32, 'beausejour', 'rita', 'marchand111', 'marchand111@hotmail.com', '2', NOW(), '1', '5142224444', '2001-01-01', 32);
INSERT INTO user_access VALUES 
(32, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(23, 32, 'rita services  111', 72, 'marchand111@hotmail.com', '5142224444');

INSERT INTO user VALUES 
(33, 'beausejour', 'rita', 'marchand112', 'marchand112@hotmail.com', '2', NOW(), '1', '5142224444', '2001-01-01', 33);
INSERT INTO user_access VALUES 
(33, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(24, 33, 'rita services  112', 73, 'marchand112@hotmail.com', '5142224444');

INSERT INTO user VALUES 
(34, 'bouchard', 'bob', 'marchand113', 'marchand113@hotmail.com', '2', NOW(), '1', '5142225555', '1990-01-01', 34);
INSERT INTO user_access VALUES 
(34, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(25, 34, 'bob enregistré 113', 74, 'marchand113@hotmail.com', '5149994444');

INSERT INTO user VALUES 
(35, 'beauchamps', 'rose', 'marchand118', 'marchand118@hotmail.com', '2', NOW(), '1', '5142226666', '1991-01-01', 35);
INSERT INTO user_access VALUES 
(35, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(26, 35, 'rose enregistré 118', 75, 'marchand118@hotmail.com', '5148884444');

INSERT INTO user VALUES 
(36, 'beland', 'mitch', 'marchand114', 'marchand114@hotmail.com', '2', NOW(), '1', '5142227777', '1992-01-01', 36);
INSERT INTO user_access VALUES 
(36, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(27, 36, 'mitch enregistré 114', 76, 'marchand114@hotmail.com', '4507774444');

INSERT INTO user VALUES 
(37, 'tremblay', 'mike', 'marchand115', 'marchand115@hotmail.com', '2', NOW(), '1', '5142221111', '1993-01-01', 37);
INSERT INTO user_access VALUES 
(37, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(28, 37, 'mike enregistré 115', 77, 'marchand115@hotmail.com', '4386664444');

INSERT INTO user VALUES 
(38, 'tremblay', 'rose', 'marchand116', 'marchand116@hotmail.com', '2', NOW(), '1', '5142222222', '1994-01-01', 38);
INSERT INTO user_access VALUES 
(38, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(29, 38, 'La belle marchand117', 78, 'marchand117@hotmail.com', '4385554444');

INSERT INTO user VALUES 
(39, 'tremblay', 'bella', 'marchand117', 'marchand117@hotmail.com', '2', NOW(), '1', '5142223333', '1995-01-01', 39);
INSERT INTO user_access VALUES 
(39, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(30, 39, 'La belle marchand117', 79, 'marchand117@hotmail.com', '4384444444');

INSERT INTO user VALUES 
(40, 'beausejour', 'rita', 'marchand119', 'marchand119@hotmail.com', '2', NOW(), '1', '5142224444', '2001-01-01', 40);
INSERT INTO user_access VALUES 
(40, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(31, 40, 'rita enregistré 119', 80, 'marchand119@hotmail.com', '5142224444');

INSERT INTO user VALUES 
(41, 'beausejour', 'rita', 'marchand120', 'marchand120@hotmail.com', '2', NOW(), '1', '5142224444', '2001-01-01', 41);
INSERT INTO user_access VALUES 
(41, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(32, 41, 'rita enregistré 120', 81, 'marchand120@hotmail.com', '5142224444');

INSERT INTO user VALUES 
(42, 'beausejour', 'rita', 'marchand121', 'marchand121@hotmail.com', '2', NOW(), '1', '5142224444', '2001-01-01', 42);
INSERT INTO user_access VALUES 
(42, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(33, 42, 'rita enregistré 121', 82, 'marchand121@hotmail.com', '5142224444');

INSERT INTO user VALUES 
(43, 'beausejour', 'rita', 'marchand122', 'marchand122@hotmail.com', '2', NOW(), '1', '5142224444', '2001-01-01', 43);
INSERT INTO user_access VALUES 
(43, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(34, 43, 'rita et cie 122', 83, 'marchand122@hotmail.com', '5142224444');

INSERT INTO user VALUES 
(44, 'bouchard', 'bob', 'marchand123', 'marchand123@hotmail.com', '2', NOW(), '1', '5142225555', '1990-01-01', 44);
INSERT INTO user_access VALUES 
(44, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(35, 44, 'bob et cie 123', 84, 'marchand123@hotmail.com', '5149994444');

INSERT INTO user VALUES 
(45, 'beauchamps', 'rose', 'marchand128', 'marchand128@hotmail.com', '2', NOW(), '1', '5142226666', '1991-01-01', 45);
INSERT INTO user_access VALUES 
(45, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(36, 45, 'rose et cie 128', 85, 'marchand128@hotmail.com', '5148884444');

INSERT INTO user VALUES 
(46, 'beland', 'mitch', 'marchand124', 'marchand124@hotmail.com', '2', NOW(), '1', '5142227777', '1992-01-01', 46);
INSERT INTO user_access VALUES 
(46, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(37, 46, 'mitch et cie 124', 86, 'marchand124@hotmail.com', '4507774444');

INSERT INTO user VALUES 
(47, 'tremblay', 'mike', 'marchand125', 'marchand125@hotmail.com', '2', NOW(), '1', '5142221111', '1993-01-01', 47);
INSERT INTO user_access VALUES 
(47, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(38, 47, 'mike et cie 125', 87, 'marchand125@hotmail.com', '4386664444');

INSERT INTO user VALUES 
(48, 'tremblay', 'rose', 'marchand126', 'marchand126@hotmail.com', '2', NOW(), '1', '5142222222', '1994-01-01', 48);
INSERT INTO user_access VALUES 
(48, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(39, 48, 'La belle marchand126', 88, 'marchand126@hotmail.com', '4385554444');

INSERT INTO user VALUES 
(49, 'tremblay', 'bella', 'marchand127', 'marchand127@hotmail.com', '2', NOW(), '1', '5142223333', '1995-01-01', 49);
INSERT INTO user_access VALUES 
(49, 'ee91eb0.55177230', '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC');
INSERT INTO merchant (merchant_id, user_id, name, address_id, email, phone) VALUES 
(40, 49, 'La belle marchand127', 89, 'marchand127@hotmail.com', '4384444444');




COMMIT;