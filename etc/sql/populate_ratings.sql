BEGIN;

INSERT INTO rating (rating_id, service_id, consumer_id, score, comment) VALUES 
(1, 1, 1, '3', 'Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');

INSERT INTO rating VALUES 
(2, 2, 1, '4', 'Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');

INSERT INTO rating VALUES 
(3, 3, 1, '2', 'Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');

INSERT INTO rating VALUES 
(4, 4, 1, '3', 'excellent.');
INSERT INTO rating VALUES 
(5, 4, 1, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(6, 4, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(7, 4, 2, '3', 'bon');
INSERT INTO rating VALUES 
(8, 4, 3, '4', 'good');
INSERT INTO rating VALUES 
(9, 4, 4, '1', 'moyenne');

INSERT INTO rating VALUES 
(10, 5, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(11, 5, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(12, 5, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(13, 5, 6, '3', 'excellent.');
INSERT INTO rating VALUES 
(14, 5, 6, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(15, 5, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(16, 5, 3, '3', 'bon');
INSERT INTO rating VALUES 
(17, 5, 4, '4', 'good');
INSERT INTO rating VALUES 
(18, 5, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(19, 5, 5, '1', 'moyenne');

INSERT INTO rating VALUES 
(20, 6, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(21, 6, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(22, 6, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(23, 6, 6, '3', 'excellent.');
INSERT INTO rating VALUES 
(24, 6, 6, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(25, 6, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(26, 6, 3, '3', 'bon');
INSERT INTO rating VALUES 
(27, 6, 4, '4', 'good');
INSERT INTO rating VALUES 
(28, 6, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(29, 6, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(30, 7, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(31, 7, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(32, 7, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(33, 7, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(34, 7, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(35, 7, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(36, 7, 3, '3', 'bon');
INSERT INTO rating VALUES 
(37, 7, 4, '4', 'good');
INSERT INTO rating VALUES 
(38, 7, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(39, 7, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(40, 8, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(41, 8, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(42, 8, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(43, 8, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(44, 8, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(45, 8, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(46, 8, 3, '3', 'bon');
INSERT INTO rating VALUES 
(47, 8, 4, '4', 'good');
INSERT INTO rating VALUES 
(48, 8, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(49, 8, 5, '1', 'moyenne');

INSERT INTO rating VALUES 
(50, 9, 9, '3', 'excellent.');
INSERT INTO rating VALUES 
(51, 9, 9, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(52, 9, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(53, 9, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(54, 9, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(55, 9, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(56, 9, 3, '3', 'bon');
INSERT INTO rating VALUES 
(57, 9, 4, '4', 'good');
INSERT INTO rating VALUES 
(58, 9, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(59, 9, 5, '1', 'moyenne');

INSERT INTO rating VALUES 
(60, 10, 5, '3', 'excellent.');
INSERT INTO rating VALUES 
(61, 10, 5, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(62, 10, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(63, 10, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(64, 10, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(65, 10, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(66, 10, 3, '3', 'bon');
INSERT INTO rating VALUES 
(67, 10, 4, '4', 'good');
INSERT INTO rating VALUES 
(68, 10, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(69, 10, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(70, 11, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(71, 11, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(72, 11, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(73, 11, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(74, 11, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(75, 11, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(76, 11, 3, '3', 'bon');
INSERT INTO rating VALUES 
(77, 11, 4, '4', 'good');
INSERT INTO rating VALUES 
(78, 11, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(79, 11, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(80, 12, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(81, 12, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(82, 12, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(83, 12, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(84, 12, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(85, 12, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(86, 12, 3, '3', 'bon');
INSERT INTO rating VALUES 
(87, 12, 4, '4', 'good');
INSERT INTO rating VALUES 
(88, 12, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(89, 12, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(90, 13, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(91, 13, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(92, 13, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(93, 13, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(94, 13, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(95, 13, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(96, 13, 3, '3', 'bon');
INSERT INTO rating VALUES 
(97, 13, 4, '4', 'good');
INSERT INTO rating VALUES 
(98, 13, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(99, 13, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(100, 14, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(101, 14, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(102, 14, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(103, 14, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(104, 14, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(105, 14, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(106, 14, 3, '3', 'bon');
INSERT INTO rating VALUES 
(107, 14, 4, '4', 'good');
INSERT INTO rating VALUES 
(108, 14, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(109, 14, 7, '1', 'moyenne');




















INSERT INTO rating VALUES 
(110, 15, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(111, 15, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(112, 15, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(113, 15, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(114, 15, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(115, 15, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(116, 15, 3, '3', 'bon');
INSERT INTO rating VALUES 
(117, 15, 4, '4', 'good');
INSERT INTO rating VALUES 
(118, 15, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(119, 15, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(120, 16, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(121, 16, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(122, 16, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(123, 16, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(124, 16, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(125, 16, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(126, 16, 3, '3', 'bon');
INSERT INTO rating VALUES 
(127, 16, 4, '4', 'good');
INSERT INTO rating VALUES 
(128, 16, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(129, 16, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(130, 17, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(131, 17, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(132, 17, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(133, 17, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(134, 17, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(135, 17, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(136, 17, 3, '3', 'bon');
INSERT INTO rating VALUES 
(137, 17, 4, '4', 'good');
INSERT INTO rating VALUES 
(138, 17, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(139, 17, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(140, 18, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(141, 18, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(142, 18, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(143, 18, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(144, 18, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(145, 18, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(146, 18, 3, '3', 'bon');
INSERT INTO rating VALUES 
(147, 18, 4, '4', 'good');
INSERT INTO rating VALUES 
(148, 18, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(149, 18, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(150, 19, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(151, 19, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(152, 19, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(153, 19, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(154, 19, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(155, 19, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(156, 19, 3, '3', 'bon');
INSERT INTO rating VALUES 
(157, 19, 4, '4', 'good');
INSERT INTO rating VALUES 
(158, 19, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(159, 19, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(160, 20, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(161, 20, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(162, 20, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(163, 20, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(164, 20, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(165, 20, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(166, 20, 3, '3', 'bon');
INSERT INTO rating VALUES 
(167, 20, 4, '4', 'good');
INSERT INTO rating VALUES 
(168, 20, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(169, 20, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(170, 21, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(171, 21, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(172, 21, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(173, 21, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(174, 21, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(175, 21, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(176, 21, 3, '3', 'bon');
INSERT INTO rating VALUES 
(177, 21, 4, '4', 'good');
INSERT INTO rating VALUES 
(178, 21, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(179, 21, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(180, 22, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(181, 22, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(182, 22, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(183, 22, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(184, 22, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(185, 22, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(186, 22, 3, '3', 'bon');
INSERT INTO rating VALUES 
(187, 22, 4, '4', 'good');
INSERT INTO rating VALUES 
(188, 22, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(189, 22, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(190, 23, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(191, 23, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(192, 23, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(193, 23, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(194, 23, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(195, 23, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(196, 23, 3, '3', 'bon');
INSERT INTO rating VALUES 
(197, 23, 4, '4', 'good');
INSERT INTO rating VALUES 
(198, 23, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(199, 23, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(200, 24, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(201, 24, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(202, 24, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(203, 24, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(204, 24, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(205, 24, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(206, 24, 3, '3', 'bon');
INSERT INTO rating VALUES 
(207, 24, 4, '4', 'good');
INSERT INTO rating VALUES 
(208, 24, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(209, 24, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(210, 25, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(211, 25, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(212, 25, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(213, 25, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(214, 25, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(215, 25, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(216, 25, 3, '3', 'bon');
INSERT INTO rating VALUES 
(217, 25, 4, '4', 'good');
INSERT INTO rating VALUES 
(218, 25, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(219, 25, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(220, 26, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(221, 26, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(222, 26, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(223, 26, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(224, 26, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(225, 26, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(226, 26, 3, '3', 'bon');
INSERT INTO rating VALUES 
(227, 26, 4, '4', 'good');
INSERT INTO rating VALUES 
(228, 26, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(229, 26, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(230, 27, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(231, 27, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(232, 27, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(233, 27, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(234, 27, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(235, 27, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(236, 27, 3, '3', 'bon');
INSERT INTO rating VALUES 
(237, 27, 4, '4', 'good');
INSERT INTO rating VALUES 
(238, 27, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(239, 27, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(240, 28, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(241, 28, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(242, 28, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(243, 28, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(244, 28, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(245, 28, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(246, 28, 3, '3', 'bon');
INSERT INTO rating VALUES 
(247, 28, 4, '4', 'good');
INSERT INTO rating VALUES 
(248, 28, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(249, 28, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(250, 29, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(251, 29, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(252, 29, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(253, 29, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(254, 29, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(255, 29, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(256, 29, 3, '3', 'bon');
INSERT INTO rating VALUES 
(257, 29, 4, '4', 'good');
INSERT INTO rating VALUES 
(258, 29, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(259, 29, 7, '1', 'moyenne');

INSERT INTO rating VALUES 
(260, 30, 8, '3', 'excellent.');
INSERT INTO rating VALUES 
(261, 30, 8, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(262, 30, 8, '2', 'non recommandé');
INSERT INTO rating VALUES 
(263, 30, 7, '3', 'excellent.');
INSERT INTO rating VALUES 
(264, 30, 7, '4', 'dans la moyenne');
INSERT INTO rating VALUES 
(265, 30, 2, '2', 'non recommandé');
INSERT INTO rating VALUES 
(266, 30, 3, '3', 'bon');
INSERT INTO rating VALUES 
(267, 30, 4, '4', 'good');
INSERT INTO rating VALUES 
(268, 30, 5, '1', 'moyenne');
INSERT INTO rating VALUES 
(269, 30, 7, '1', 'moyenne');

COMMIT;	
