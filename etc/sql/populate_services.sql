BEGIN;

INSERT INTO service VALUES (
	1, 
	50, 
	1, 
	'Changement d\'huile',  
	'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure.', 
	'39.99', 
	'2017-01-14 10:25:15', 
	'2017-03-14 10:25:15',
	'01:00:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
); 

INSERT INTO service VALUES (
	2, 
	90, 
	1, 
	'Coupe barbier',  
	'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 
	'24.99', 
	'2017-03-14 10:25:15',
	'2017-04-30 12:25:15',
	'02:00:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	3, 
	69, 
	1, 
	'Massage thérapeutique',  
	'Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 
	'60.00', 
	'2017-03-14 10:25:15',
	'2017-05-11 08:25:15', 
	'01:00:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	4, 
	13, 
	2, 
	'Réparation de caméra',  
	'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure.', 
	'69.99', 
	'2017-01-14 10:25:15', 
	'2017-05-14 10:25:15',
	'01:00:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
); 

INSERT INTO service VALUES (
	5, 
	48, 
	2, 
	'Changement de pneu',  
	'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 
	'60.00', 
	'2017-03-14 10:25:15',
	'2017-04-30 12:25:15',
	'01:00:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	6, 
	89, 
	2, 
	'Coupe de cheveux',  
	'Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 
	'29.99', 
	'2017-03-14 10:25:15',
	'2017-05-11 08:25:15', 
	'02:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	7, 
	67, 
	2, 
	'Service de traiteur - Buffets',  
	'Service 4 de mon commerce X', 
	'100.00', 
	'2016-02-14 10:25:15', 
	'2017-05-30 5:25:15',
	'01:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
); 

INSERT INTO service VALUES (
	8, 
	52, 
	2, 
	'Déménagement',  
	'Service 5 de mon commerce X', 
	'119.99', 
	'2017-03-14 10:25:15',
	'2018-06-31 20:25:15',
	'02:00:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	9, 
	62, 
	2, 
	'Diagnostic informatique',  
	'Service 6 de mon commerce X', 
	'70.00', 
	'2017-02-14 10:25:15',
	'2018-04-05 08:25:15', 
	'02:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	10, 
	7, 
	2, 
	'Réparation de vitre 10',  
	'Service 7 de mon commerce X', 
	'49.99', 
	'2017-02-14 10:25:15',
	'2018-04-05 08:25:15', 
	'02:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);



INSERT INTO service VALUES (
	11, 
	1, 
	3, 
	'Réparation d\'écran',  
	'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure.', 
	'79.99', 
	'2017-01-14 10:25:15', 
	'2017-05-14 10:25:15',
	'01:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
); 

INSERT INTO service VALUES (
	12, 
	3, 
	3, 
	'Réparation de vitre 12',  
	'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 
	'14.99', 
	'2017-03-14 10:25:15',
	'2017-04-30 12:25:15',
	'02:00:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	13, 
	5, 
	3, 
	'Réparation de porte 13',  
	'Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 
	'14.99', 
	'2017-03-14 10:25:15',
	'2017-05-11 08:25:15', 
	'02:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	14, 
	7, 
	3, 
	'Réparation de porte 14',  
	'Service 4 de mon commerce X', 
	'24.99', 
	'2016-02-14 10:25:15', 
	'2017-05-30 5:25:15',
	'01:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
); 

INSERT INTO service VALUES (
	15, 
	9, 
	3, 
	'Réparation de porte 15',  
	'Service 5 de mon commerce X', 
	'14.99', 
	'2017-03-14 10:25:15',
	'2018-06-31 20:25:15',
	'02:00:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	16, 
	11, 
	3, 
	'Réparation de piscine 16',  
	'Service 6 de mon commerce X', 
	'14.99', 
	'2017-02-14 10:25:15',
	'2018-04-05 08:25:15', 
	'02:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	17, 
	13, 
	3, 
	'Réparation de piscine 17',  
	'Service 7 de mon commerce X', 
	'14.99', 
	'2017-02-14 10:25:15',
	'2018-04-05 08:25:15', 
	'02:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);



INSERT INTO service VALUES (
	18, 
	5, 
	4, 
	'Réparation de piscine 18',  
	'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure.', 
	'24.99', 
	'2017-01-14 10:25:15', 
	'2017-05-14 10:25:15',
	'01:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
); 

INSERT INTO service VALUES (
	19, 
	10, 
	4, 
	'Lunette 19',  
	'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 
	'14.99', 
	'2017-03-14 10:25:15',
	'2017-04-30 12:25:15',
	'02:00:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	20, 
	15, 
	4, 
	'Lunette 20',  
	'Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 
	'14.99', 
	'2017-03-14 10:25:15',
	'2017-05-11 08:25:15', 
	'02:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	21, 
	20, 
	4, 
	'Lunette 21',  
	'Service 4 de mon commerce X', 
	'24.99', 
	'2016-02-14 10:25:15', 
	'2017-05-30 5:25:15',
	'01:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
); 

INSERT INTO service VALUES (
	22, 
	21, 
	4, 
	'Carrosserie 22',  
	'Service 5 de mon commerce X', 
	'14.99', 
	'2017-03-14 10:25:15',
	'2018-06-31 20:25:15',
	'02:00:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	23, 
	22, 
	4, 
	'Carrosserie 23',  
	'Service 6 de mon commerce X', 
	'14.99', 
	'2017-02-14 10:25:15',
	'2018-04-05 08:25:15', 
	'02:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	24, 
	23, 
	4, 
	'Carrosserie 24',  
	'Service 7 de mon commerce X', 
	'14.99', 
	'2017-02-14 10:25:15',
	'2018-04-05 08:25:15', 
	'02:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);



INSERT INTO service VALUES (
	25, 
	12, 
	5, 
	'Peinture 25',  
	'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure.', 
	'24.99', 
	'2017-01-14 10:25:15', 
	'2017-05-14 10:25:15',
	'01:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
); 

INSERT INTO service VALUES (
	26, 
	13, 
	5, 
	'Peinture 26',  
	'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 
	'14.99', 
	'2017-03-14 10:25:15',
	'2017-04-30 12:25:15',
	'02:00:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	27, 
	14, 
	5, 
	'Peinture 27',  
	'Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 
	'14.99', 
	'2017-03-14 10:25:15',
	'2017-05-11 08:25:15', 
	'02:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	28, 
	15, 
	5, 
	'Platre 28',  
	'Service 4 de mon commerce X', 
	'24.99', 
	'2016-02-14 10:25:15', 
	'2017-05-30 5:25:15',
	'01:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
); 

INSERT INTO service VALUES (
	29, 
	16, 
	5, 
	'Platre 29',  
	'Service 5 de mon commerce X', 
	'14.99', 
	'2017-03-14 10:25:15',
	'2018-06-31 20:25:15',
	'02:00:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	30, 
	27, 
	5, 
	'Teinture 30',  
	'Service 6 de mon commerce X', 
	'14.99', 
	'2017-02-14 10:25:15',
	'2018-04-05 08:25:15', 
	'02:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	31, 
	28, 
	5, 
	'Teinture 31',  
	'Service 7 de mon commerce X', 
	'14.99', 
	'2017-02-14 10:25:15',
	'2018-04-05 08:25:15', 
	'02:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);



INSERT INTO service VALUES (
	32, 
	12, 
	6, 
	'Teinture 32',  
	'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure.', 
	'24.99', 
	'2017-01-14 10:25:15', 
	'2017-05-14 10:25:15',
	'01:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
); 

INSERT INTO service VALUES (
	33, 
	11, 
	6, 
	'Réparation mur 33',  
	'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 
	'14.99', 
	'2017-03-14 10:25:15',
	'2017-04-30 12:25:15',
	'02:00:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	34, 
	10, 
	6, 
	'Réparation mur 34',  
	'Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 
	'14.99', 
	'2017-03-14 10:25:15',
	'2017-05-11 08:25:15', 
	'02:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	35, 
	9, 
	6, 
	'Réparation mur 35',  
	'Service 4 de mon commerce X', 
	'24.99', 
	'2016-02-14 10:25:15', 
	'2017-05-30 5:25:15',
	'01:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
); 

INSERT INTO service VALUES (
	36, 
	8, 
	6, 
	'Inspection 36',  
	'Service 5 de mon commerce X', 
	'14.99', 
	'2017-03-14 10:25:15',
	'2018-06-31 20:25:15',
	'02:00:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	37, 
	7, 
	6, 
	'Inspection 37',  
	'Service 6 de mon commerce X', 
	'14.99', 
	'2017-02-14 10:25:15',
	'2018-04-05 08:25:15', 
	'02:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);

INSERT INTO service VALUES (
	38, 
	6, 
	6, 
	'Inspection 38',  
	'Service 7 de mon commerce X', 
	'14.99', 
	'2017-02-14 10:25:15',
	'2018-04-05 08:25:15', 
	'02:30:00',
	'08:30:00',
	'21:30:00',
	'1',
	'1',
	'1',
	'1',
	'1',
	'0',
	'0',
	'1'
);


INSERT INTO service VALUES 
(39, 6, 6, 'Construction divers', 'commerce X', '14.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(40, 7, 7, 'Construction divers', 'commerce X', '14.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(41, 8, 8, 'Construction divers', 'commerce X', '14.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(42, 9, 9, 'Livraison', 'commerce X', '14.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(43, 10, 10, 'Livraison', 'commerce X', '14.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(44, 11, 11, 'Livraison', 'commerce X', '14.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(45, 12, 12, 'Culture', 'commerce X', '44.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(46, 13, 13, 'Culture', 'commerce X', '44.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(47, 14, 14, 'Culture', 'commerce X', '44.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(48, 15, 15, 'Evenements', 'commerce X', '44.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(49, 16, 16, 'Evenements', 'commerce X', '14.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(50, 17, 17, 'Evenements', 'commerce X', '14.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(51, 18, 18, 'Déménagement', 'commerce X', '54.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(52, 19, 19, 'Déménagement', 'commerce X', '14.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(53, 20, 20, 'Déménagement', 'commerce X', '34.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(54, 21, 21, 'Traiteur', 'commerce X', '14.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(55, 22, 22, 'Traiteur', 'commerce X', '34.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(56, 23, 23, 'Traiteur', 'commerce X', '24.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(57, 24, 24, 'Cloture', 'commerce X', '24.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(58, 25, 25, 'Cloture', 'commerce X', '12.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(59, 26, 26, 'Cloture', 'commerce X', '14.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(60, 27, 27, 'Coiffure', 'commerce X', '13.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(61, 28, 28, 'Coiffure', 'commerce X', '14.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');
INSERT INTO service VALUES 
(62, 29, 29, 'Coiffure', 'commerce X', '15.99','2017-02-14 10:25:15','2018-04-05 08:25:15', '01:00:00','08:30:00',	'21:30:00',	'1','1','1','1','1','0','0','1');





COMMIT;