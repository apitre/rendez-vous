BEGIN;

INSERT INTO address VALUES
(1, 5000, 'labelle', 2, 'Montréal', 'Québec', 'H1C 2C2');
INSERT INTO address VALUES
(2, 5111, 'Barclay', 9, 'Longueuil', 'Québec', 'H3W 1E3');
INSERT INTO address VALUES
(3, 565, 'maisonneuve', 4, 'Laval', 'Québec', 'H1C 4R4');
INSERT INTO address VALUES
(4, 4322, 'rene', 6, 'Longueuil', 'Québec', 'H1C 2C2');
INSERT INTO address VALUES
(5, 300, 'sophie', 2, 'Drummondville', 'Québec', 'J0K 2C2');
INSERT INTO address VALUES
(6, 4, 'coconut', 3, 'st-lambert', 'Québec', 'H1C 4R4');
INSERT INTO address VALUES
(7, 777, 'beauchamp', 9, 'Montréal', 'Québec', 'H1C 2C2');
INSERT INTO address VALUES
(8, 6, 'beausejour', 3, 'Montréal', 'Québec', 'H1C 2C2');
INSERT INTO address VALUES
(9, 333, 'laval', 7, 'Levis', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(10, 5555, 'charton', 4, 'Quebec', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(11, 2, 'laval', 7, 'Levis', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(12, 7, 'fleury', 4, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(13, 337, 'fleury', 3, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(14, 704, 'fleury', 2, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(15, 300, 'sauve', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(16, 4, 'coconut2', 3, 'st-lambert', 'Québec', 'H1C 4R4');
INSERT INTO address VALUES
(17, 777, 'beauchamp', 9, 'Montréal', 'Québec', 'H1C 2C2');
INSERT INTO address VALUES
(18, 6, 'beausejour2', 3, 'Montréal', 'Québec', 'H1C 2C2');
INSERT INTO address VALUES
(19, 333, 'laval2', 7, 'Levis', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(20, 5555, 'charton2', 4, 'Quebec', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(21, 2, 'laval2', 7, 'Levis', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(22, 7, 'fleury2', 4, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(23, 337, 'fleury2', 3, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(24, 704, 'fleury2', 2, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(25, 300, 'sauve2', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(26, 555, 'sauve2', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(27, 444, 'sauve2', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(28, 333, 'sauve2', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(29, 222, 'sauve2', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(30, 5555, 'charton2', 4, 'Quebec', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(31, 2, 'laval2', 7, 'Levis', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(32, 3, 'fleury2', 4, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(33, 2, 'fleury2', 3, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(34, 1, 'fleury2', 2, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(35, 300, 'hello', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(36, 555, 'hello', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(37, 444, 'hello', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(38, 333, 'hello', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(39, 222, 'hello', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(40, 5555, 'charton2', 4, 'Quebec', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(41, 2, 'laval2', 7, 'Levis', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(42, 3, 'fleury2', 4, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(43, 2, 'fleury2', 3, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(44, 1, 'fleury2', 2, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(45, 300, 'hello', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(46, 555, 'hello', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(47, 444, 'hello', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(48, 333, 'hello', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(49, 222, 'hello', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(50, 5555, 'charton2', 4, 'Quebec', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(51, 2, 'laval2', 7, 'Levis', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(52, 3, 'fleury2', 4, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(53, 2, 'fleury2', 3, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(54, 1, 'fleury2', 2, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(55, 300, 'hello', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(56, 555, 'hello', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(57, 444, 'hello', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(58, 333, 'hello', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(59, 222, 'hello', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(60, 5555, 'charton2', 4, 'Quebec', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(61, 2, 'laval2', 7, 'Levis', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(62, 3, 'fleury2', 4, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(63, 2, 'fleury2', 3, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(64, 1, 'fleury2', 2, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(65, 300, 'tremblant', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(66, 555, 'tremblant', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(67, 444, 'tremblant', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(68, 333, 'tremblant', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(69, 222, 'tremblant', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(70, 5555, 'charton2', 4, 'Quebec', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(71, 2, 'laval2', 7, 'Levis', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(72, 3, 'fleury2', 4, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(73, 2, 'fleury2', 3, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(74, 1, 'fleury2', 2, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(75, 300, 'ny', 1, 'Laval', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(76, 555, 'ny', 1, 'Laval', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(77, 444, 'ny', 1, 'Laval', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(78, 333, 'ny', 1, 'Laval', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(79, 222, 'ny', 1, 'Laval', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(80, 5555, 'charton2', 4, 'Quebec', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(81, 2, 'laval2', 7, 'Levis', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(82, 3, 'fleury2', 4, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(83, 2, 'fleury2', 3, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(84, 1, 'fleury2', 2, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(85, 300, 'boston', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(86, 555, 'boston', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(87, 444, 'boston', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(88, 333, 'boston', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(89, 222, 'boston', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(90, 5555, 'charton2', 4, 'Quebec', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(91, 2, 'laval2', 7, 'Levis', 'Québec', 'J0K 9F4');
INSERT INTO address VALUES
(92, 3, 'fleury2', 4, 'Laval', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(93, 2, 'fleury2', 3, 'Laval', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(94, 1, 'fleury2', 2, 'Laval', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(95, 300, 'paul', 1, 'Laval', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(96, 555, 'paul', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(97, 444, 'paul', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(98, 333, 'paul', 1, 'Montreal', 'Québec', 'H1C 9F4');
INSERT INTO address VALUES
(99, 222, 'paul', 1, 'Montreal', 'Québec', 'H1C 9F4');

COMMIT;