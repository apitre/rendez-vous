BEGIN;  

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(1, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 1 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(2, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), 'message 22 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(3, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), 'message 55 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(4, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 8 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 77 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '4');	
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(5, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 44 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(6, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), 'message 33 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '4');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(7, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 15 MINUTE), 'message 11 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '3');
				
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(8, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), 'message 22 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(9, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 HOUR), 'message 33 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '1');
			
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(10, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 4 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 5 HOUR), 'message 333 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '0');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(11, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 222 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(12, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 24 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 25 HOUR), 'message 111 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(13, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 26 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 27 HOUR), 'message 222 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(14, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 28 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 29 HOUR), 'message 99 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(15, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 30 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 31 HOUR), 'message 88 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(16, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 48 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 49 HOUR), 'message 22 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(17, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 50 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 51 HOUR), 'message 11 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(18, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 52 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 53 HOUR), 'message 22 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '1');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(19, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 54 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 55 HOUR), 'message 55 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(20, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 72 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 74 HOUR), 'message 66 pour commercant', 1, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(21, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 1 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(22, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), 'message 22 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(23, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), 'message 55 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(24, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 8 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 77 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '4');	
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(25, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 44 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(26, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), 'message 33 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '4');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(27, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 15 MINUTE), 'message 11 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '3');
				
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(28, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), 'message 22 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(29, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 HOUR), 'message 33 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '1');
			
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(30, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 4 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 5 HOUR), 'message 333 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '0');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(31, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 222 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(32, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 24 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 25 HOUR), 'message 111 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(33, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 26 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 27 HOUR), 'message 222 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(34, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 28 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 29 HOUR), 'message 99 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(35, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 30 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 31 HOUR), 'message 88 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(36, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 48 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 49 HOUR), 'message 22 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(37, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 50 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 51 HOUR), 'message 11 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(38, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 52 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 53 HOUR), 'message 22 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '1');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(39, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 54 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 55 HOUR), 'message 55 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(40, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 72 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 74 HOUR), 'message 66 pour commercant', 2, FLOOR(1 + (RAND() * 5)), '1');

	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(41, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 1 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(42, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), 'message 22 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(43, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), 'message 55 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(44, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 8 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 77 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '4');	
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(45, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 44 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(46, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), 'message 33 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '4');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(47, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 15 MINUTE), 'message 11 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '3');
				
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(48, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), 'message 22 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(49, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 HOUR), 'message 33 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '1');
			
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(50, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 4 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 5 HOUR), 'message 333 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '0');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(51, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 222 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(52, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 24 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 25 HOUR), 'message 111 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(53, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 26 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 27 HOUR), 'message 222 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(54, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 28 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 29 HOUR), 'message 99 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(55, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 30 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 31 HOUR), 'message 88 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(56, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 48 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 49 HOUR), 'message 22 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(57, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 50 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 51 HOUR), 'message 11 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(58, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 52 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 53 HOUR), 'message 22 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '1');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(59, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 54 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 55 HOUR), 'message 55 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(60, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 72 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 74 HOUR), 'message 66 pour commercant', 3, FLOOR(1 + (RAND() * 5)), '1');

	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(61, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 1 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(62, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), 'message 22 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(63, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), 'message 55 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(64, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 8 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 77 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '4');	
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(65, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 44 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(66, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), 'message 33 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '4');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(67, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 15 MINUTE), 'message 11 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '3');
				
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(68, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), 'message 22 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(69, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 HOUR), 'message 33 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '1');
			
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(70, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 4 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 5 HOUR), 'message 333 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '0');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(71, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 222 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(72, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 24 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 25 HOUR), 'message 111 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(73, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 26 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 27 HOUR), 'message 222 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(74, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 28 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 29 HOUR), 'message 99 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(75, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 30 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 31 HOUR), 'message 88 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(76, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 48 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 49 HOUR), 'message 22 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(77, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 50 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 51 HOUR), 'message 11 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(78, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 52 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 53 HOUR), 'message 22 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '1');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(79, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 54 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 55 HOUR), 'message 55 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(80, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 72 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 74 HOUR), 'message 66 pour commercant', 4, FLOOR(1 + (RAND() * 5)), '1');

	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(81, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 1 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(82, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), 'message 22 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(83, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), 'message 55 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(84, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 8 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 77 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '4');	
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(85, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 44 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(86, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), 'message 33 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '4');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(87, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 15 MINUTE), 'message 11 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '3');
				
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(88, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), 'message 22 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(89, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 HOUR), 'message 33 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '1');
			
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(90, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 4 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 5 HOUR), 'message 333 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '0');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(91, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 222 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(92, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 24 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 25 HOUR), 'message 111 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(93, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 26 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 27 HOUR), 'message 222 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(94, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 28 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 29 HOUR), 'message 99 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(95, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 30 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 31 HOUR), 'message 88 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(96, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 48 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 49 HOUR), 'message 22 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(97, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 50 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 51 HOUR), 'message 11 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(98, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 52 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 53 HOUR), 'message 22 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '1');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(99, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 54 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 55 HOUR), 'message 55 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(100, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 72 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 74 HOUR), 'message 66 pour commercant', 5, FLOOR(1 + (RAND() * 5)), '1');

		
		
		
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(101, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 222 pour commercant', 36, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(102, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 24 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 25 HOUR), 'message 111 pour commercant', 36, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(103, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 26 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 27 HOUR), 'message 222 pour commercant', 36, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(104, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 28 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 29 HOUR), 'message 99 pour commercant', 36, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(105, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 30 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 31 HOUR), 'message 88 pour commercant', 36, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(106, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 48 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 49 HOUR), 'message 22 pour commercant', 36, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(107, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 50 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 51 HOUR), 'message 11 pour commercant', 36, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(108, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 52 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 53 HOUR), 'message 22 pour commercant', 36, FLOOR(1 + (RAND() * 5)), '1');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(109, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 54 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 55 HOUR), 'message 55 pour commercant', 36, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(110, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 72 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 74 HOUR), 'message 66 pour commercant', 36, FLOOR(1 + (RAND() * 5)), '1');
	
	
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(111, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 1 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(112, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), 'message 22 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(113, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), 'message 55 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(114, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 8 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 77 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '4');	
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(115, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 44 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(116, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), 'message 33 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '4');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(117, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 15 MINUTE), 'message 11 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '3');
				
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(118, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), 'message 22 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(119, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 HOUR), 'message 33 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '1');
			
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(120, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 4 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 5 HOUR), 'message 333 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '0');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(121, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 222 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(122, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 24 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 25 HOUR), 'message 111 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(123, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 26 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 27 HOUR), 'message 222 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(124, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 28 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 29 HOUR), 'message 99 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(125, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 30 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 31 HOUR), 'message 88 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(126, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 48 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 49 HOUR), 'message 22 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(127, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 50 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 51 HOUR), 'message 11 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(128, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 52 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 53 HOUR), 'message 22 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '1');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(129, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 54 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 55 HOUR), 'message 55 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(130, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 72 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 74 HOUR), 'message 66 pour commercant', 6, FLOOR(1 + (RAND() * 5)), '1');

	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(131, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 1 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(132, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), 'message 22 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(133, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), 'message 55 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(134, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 8 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 77 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '4');	
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(135, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 44 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(136, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), 'message 33 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '4');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(137, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 15 MINUTE), 'message 11 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '3');
				
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(138, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), 'message 22 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(139, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 HOUR), 'message 33 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '1');
			
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(140, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 4 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 5 HOUR), 'message 333 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '0');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(141, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 222 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(142, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 24 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 25 HOUR), 'message 111 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(143, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 26 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 27 HOUR), 'message 222 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(144, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 28 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 29 HOUR), 'message 99 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(145, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 30 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 31 HOUR), 'message 88 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(146, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 48 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 49 HOUR), 'message 22 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(147, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 50 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 51 HOUR), 'message 11 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(148, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 52 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 53 HOUR), 'message 22 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '1');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(149, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 54 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 55 HOUR), 'message 55 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(150, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 72 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 74 HOUR), 'message 66 pour commercant', 7, FLOOR(1 + (RAND() * 5)), '1');

	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(151, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 1 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(152, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), 'message 22 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(153, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), 'message 55 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(154, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 8 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 77 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '4');	
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(155, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 44 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(156, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), 'message 33 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '4');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(157, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 15 MINUTE), 'message 11 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '3');
				
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(158, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), 'message 22 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(159, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 HOUR), 'message 33 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '1');
			
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(160, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 4 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 5 HOUR), 'message 333 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '0');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(161, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 222 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(162, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 24 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 25 HOUR), 'message 111 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(163, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 26 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 27 HOUR), 'message 222 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(164, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 28 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 29 HOUR), 'message 99 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(165, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 30 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 31 HOUR), 'message 88 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(166, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 48 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 49 HOUR), 'message 22 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(167, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 50 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 51 HOUR), 'message 11 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(168, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 52 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 53 HOUR), 'message 22 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '1');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(169, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 54 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 55 HOUR), 'message 55 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(170, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 72 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 74 HOUR), 'message 66 pour commercant', 8, FLOOR(1 + (RAND() * 5)), '1');

	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(171, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 1 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(172, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), 'message 22 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(173, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), 'message 55 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(174, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 8 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 77 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '4');	
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(175, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 44 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(176, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), 'message 33 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '4');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(177, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 15 MINUTE), 'message 11 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '3');
				
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(178, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), 'message 22 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(179, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 HOUR), 'message 33 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '1');
			
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(180, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 4 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 5 HOUR), 'message 333 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '0');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(181, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 222 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(182, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 24 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 25 HOUR), 'message 111 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(183, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 26 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 27 HOUR), 'message 222 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(184, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 28 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 29 HOUR), 'message 99 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(185, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 30 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 31 HOUR), 'message 88 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(186, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 48 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 49 HOUR), 'message 22 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(187, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 50 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 51 HOUR), 'message 11 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(188, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 52 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 53 HOUR), 'message 22 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '1');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(189, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 54 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 55 HOUR), 'message 55 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(190, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 72 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 74 HOUR), 'message 66 pour commercant', 9, FLOOR(1 + (RAND() * 5)), '1');

	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(191, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 1 pour commercant', 10, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(192, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), 'message 22 pour commercant', 10, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(193, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), 'message 55 pour commercant', 10, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(194, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 8 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 77 pour commercant', 10, FLOOR(1 + (RAND() * 5)), '4');	
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(195, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 44 pour commercant', 10, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(196, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), 'message 33 pour commercant', 10, FLOOR(1 + (RAND() * 5)), '4');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(197, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 15 MINUTE), 'message 11 pour commercant', 10, FLOOR(1 + (RAND() * 5)), '3');
				
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(198, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), 'message 22 pour commercant', 10, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(199, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 HOUR), 'message 33 pour commercant', 10, FLOOR(1 + (RAND() * 5)), '1');
			
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(200, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 4 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 5 HOUR), 'message 333 pour commercant', 10, FLOOR(1 + (RAND() * 5)), '0');
		
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(201, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 222 pour commercant', 35, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(202, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 24 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 25 HOUR), 'message 111 pour commercant', 35, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(203, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 26 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 27 HOUR), 'message 222 pour commercant', 35, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(204, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 28 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 29 HOUR), 'message 99 pour commercant', 35, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(205, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 30 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 31 HOUR), 'message 88 pour commercant', 35, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(206, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 48 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 49 HOUR), 'message 22 pour commercant', 35, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(207, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 50 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 51 HOUR), 'message 11 pour commercant', 35, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(208, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 52 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 53 HOUR), 'message 22 pour commercant', 35, FLOOR(1 + (RAND() * 5)), '1');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(209, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 54 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 55 HOUR), 'message 55 pour commercant', 35, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(210, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 72 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 74 HOUR), 'message 66 pour commercant', 35, FLOOR(1 + (RAND() * 5)), '1');
	
	
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(211, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 1 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(212, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), 'message 22 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(213, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), 'message 55 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(214, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 8 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 77 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '4');	
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(215, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 44 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(216, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), 'message 33 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '4');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(217, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 15 MINUTE), 'message 11 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '3');
				
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(218, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), 'message 22 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(219, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 HOUR), 'message 33 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '1');
			
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(220, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 4 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 5 HOUR), 'message 333 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '0');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(221, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 222 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(222, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 24 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 25 HOUR), 'message 111 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(223, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 26 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 27 HOUR), 'message 222 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(224, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 28 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 29 HOUR), 'message 99 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(225, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 30 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 31 HOUR), 'message 88 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(226, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 48 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 49 HOUR), 'message 22 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(227, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 50 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 51 HOUR), 'message 11 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(228, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 52 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 53 HOUR), 'message 22 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '1');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(229, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 54 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 55 HOUR), 'message 55 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(230, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 72 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 74 HOUR), 'message 66 pour commercant', 11, FLOOR(1 + (RAND() * 5)), '1');

	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(231, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 1 pour commercant', 12,  FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(232, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), 'message 22 pour commercant', 12,  FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(233, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), 'message 55 pour commercant', 12,  FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(234, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 8 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 77 pour commercant', 12,  FLOOR(1 + (RAND() * 5)), '4');	
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(235, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 44 pour commercant', 12,  FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(236, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), 'message 33 pour commercant', 12,  FLOOR(1 + (RAND() * 5)), '4');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(237, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 15 MINUTE), 'message 11 pour commercant', 12,  FLOOR(1 + (RAND() * 5)), '3');
				
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(238, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), 'message 22 pour commercant', 12,  FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(239, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 HOUR), 'message 33 pour commercant', 12,  FLOOR(1 + (RAND() * 5)), '1');
			
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(240, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 4 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 5 HOUR), 'message 333 pour commercant', 12,  FLOOR(1 + (RAND() * 5)), '0');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(241, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 222 pour commercant', 12,  FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(242, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 24 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 25 HOUR), 'message 111 pour commercant', 12,  FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(243, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 26 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 27 HOUR), 'message 222 pour commercant', 12,  FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(244, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 28 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 29 HOUR), 'message 99 pour commercant', 12,  FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(245, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 30 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 31 HOUR), 'message 88 pour commercant', 12,  FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(246, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 48 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 49 HOUR), 'message 22 pour commercant', 12,  FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(247, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 50 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 51 HOUR), 'message 11 pour commercant', 12,  FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(248, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 52 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 53 HOUR), 'message 22 pour commercant', 12,  FLOOR(1 + (RAND() * 5)), '1');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(249, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 54 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 55 HOUR), 'message 55 pour commercant', 12,  FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(250, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 72 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 74 HOUR), 'message 66 pour commercant', 12, FLOOR(1 + (RAND() * 5)), '1');

	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(251, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 1 pour commercant', 13,  FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(252, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), 'message 22 pour commercant', 13,  FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(253, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), 'message 55 pour commercant', 13,  FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(254, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 8 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 77 pour commercant', 13,  FLOOR(1 + (RAND() * 5)), '4');	
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(255, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 44 pour commercant', 13,  FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(256, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), 'message 33 pour commercant', 13,  FLOOR(1 + (RAND() * 5)), '4');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(257, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 15 MINUTE), 'message 11 pour commercant', 13,  FLOOR(1 + (RAND() * 5)), '3');
				
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(258, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), 'message 22 pour commercant', 13,  FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(259, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 HOUR), 'message 33 pour commercant', 13,  FLOOR(1 + (RAND() * 5)), '1');
			
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(260, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 4 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 5 HOUR), 'message 333 pour commercant', 13,  FLOOR(1 + (RAND() * 5)), '0');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(261, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 222 pour commercant', 13,  FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(262, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 24 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 25 HOUR), 'message 111 pour commercant', 13,  FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(263, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 26 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 27 HOUR), 'message 222 pour commercant', 13,  FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(264, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 28 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 29 HOUR), 'message 99 pour commercant', 13,  FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(265, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 30 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 31 HOUR), 'message 88 pour commercant', 13,  FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(266, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 48 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 49 HOUR), 'message 22 pour commercant', 13,  FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(267, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 50 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 51 HOUR), 'message 11 pour commercant', 13,  FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(268, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 52 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 53 HOUR), 'message 22 pour commercant', 13,  FLOOR(1 + (RAND() * 5)), '1');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(269, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 54 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 55 HOUR), 'message 55 pour commercant', 13,  FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(270, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 72 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 74 HOUR), 'message 66 pour commercant', 13, FLOOR(1 + (RAND() * 5)), '1');

	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(271, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 1 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(272, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), 'message 22 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(273, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 3 DAY), 'message 55 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '4');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(274, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 8 HOUR), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 77 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '4');	
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(275, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 2 DAY), 'message 44 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '4');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(276, SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), SUBDATE(CURRENT_TIMESTAMP, INTERVAL 1 DAY), 'message 33 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '4');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(277, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 15 MINUTE), 'message 11 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '3');
				
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(278, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 1 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), 'message 22 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(279, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 2 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 3 HOUR), 'message 33 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '1');
			
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(280, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 4 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 5 HOUR), 'message 333 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '0');
		
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(281, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 6 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 7 HOUR), 'message 222 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(282, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 24 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 25 HOUR), 'message 111 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(283, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 26 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 27 HOUR), 'message 222 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(284, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 28 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 29 HOUR), 'message 99 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(285, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 30 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 31 HOUR), 'message 88 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(286, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 48 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 49 HOUR), 'message 22 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '0');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(287, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 50 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 51 HOUR), 'message 11 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(288, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 52 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 53 HOUR), 'message 22 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '1');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(289, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 54 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 55 HOUR), 'message 55 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '1');
	
	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES 
	(290, ADDDATE(CURRENT_TIMESTAMP, INTERVAL 72 HOUR), ADDDATE(CURRENT_TIMESTAMP, INTERVAL 74 HOUR), 'message 66 pour commercant', 14, FLOOR(1 + (RAND() * 5)), '1');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES  
	(291, "2017-03-01 08:30:00", "2017-03-01 10:00:00", 'Message test1 conflict', '1', '1', '1');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES  
	(292, "2017-03-01 10:00:00", "2017-03-01 11:30:00", 'message test2 conflict', '1', '1', '1');

	INSERT INTO appointment (id, start_date, end_date, text, service_id, consumer_id, status) VALUES  
	(293, "2017-03-01 17:29:00", "2017-03-01 19:10:00", 'message test3 conflict', '1', '1', '1');
	
COMMIT;


