CREATE TABLE address (
	address_id int NOT NULL auto_increment,
   	address int NOT NULL,
   	street varchar(256) NOT NULL,
   	appartment int default null,
	city varchar(30) default '',
	province varchar(30) default '',
	zip_code varchar(30) default '',
	PRIMARY KEY (address_id)
);

CREATE TABLE user (
	user_id int NOT NULL auto_increment,
   	lastname varchar(30) NOT NULL,
   	firstname varchar(30) NOT NULL,
   	username varchar(30) NOT NULL UNIQUE,
	email varchar(255) NOT NULL UNIQUE,
	type set('0', '1', '2') default '0',
	creation_ts datetime default '0000-00-00 00:00:00',
	status set('0', '1', '2') default '0',
	phone varchar(30) default '',
	birthdate date default '0000-00-00',
  	address_id int default 0,
	PRIMARY KEY (user_id),
	FOREIGN KEY (address_id) REFERENCES address(address_id)
);

CREATE TABLE user_pending (
	unique_key varchar(255) NOT NULL UNIQUE,
	user_id int NOT NULL,
	FOREIGN KEY (user_id) REFERENCES user(user_id)
);

CREATE TABLE user_access (
	user_id int NOT NULL,
	salt varchar(256) NOT NULL,
   	token varchar(256) NOT NULL,
	FOREIGN KEY (user_id) REFERENCES user(user_id)
);

CREATE TABLE merchant (
	merchant_id int NOT NULL auto_increment,
	user_id int NOT NULL default 0,
	name varchar(255) NOT NULL,
	address_id int default 0,
	email varchar(255) default '',
	phone varchar(30) default '',
	PRIMARY KEY (merchant_id),
	FOREIGN KEY (user_id) REFERENCES user(user_id),
	FOREIGN KEY (address_id) REFERENCES address(address_id)
);

CREATE TABLE consumer (
	consumer_id int NOT NULL auto_increment,
	user_id int NOT NULL default 0,
	PRIMARY KEY (consumer_id),
	FOREIGN KEY (user_id) REFERENCES user(user_id)
);

CREATE TABLE service_domain (
	domain_id int NOT NULL auto_increment,
	name varchar(256) NOT NULL,
   	description text NOT NULL,
	PRIMARY KEY (domain_id)
);

CREATE TABLE service_category (
	category_id int NOT NULL auto_increment,
	domain_id int NOT NULL,
	name varchar(256) NOT NULL,
   	description text NOT NULL,
	PRIMARY KEY (category_id),
	FOREIGN KEY (domain_id) REFERENCES service_domain(domain_id)
);

CREATE TABLE service (
	service_id int NOT NULL auto_increment,
	category_id int NOT NULL,
	merchant_id int NOT NULL,
   	name varchar(256) NOT NULL,
   	description text NOT NULL,
	price decimal(8, 2) NOT NULL,
	date_from datetime NOT NULL,
	date_to datetime NOT NULL,
	length time NOT NULL,
    hour_begin time NOT NULL,
    hour_end time NOT NULL,
    monday set('0', '1') default '0',
    tuesday set('0', '1') default '0',
    wednesday set('0', '1') default '0',
    thursday set('0', '1') default '0',
    friday set('0', '1') default '0',
    saturday set('0', '1') default '0',
    sunday set('0', '1') default '0',
	status set('1', '2') default '1',
	PRIMARY KEY (service_id),
	FOREIGN KEY (category_id) REFERENCES service_category(category_id),
	FOREIGN KEY (merchant_id) REFERENCES merchant(merchant_id)
);

CREATE TABLE rating (
	rating_id int NOT NULL auto_increment,
   	service_id int NOT NULL,
   	consumer_id int NOT NULL,
   	score set('0', '1', '2', '3', '4', '5') default '0',
	comment text,
	PRIMARY KEY (rating_id),
	FOREIGN KEY (service_id) REFERENCES service(service_id)
);

CREATE TABLE appointment (
	id int NOT NULL AUTO_INCREMENT,
	start_date datetime NOT NULL,
	end_date datetime NOT NULL,
	`text` varchar(255) NOT NULL,
	service_id int NOT NULL,
	consumer_id int NOT NULL,
	status set('0', '1', '2', '3', '4') default '0',
	PRIMARY KEY (id),
	FOREIGN KEY (consumer_id) REFERENCES consumer(consumer_id),
	FOREIGN KEY (service_id) REFERENCES service(service_id)
);