<?php

namespace App\Common;

class TemplatesName {

    // Nom des templates utilisés
    const LOGIN = "_login.html";
    const ACTIVATE = "member_activate.html";
    const REGISTRATION_TYPE = "_choice.html";
    const REGISTRATION_CONSUMER = "_register.html";
    const REGISTRATION_MERCHANT = "_register.html";
    const CONFIRMATION_CONSUMER = "member_confirmation.html";
    const CONFIRMATION_MERCHANT = "member_confirmation.html";
    const CONFIRMATION_MEMBER = "member_confirmation_email.html";
    const CONFIRMATION_BOOKING = "booking_confirmation_email.html";
    const HOME_MERCHANT = "member_home.html";
    const HOME_CONSUMER = "member_home.html";
    const MODIFY_PARAMETERS_CONSUMER = "member_modify.html";
    const MODIFY_PARAMETERS_MERCHANT = "member_modify.html";
    const CREATE_SERVICE = "merchant_new_service.html";
    const MODIFY_SERVICE = "merchant_modify_service.html";
    const MERCHANT_SERVICES = "merchant_services.html";
    const SERVICE_RATINGS = "merchant_comments.html";
    const MERCHANT_SUMMARY = "merchant_summary.html";
    const CALENDAR = "calendar.html";
    const SEARCH_SERVICE = "consumer_search_service.html";
    const VIEW_SERVICE = "consumer_view_service.html";
    const CONSUMER_SUMMARY = "consumer_summary.html";
    const POPULAR_SERVICES = "popular_services.html";
    
    // ASUPPRIMER : les template de tests devraient pas faire partie de la liste des template officiel
    const TEST_SARAH = "test.html";
}
