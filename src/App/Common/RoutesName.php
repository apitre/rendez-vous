<?php

namespace App\Common;

class RoutesName {

    // Défaut 
    const HOME = "/";
    // Sélection du type l'utilisateur 
    const REGISTRATION_TYPE = "/registration/choice";
    // Authentification de l'utilisateur 
    const SIGNIN_USER = "/signin";
    // Mettre fin a la session de l'utilisateur 
    const SIGNOUT_USER = "/signout";
    // Désactivation du compte de l'utilisateur
    const DEACTIVATE_ACCOUNT = "/deactivate";
    // Validation de l'utilisateur par email/token
    const ACTIVATE_USER = "/activate";
    // Défaut pour le consommateur
    const CONSUMER = "/consumer";
    // Défaut pour le commcercant
    const MERCHANT = "/merchant";
    // Page accueil de l'utilisateur 
    const USER_HOME = "/home";
    // Page d'enregistrement de l'utilisateur 
    const USER_REGISTRATION = "/registration";
    // Enregistrement de l'utilisateur
    const USER_REGISTER = "/register";
    // Confirmation de l'enregistrement de l'utilisateur 
    const USER_CONFIRMATION = "/confirmation";
    // Page de modification des paramètres de l'utilisateur 
    const USER_MODIFICATION = "/modification";
    // Mise a jour des paramètres de l'utilisateur
    const USER_MODIFY = "/modify";
        
    // Page de création d'un nouveau service 
    const SERVICE_CREATION = "/service/creation";
    // Enregistrement du service
    const SERVICE_REGISTER = "/service/register";
    // Page de modification d'un service 
    const SERVICE_MODIFICATION = "/service/modification";
    // Mise a jour du service
    const SERVICE_MODIFY = "/service/modify";
    // Supprimer le service
    const SERVICE_DELETE = "/service/delete";
    // Obtenir la liste des Categories group by Domains
    const SERVICE_DOMAINS = "/service/domains";
    // Obtenir la liste des Categories group by Domains
    const SERVICE_CATEGORIES = "/service/categories";
    // Listes des services commercant
    const SERVICES = "/services";
    // Obtenir la liste des rendez-vous
    const SERVICE_APPOINTMENTS_SUMMARY = "/summary";
    // Calendrier 
    const CALENDAR = "/calendar";
    const CALENDAR_LOAD = "/calendar/init";
    const CALENDAR_EDIT = "/calendar/edit";

    // Listes des évaluations de tous les services
    const SERVICE_RATINGS = "/service/ratings";
    // Recherche de services 
    const SERVICE_SEARCH = "/service/search";
    const SERVICE_SEARCHING = "/service/searching";
    // Consultation info service 
    const SERVICE_VIEW = "/service/view";
    // Prise de rendez-vous pour un service
    const SERVICE_BOOKING = "/service/booking";
    
    // Liste des services populaires tout commercant confondus
    const POPULAR_SERVICES = "/popular";
    
    // A SUPPRIMER : les routes de tests devraient pas faire partie de la liste des routes officielles
    const TEST_SARAH = "/test";

    public static function getAll()
    {
        $reflector = new \ReflectionClass(__CLASS__);
        return (object)$reflector->getConstants();
    }
}
