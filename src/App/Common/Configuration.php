<?php

namespace App\Common;

class Configuration
{
    private $emailConfirmationEnabled = true; // Confirmation par courriel activé/désactivé
    private $settings = [];

    public function __construct($settings)
    {
    	$this->settings = $settings;
    }

    public function isLocal()
    {
    	return $this->settings['env'] == "local";
    }

    public function isProduction()
    {
    	return $this->settings['env'] == "heroku";
    }
    
    public function getDomain()
    {   
    	return $this->settings['domain'];
    }
    
    public function isEmailConfirmationEnabled()
    {
        return $this->emailConfirmationEnabled;
    }
}

