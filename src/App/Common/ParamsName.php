<?php

namespace App\Common;

// Liste des paramètres des requetes HTTP.

class ParamsName
{
    // Page d'authentification d'un utilisateur
    const AUTH_USERNAME  			= "username";
    const AUTH_PASSWORD                  	= "password";
    const AUTH_TOKEN                        = "token";

    // Page d'enregistrement/modification du consommateur et du commercant
    const REGISTER_FIRSTNAME             	= "firstname";
    const REGISTER_LASTNAME              	= "lastname";
    const REGISTER_BIRTHDATE            	= "birthdate";
    const REGISTER_HOMEPHONE             	= "phone";
    const REGISTER_EMAIL                 	= "mail";
    const REGISTER_PASSWORD                 = "password";
    const REGISTER_USERNAME              	= "username";
    const REGISTER_HOMEADDRNUM           	= "adresse";
    const REGISTER_HOMEADDRSTREET        	= "street";
    const REGISTER_HOMEADDRAPPART        	= "app";
    const REGISTER_HOMEADDRTOWN          	= "town";
    const REGISTER_HOMEADDRCODE          	= "zip_code";
    const REGISTER_HOMEADDRPROVINCE      	= "province";

    // Page d'enregistrement/modification du commercant
    const REGISTER_BUSINESSNAME          	= "company_name";
    const REGISTER_BUSINESSPHONE         	= "business_phone";
    const REGISTER_BUSINESSMAIL          	= "business_mail";
    const REGISTER_BUSINESSADDRNUM       	= "business_adresse";
    const REGISTER_BUSINESSADDRSTREET    	= "business_street";
    const REGISTER_BUSINESSADDRAPPART    	= "business_app";
    const REGISTER_BUSINESSADDRTOWN      	= "business_town";
    const REGISTER_BUSINESSADDRCODE      	= "business_zip_code";
    const REGISTER_BUSINESSADDRPROVINCE  	= "business_province";

    // Page modification de l'image de l'utilisateur
    const MODIFY_PARAM_USER_IMAGE               = "userImage";

    // Page de creation/modification d'un service
    const SERVICE_NAME                    	= "name";
    const SERVICE_DESCRIPTION              	= "description";
    const SERVICE_DOMAIN          		= "domain";
    const SERVICE_CATEGORY                      = "category";
    const SERVICE_PRICE                         = "price";
    const SERVICE_DATE_BEGIN                    = "dateBegin";
    const SERVICE_DATE_END                      = "dateEnd";
    const SERVICE_HOUR_BEGIN               	= "hourBegin";
    const SERVICE_HOUR_END                      = "hourEnd";
    const SERVICE_LENGTH                  	= "length";
    const SERVICE_MONDAY                        = "monday";
    const SERVICE_THUESDAY 			= "tuesday";
    const SERVICE_WEDNESDAY 			= "wednesday";
    const SERVICE_THURSDAY                      = "thursday";
    const SERVICE_FRIDAY 			= "friday";
    const SERVICE_SATURDAY 			= "saturday";
    const SERVICE_SUNDAY 			= "sunday";

    // Page de modification d'un service / consultation des rendez-vous
    const SERVICE_ID                            = "id";
    
    // Prise de rendez-vous
    const APPOINTMENT_SERVICE_ID                = "serviceId";
    const APPOINTMENT_DATE_BEGIN                = "dateBegin";
    const APPOINTMENT_DATE_END                  = "dateEnd";
    const APPOINTMENT_COMMENT                   = "comment";
    
    
    
}

