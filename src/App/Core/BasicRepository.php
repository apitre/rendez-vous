<?php

namespace App\Core;

use \App\Core\SQLDatabase;

//
// Classe de base pour le dépot de données
//
abstract class BasicRepository 
{
    protected $db = null;
    
    public function __construct(SQLDatabase $db) 
    {
        $this->db = $db;
    }

    abstract public function add($object);
    abstract public function update($object);
    abstract public function getInfoFromId($id);
} 