<?php

namespace App\Core;

class Status
{   
    public $type = "";
    public $message = "";

    public function __construct($type, $message)
    {
        $this->type = $type;
        $this->message = $message;
    }
}