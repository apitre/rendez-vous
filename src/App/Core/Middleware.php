<?php

namespace App\Core;

use \App\Common\RoutesName;

class Middleware
{   
    //
    // Protection accès routes : Route middleware
    // Permet de s'assurer que l'utilisateur est authentifié lors de l'accès aux routes
    //
    public static function loginVerify($redirect = "") 
    {
        return function($request, $response, $next) use ($redirect) {
        	if (empty($redirect)) {
        		$redirect = RoutesName::HOME;
        	}

            if ($this->session->isUserAuthorized() == false) {
                return $response->withRedirect($redirect);
            }

            return $next($request, $response);
        };
    }

    //
    // Protection au route SQL
    // Seulement accèssible en environnement local
    //
    public static function localRestriction() 
    {
        return function($request, $response, $next) {
            if ($this->configuration->isLocal() == false) {
                return $response->withRedirect(RoutesName::HOME);
            }

            return $next($request, $response);
        };
    }
    
    //
    // Protection accès routes : Route middleware
    // Permet de s'assurer que l'utilisateur est authentifié lors de l'accès aux routes
    //
    public static function loginAccountVerify($accountType, $redirect = "") 
    {
        return function($request, $response, $next) use ($accountType, $redirect) {
        	if (empty($redirect)) {
        		$redirect = RoutesName::HOME;
        	}

            if ($this->session->isUserTypeAuthorized($accountType) == false) {
                return $response->withRedirect($redirect);
            }

            return $next($request, $response);
        };
    }
}