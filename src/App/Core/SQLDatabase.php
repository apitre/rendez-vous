<?php

namespace App\Core;

use \PDO;
use \App\Core\SQLDatabaseException;

class SQLDatabase
{
    protected $pdo = null;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function import($sql)
    {
        $queries = explode(';', $sql);

        array_pop($queries);

        foreach ($queries as $query) {
            $sqlQuery = $this->createSQLQuery();
            $ok = $this->createSQLQuery()->exec("$query;");
        }
    }

    public function getLastInsertId()
    {
        return $this->pdo->lastInsertId();
    }

    public function transaction($callable)
    {
        try {
            $this->pdo->beginTransaction();
            $callable();
            $this->pdo->commit();
        } catch (Exception $e) {
            $this->pdo->rollback();
            throw new SQLDatabaseException("Error starting new transaction: {$e->getMessage()}", 0, $e);
        }

        return true;
    }

    protected function createSQLQuery()
    {
        if (is_null($this->pdo)) {
            trigger_error("Error creating query: PDO object was not set with SQLDatabase::setPDO()", E_USER_ERROR);
        }

        return new SQLQuery($this->pdo);
    }

    public function querySimple($sql, $params = [])
    {
        $sqlQuery = $this->createSQLQuery();

        $sqlQuery->sql($sql);
        
        $sqlQuery->binds($params);

        return $sqlQuery->executeNoFetch();
    }

    public function query($sql, $params = [])
    {
        $sqlQuery = $this->createSQLQuery();

        $sqlQuery->sql($sql);

        $sqlQuery->binds($params);

        return $sqlQuery->executeFetchAll();
    }
}