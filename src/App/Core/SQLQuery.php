<?php

namespace App\Core;

use \PDO;
use \App\Core\SQLRow;
use \App\Core\SQLDatabaseException;

class SQLQuery
{
    protected $sql      = "";
    protected $params   = [];
    protected $expand   = [];
    protected $pdo      = null;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function sql($sql)
    {
        $this->sql = $sql;
    }

    public function bind($name, $value)
    {
        $this->expand[$name] = (is_array($value) ? true : false);
        $this->params[$name] = $value;        
    }

    public function binds($nameValueArray)
    {
        foreach ($nameValueArray as $name => $value) {
            $this->bind($name, $value);
        }
    }

    public function exec($sql)
    {
        $this->validateConfiguration();

        try {
            $this->pdo->exec($sql);
            return true;
        } catch (Exception $e) {
            throw new SQLDatabaseException("Error executing query: {$e->getMessage()}", 0, $e);
            return false;
        }
    }

    public function executeNoFetch()
    {
        $this->validateConfiguration();

        try {
            $statement = $this->prepare();
            $statement->execute();
            return $statement->rowCount();
        } catch (\Exception $e) {
            throw new SQLDatabaseException("Error executing query: {$e->getMessage()}", 0, $e);
        }
    }

    public function executeFetchAll()
    {
        $this->validateConfiguration();

        try {
            $statement = $this->prepare();
            $statement->execute();
            return $statement->fetchAll(PDO::FETCH_CLASS, SQLRow::class);
        } catch (\Exception $e) {
            throw new SQLDatabaseException("Error executing query: {$e->getMessage()}", 0, $e);
        }
    }

    protected function prepare()
    {
        $finalSQL    = $this->sql;
        $finalParams = [];

        // determine if parameters needs expansion or not
        foreach ($this->params as $name => $value) {
            if ($this->expand[$name]) {
                // if the parameter needs expansion:

                // determine how long the expansion will be
                $count     = count($value);
                $nameArray = [];

                // for each element in the expansion:
                for ($i=0; $i < $count; $i++) {
                    // create a new name for the element
                    // extract the value of that element
                    $singleName  = $name . '_' . $i . '_';
                    $singleValue = $value[$i];

                    // collect the new name into the name bag
                    $nameArray[] = $singleName;

                    // set the name value pair to be bound later
                    $finalParams[$singleName] = $singleValue;
                }

                // calculate the (:n_1_, :n_2_, ...) form
                $nameArrayStr = '('.implode(',', $nameArray).')';

                // replace the initial parameter name with the new set of
                // parameter names in the sql query
                $finalSQL = str_replace($name, $nameArrayStr, $finalSQL);

            } else {
                // nothing to do, just pass the parameter along
                $finalParams[$name] = $value;
            }
        }

        $statement = $this->pdo->prepare($finalSQL);

        foreach ($finalParams as $name => $value) {
            $statement->bindValue($name, $value);
        }

        return $statement;
    }

    protected function validateConfiguration()
    {
        if (is_null($this->pdo)) {
            trigger_error(
                "Error preparing statement: PDO object was not set with SQLQuery::setPDO()",
                E_USER_ERROR
            );
        }

        if ($this->pdo->getAttribute(PDO::MYSQL_ATTR_DIRECT_QUERY)) {
            trigger_error(
                "Error preparing statement: Bad PDO attribute: Emulated prepares should be disabled",
                E_USER_ERROR
            );
        }

        if ($this->pdo->getAttribute(PDO::ATTR_ERRMODE) !== PDO::ERRMODE_EXCEPTION) {
            trigger_error(
                "Error preparing statement: Bad PDO attribute: Error mode should be PDO::ERRMODE_EXCEPTION",
                E_USER_ERROR
            );
        }
    }

}
