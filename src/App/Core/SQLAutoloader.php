<?php

namespace App\Core;

use \App\Core\SQLDatabase;

class SQLAutoloader
{
    protected $db = null;
    protected $root = "";

    public function __construct(SQLDatabase $db, $root)
    {
        $this->db = $db;
        $this->root = $root;
    }

    public function executeSingle($path)
    {
        $fullpath = $this->root.$path;

        if (file_exists($fullpath)) {
            $sql = file_get_contents($fullpath);
            if (empty($sql)) {
               echo "File: $path -> <span style='color: orange;'>Empty file</span><br/>"; 
            } else {
                $this->db->import($sql);
                echo "File: $path -> <span style='color: green;'>Executed sucessfully</span><br/>";
            }
        } else {
            echo "File: $path -> <span style='color: red;'>File don't exists</span><br/>";
        }
    }

    public function dump($pathList)
    {
        $sql = "";

        foreach ($pathList as $i => $path) {
            $fullpath = $this->root.$path;
            $sql .= file_get_contents($fullpath);
        }

        echo $sql; exit;
    }

    public function executeList($pathList)
    {
        foreach ($pathList as $i => $path) {
            $this->executeSingle($path);
        }
    }
}