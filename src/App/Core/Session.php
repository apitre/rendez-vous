<?php

namespace App\Core;

use App\User\User;
use App\Core\Status;

class Session
{   
    public function __construct()
    {
        if (empty($_SESSION)) {
            session_start();
        }
    }

    public function getUser()
    {   
        $user = isset($_SESSION['user']) ? $_SESSION['user'] : null;

        if ($user instanceof User) {
            return clone $user;
        }

        return null;
    }

    public function getStatus()
    {   
        $status = isset($_SESSION['status']) ? $_SESSION['status'] : null;

        if ($status instanceof Status) {
            return clone $status;
        }

        return null;
    }

    public function setStatus(Status $status)
    {
        $_SESSION['status'] = $status;   
    }
    
    public function isUserAuthorized()
    {   
        $authorized = false;
        
        $user = $this->getUser();
        if ($user != null) {
            $authorized = ($user->accountState == User::ACCOUNT_ACTIVATED);
        }
        
        return $authorized;
    }
    
    public function isUserTypeAuthorized($accountType)
    {   
        $authorized = false;
        
        $user = $this->getUser();
        if ($user != null) {
            $authorized = ($user->accountType == $accountType) && 
                            ($user->accountState == User::ACCOUNT_ACTIVATED);
        }
        
        return $authorized;
    }
    
    public function setUser(User $user)
    {
        $_SESSION['user'] = $user;
    }

    public function deleteUser()
    {
        $_SESSION['user'] = null;
    }

    public function deleteStatus()
    {
        $_SESSION['status'] = null;
    }

}