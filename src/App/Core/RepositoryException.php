<?php

namespace App\Core;

class RepositoryException extends \Exception
{
    const EX_0      = "L'application fonctionne normalement.";
    const EX_1      = "Une erreur est survenue durant votre transaction. Veuillez reessayer un peu plus tard.";
    const EX_2      = "Utilisateur existant! Veuillez choisir un autre nom d’utilisateur.";
    const EX_3      = "Le statut de votre compte est invalide.";
    const EX_4      = "Nom d'utilisateur ou mot de passe incorrect! Réessayez de nouveau.";
    const EX_5      = "Votre compte n'est pas encore approuvé! Veuillez consulter vos emails.";
    const EX_6      = "L'url de confirmation que vous tentez d'accèder est invalide.";
    const EX_7      = "Utilisateur existant! Veuillez choisir un autre email pour votre compte.";
    const EX_8      = "Le membre est invalid ou inexistant.";
    const EX_9      = "Le service sélectionné est invalid ou inexistant.";
    const EX_10     = "La categorie ou le domaine sélectionné est invalide.";

    public function __construct($code)
    {
        parent::__construct(constant("self::EX_$code"));
    }
}
