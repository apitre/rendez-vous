<?php

namespace App\Core;

use \App\Core\MailMessage;

class MailService
{
    protected $view;
    protected $mailer;
    
    public function __construct($view, $mailer)
    {
        $this->view = $view;
        $this->mailer = $mailer;
    }
    
    public function send($template, $data, $callback)
    {
        $message = new MailMessage($this->mailer);
        
        $message->body($this->view->fetch($template, $data));
        
        call_user_func($callback, $message);
        
        $this->mailer->send();
    }
}