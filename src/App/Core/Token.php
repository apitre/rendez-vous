<?php

namespace App\Core;

class Token
{
    private static $length = 16;

    public static function salt()
    {
        return substr(uniqid(rand(), true), self::$length, self::$length);
    }

    public static function validate($salt, $password, $token)
    {
        $tpm = self::assembly($salt, $password);

        return password_verify($tpm, $token);
    }

    public static function hash($salt, $password)
    {
        $tpm = self::assembly($salt, $password);

        return password_hash($tpm, PASSWORD_DEFAULT);
    }

    public static function assembly($salt, $password)
    {
        $half = self::$length / 2;

        return substr($salt, 0, $half) . $password . substr($salt, $half);
    }

}