<?php

namespace App\Core;

use Psr\Http\Message\ServerRequestInterface;
use App\Core\SQLRow;
use App\User\User;

interface BasicFactory
{      
	public static function buildController($app, $response);

	public static function buildDtoRequest(ServerRequestInterface $row);

	public static function buildDtoDatabase(SQLRow $row);
}