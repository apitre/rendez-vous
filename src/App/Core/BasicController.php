<?php

namespace App\Core;

use App\Common\Configuration;
use App\Core\Session;
use App\Core\RepositoryException;
use App\Core\BasicRepository;
use App\View\HTMLView;
use App\User\User;
use App\User\UserFactory;
use App\Common\RoutesName;
use App\Core\Status;

//
// Classe de base pour le controleur
//
abstract class BasicController
{
    private $view = null;           // Vue
    private $repository = null;     // Dépot
    private $session = null;        // Session
    private $configuration = null;  // Configuration
    private $user = null;           // UserFromSession
    protected $data;                // Conteneur de données
    
    //
    // Constructeur
    //
    public function __construct(BasicRepository $repository, HTMLView $view, Session $session, Configuration $configuration) 
    {        
        $this->view = $view;
        $this->repository = $repository;
        $this->session = $session;
        $this->configuration = $configuration;

        $this->user = $this->session->getUser();

        $this->assignDefaultData();
    }

    //Ajout des variables disponible à tous les templates
    protected function assignDefaultData()
    {
        $this->data['ROUTE'] = RoutesName::getAll();
        $this->data['user'] = $this->loadUserData();
    }
    
    protected function getView()
    {
        return $this->view;
    }

    private function loadUserData()
    {
        if (empty($this->user)) {
            return [];
        }

        return [
            'id'        => $this->user->id,
            'username'  => $this->user->userName,
            'fullname'  => $this->user->firstName." ".$this->user->lastName,
            'email'     => $this->user->email,
            'active'    => $this->user->accountState != User::ACCOUNT_NOT_CONFIRMED,
            'phone'     => $this->user->phone,
            'type'      => $this->user->accountType == User::ACCOUNT_CONSUMER ? 'Consommateur' : 'Commerçant',
            'image'     => UserFactory::loadUserImage($this->user->id, $this->user->accountType),
            'section'   => $this->user->accountType == User::ACCOUNT_CONSUMER ? RoutesName::CONSUMER : RoutesName::MERCHANT
        ];
    }
    
    protected function getRepository()
    {
        return $this->repository;
    }
    
    protected function session()
    {
        return $this->session;
    }
    
    protected function getConfiguration()
    {
        return $this->configuration;
    }
    
    protected function getMemberFromSessionsUser()
    {
        if (empty($this->user)) {
            return null;
        }
        
        $member= null;
        
        try {
            $member = $this->getRepository()->getInfoFromId($this->user->id);
        } catch (RepositoryException $e) {
            // Rethrow exception
            throw $e;
        }
        
        return $member;
    }
    
    public function isUserOnline()
    {
        return $this->session->getUser() != null;
    }
    //
    // Changer le statut de connexion de l'utilisateur
    //
    public function setUserOnline(User $user)
    {
        $this->session->setUser($user);
    }
    
    //
    // Changer le statut de connexion de l'utilisateur
    //
    public function setUserOffline()
    {
        $this->session->deleteUser();
    }

    public function setMsgSuccess($msg)
    {
        $status = new Status("success", $msg);

        $this->session->setStatus($status);
    }

    public function setMsgError($msg)
    {
        $status = new Status("danger", $msg);

        $this->session->setStatus($status);
    }

    public function render($template, $data)
    {
        $data['status'] = $this->session->getStatus();

        $this->session->deleteStatus();

        return $this->view->render($template, $data);
    }

    //
    // Récupérer la route de l'utilisateur courant
    //
    protected function getUserRoute($accountType, $path)
    {
        $userRoutes = [
            User::ACCOUNT_NONE  => "",
            User::ACCOUNT_CONSUMER => RoutesName::CONSUMER,
            User::ACCOUNT_MERCHANT => RoutesName::MERCHANT
        ];

        $route = $userRoutes[(int)$accountType];

        return $route.$path;
    }
}