<?php

namespace App\Service;

use App\Lib\Time;
use App\Service\Service;
use App\Service\ServiceFactory;
use App\Service\ServiceFilters;
use App\Service\ServiceAppointment;

class ServiceAvailabilities
{
	private $listing = [];
	private $unAvailableDates = [];
	private $merchants = [];
	private $services = [];
	private $serviceFilters = [];

	public function setServiceFilters(ServiceFilters $serviceFilters)
	{
		$this->serviceFilters = $serviceFilters;
	}

	public function setMerchants($merchants)
	{
		$this->merchants = $merchants;
	}

	public function setServices($services)
	{
		$this->services = $services;
	}

	public function generate()
	{
		$selectedDays = $this->serviceFilters->getSelectedDays();

		foreach ($this->services as $service) {

			$timeStart = $this->getTimeStart($service);
			$timeEnd = $this->getTimeEnd($service);

			$hourStart = $this->getHourStart($service);
			$hourEnd = $this->getHourEnd($service);

			while($timeStart <= $timeEnd) {
	        	$day = strtolower(date('l', $timeStart));

	            if ($service->{$day} == 1 AND in_array($day, $selectedDays)) {
	                $this->prepareAvailabilities($service, date('Y-m-d', $timeStart), $hourStart, $hourEnd);
	            }

	            $timeStart = strtotime('+1 day', $timeStart);
	        }

		}
	}

	public function getHourStart(Service $service)
	{
		$serviceStart 	= new Time($service->hourBegin);
		$filterStart 	= new Time($this->serviceFilters->getTimeBegin());

		if ($serviceStart->seconds() > $filterStart->seconds()) {
			return $serviceStart;
		}


		return $filterStart;
	}

	public function getHourEnd(Service $service)
	{
		$serviceEnd 	= new Time($service->hourEnd);
		$filterEnd 		= new Time($this->serviceFilters->getTimeEnd());

		if ($serviceEnd->seconds() < $filterEnd->seconds()) {
			return $serviceEnd;
		}


		return $filterEnd;
	}

	public function getTimeEnd(Service $service)
	{
		$serviceEnd = strtotime($service->dateEnd);
        $filterEnd = strtotime($this->serviceFilters->getDateEnd());

        if ($serviceEnd < $filterEnd) {
        	return $serviceEnd;
        }

       	return $filterEnd;
	}

	public function getTimeStart(Service $service)
	{
		$serviceStart = strtotime($service->dateBegin);
		$filterStart = strtotime($this->serviceFilters->getDateBegin());

        if ($serviceStart > $filterStart) {
        	return $serviceStart;
        }

	    return $filterStart;
	}

	private function addUnavailableDate(ServiceAppointment $appointment)
	{
		$serviceId = $appointment->serviceId;

		if (!isset($this->unAvailableDates[$serviceId])) {
			$this->unAvailableDates[$serviceId] = [];
		}

		$this->unAvailableDates[$serviceId][] = [
			'start' => strtotime($appointment->datetimeStart),
			'end' => strtotime($appointment->datetimeEnd),
		];
	}

	public function setUnavailableDates($appointments)
	{
		foreach ($appointments as $appointment) {
			$this->addUnavailableDate($appointment);
		}
	}

	private function isAvailable($serviceId, $start, $end)
	{
		$now = date_create("now",timezone_open("America/New_York"));

		$a = strtotime($start);
		$b = strtotime($end);
		$n = strtotime(date_format($now,'Y-m-d H:i:s'));

		if ($a < $n || $b < $n) {
			return false;
		}

		if (!isset($this->unAvailableDates[$serviceId])) {
			return true;
		}

		foreach ($this->unAvailableDates[$serviceId] as $appointment) {
			
			$x = $appointment['start'];
			$y = $appointment['end'];

			if ($a >= $x && $a <= $y || $b >= $x && $b <= $y || $a <= $x && $b >= $y) {
				return false;
			}

		}

		return true;
	}

	private function prepareAvailabilities(Service $service, $strDate, $hourStart, $hourEnd)
	{
		$length 	= new Time($service->length);
		$from 		= $this->getHourStart($service);
		$to 		= $this->getHourEnd($service);
		$current 	= $this->getHourStart($service);

		while ($current->seconds() <= $to->seconds()) {
			$start = "{$strDate} {$current->toString()}";
			$current->add($length);
			$end = "{$strDate} {$current->toString()}";
			$uniqueKey = "{$start} {$service->id}";

			if ($this->isAvailable($service->id, $start, $end)) {
				$mechant = $this->merchants[$service->id];
				$this->listing[$uniqueKey] = ServiceFactory::buildDtoServiceAvailability($service, $mechant, $start, $end);
			}
		}
	}

	public function getListing()
	{
		ksort($this->listing);
		return array_slice($this->listing, 0, 100);
	}
}