<?php

namespace App\Service;


//
// Rendez-vous pour un service
//
class ServiceAppointment
{   
    // Liste des états du rendez-vous
    const STATE_NOT_CONFIRMED = 0;           
    const STATE_CONFIRMED = 1;  
    const STATE_CANCELLED = 2;
    const STATE_INPROGRESS = 3;
    const STATE_COMPLETED = 4;             
    
    public $id = null;                                          // Identifiant unique
    public $serviceId = null;                                   // Identifiant service
    public $serviceName = "";                                   // Nom du service
    public $merchantName = "";                                  // Nom du commerce
    public $consumerId = null;                                  // Information sur le consommateur
    public $comment = null;                                     // Commentaire
    public $datetimeStart = null;                               // Heure et date début rendez-vous
    public $datetimeEnd = null;                                 // Heure et date fin de rendez-vous
    public $state = ServiceAppointment::STATE_NOT_CONFIRMED;    // État du rendez-vous
}