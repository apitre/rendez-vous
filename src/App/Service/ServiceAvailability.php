<?php

namespace App\Service;

//
// Plage horaire disponible pour un service
//
class ServiceAvailability
{
    public $serviceId = 0;    
    public $serviceName = "";
    public $businessName = "";
    public $businessAddress = null;
    public $consumerId = null;
    public $dateBegin = '0000-00-00 00:00:00';
    public $dateEnd = '0000-00-00 00:00:00';
    public $color = "#cccccc";
}