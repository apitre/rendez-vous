<?php

namespace App\Service;

use Psr\Http\Message\ServerRequestInterface;
use App\Core\BasicFactory;
use App\Core\SQLRow;
use App\User\User;
use App\User\UserFactory;
use App\Merchant\MerchantRepository;
use App\Merchant\Merchant;
use App\Service\Service;
use App\Service\ServiceAppointment;
use App\Service\ServiceCategory;
use App\Service\ServiceDomain;
use App\Service\ServiceInformation;
use App\Service\ServiceRating;
use App\Service\ServiceController;
use App\Service\ServiceRepository;
use App\View\HTMLView;
use App\Common\ParamsName;
use App\Common\RoutesName;
use App\Consumer\ConsumerFactory;

class ServiceFactory implements BasicFactory 
{

    public static function buildController($app, $response) 
    {
        $view = new HTMLView($app->view, $response);
        $repository = new ServiceRepository($app->db);
        $merchantRepo = new MerchantRepository($app->db);
        $controller = new ServiceController($repository, $view, $app->session, $app->configuration);

        $controller->setMerchantRepository($merchantRepo);

        return $controller;
    }

    public static function buildDtoRequest(ServerRequestInterface $request)
    {   
        $service = new Service();

        $service->name = $request->getParam(ParamsName::SERVICE_NAME, "");
        $service->description = $request->getParam(ParamsName::SERVICE_DESCRIPTION, "");
        $service->price = $request->getParam(ParamsName::SERVICE_PRICE, "");
        $service->domainId = $request->getParam(ParamsName::SERVICE_DOMAIN, "");
        $service->categoryId = $request->getParam(ParamsName::SERVICE_CATEGORY, "");
        $service->dateBegin = $request->getParam(ParamsName::SERVICE_DATE_BEGIN, "");
        $service->dateEnd = $request->getParam(ParamsName::SERVICE_DATE_END, "");
        $service->length = $request->getParam(ParamsName::SERVICE_LENGTH, "");
        $service->hourBegin = $request->getParam(ParamsName::SERVICE_HOUR_BEGIN, "");
        $service->hourEnd = $request->getParam(ParamsName::SERVICE_HOUR_END, "");
        $service->monday = $request->getParam(ParamsName::SERVICE_MONDAY, "0");
        $service->tuesday = $request->getParam(ParamsName::SERVICE_THUESDAY, "0");
        $service->wednesday = $request->getParam(ParamsName::SERVICE_WEDNESDAY, "0");
        $service->thursday = $request->getParam(ParamsName::SERVICE_THURSDAY, "0");
        $service->friday = $request->getParam(ParamsName::SERVICE_FRIDAY, "0");
        $service->saturday = $request->getParam(ParamsName::SERVICE_SATURDAY, "0");
        $service->sunday = $request->getParam(ParamsName::SERVICE_SUNDAY, "0");
        
        return $service;
    }

    public static function buildDtoDomain(SQLRow $row) 
    {
        $domain = new ServiceDomain();
        
        $domain->id = $row->domain_id;
        $domain->name = $row->name;
        $domain->description = $row->description;
        
        return $domain;
    }

    public static function buildDtoCategory(SQLRow $row) 
    {
        $category = new ServiceCategory();
        
        $category->id = $row->category_id;
        $category->name = $row->name;
        $category->description = $row->description;
        
        return $category;
    }

    public static function buildDtoServiceRating(SQLRow $row) 
    {
        $rating = new ServiceRating();

        $rating->service = self::buildDtoDatabase($row);
        $rating->id = $row->rating_id;
        $rating->comment = $row->comment;
        $rating->score = $row->score; 
        $rating->userImage = UserFactory::loadUserImage($row->user_id, User::ACCOUNT_CONSUMER);
        
        return $rating;
    }

    public static function buildDtoDatabase(SQLRow $row) 
    {
        $service = new Service();

        $service->id = $row->service_id;
        $service->name = $row->name;
        $service->description = $row->description;
        $service->price = $row->price;
        $service->domainId = $row->domain_id;
        $service->categoryId = $row->category_id;
        $service->dateBegin = $row->date_from;
        $service->dateEnd = $row->date_to;
        $service->merchantId = $row->merchant_id;
        $service->length = $row->length;
        $service->hourBegin = $row->hour_begin;
        $service->hourEnd = $row->hour_end;
        $service->monday = $row->monday;
        $service->tuesday = $row->tuesday;
        $service->wednesday = $row->wednesday;
        $service->thursday = $row->thursday;
        $service->friday = $row->friday;
        $service->saturday = $row->saturday;
        $service->sunday = $row->sunday;
        $service->state = $row->status;
        $service->color = "#".substr(dechex(crc32($row->name)), 0, 6);
        
        return $service;
    }
    
    public static function buildDtoServiceInformation(SQLRow $row) 
    {
        $serviceInfo = new ServiceInformation();
        $serviceInfo->service = ServiceFactory::buildDtoDatabase($row);
        $serviceInfo->appointmentsCount = $row->appointments_count;

        return $serviceInfo;
    }
    
    public static function buildDtoPopularServiceInformation(SQLRow $row) 
    {
        $serviceInfo = new ServiceInformation();
        $serviceInfo->service = ServiceFactory::buildDtoDatabase($row);
        $serviceInfo->appointmentsCount = $row->appointments_count;
        $serviceInfo->userId = $row->user_id;
        $serviceInfo->merchantName = $row->merchant_name;
        $serviceInfo->merchantCity = $row->merchant_city;
        $serviceInfo->merchantImage = UserFactory::loadUserImage($row->user_id, User::ACCOUNT_MERCHANT);

        return $serviceInfo;
    }
    
    public static function buildDtoServiceAppointmentRequest(ServerRequestInterface $request) 
    {
        $appointment = new ServiceAppointment();
        $appointment->serviceId = $request->getParam(ParamsName::APPOINTMENT_SERVICE_ID, ""); 
        $appointment->datetimeStart = $request->getParam(ParamsName::APPOINTMENT_DATE_BEGIN, ""); 
        $appointment->datetimeEnd = $request->getParam(ParamsName::APPOINTMENT_DATE_END, ""); 
        $appointment->comment = $request->getParam(ParamsName::APPOINTMENT_COMMENT, "");

        return $appointment;
    }
    
    public static function buildDtoServiceAppointment(SQLRow $row) 
    {        
        $appointment = new ServiceAppointment();
        
        $appointment->id = $row->id;
        $appointment->serviceId = $row->service_id;
        $appointment->serviceName = $row->service_name;
        $appointment->merchantName = $row->merchant_name;
        $appointment->merchantImage = UserFactory::loadUserImage($row->user_id, User::ACCOUNT_MERCHANT);
        $appointment->consumerId = $row->consumer_id;
        $appointment->datetimeStart = $row->start_date;
        $appointment->datetimeEnd =  $row->end_date;
        $appointment->comment = $row->text; 
        $appointment->state = $row->status; 
        
        return $appointment;
    }

    public static function buildDtoServiceAvailability(Service $service, Merchant $merchant, $start, $end)
    {
        $availabitlity = new ServiceAvailability();

        $availabitlity->serviceId = $service->id;
        $availabitlity->serviceName = $service->name;
        $availabitlity->businessImage = UserFactory::loadUserImage($merchant->id, User::ACCOUNT_MERCHANT);
        $availabitlity->businessName = $merchant->businessName;
        $availabitlity->businessAddress = $merchant->businessAddress;
        $availabitlity->dateBegin = $start;
        $availabitlity->dateEnd = $end;
        $availabitlity->color = $service->color;

        return $availabitlity;
    }
}
