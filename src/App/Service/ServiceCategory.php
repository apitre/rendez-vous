<?php

namespace App\Service;

//
// Categorie d'un service commercant
//
class ServiceCategory
{   
    public $id = null;                          // Identifiant unique
    public $name = "";                          // Nom
    public $description = "";                   // Description détaillée 
}