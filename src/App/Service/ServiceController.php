<?php

namespace App\Service;

use App\Core\BasicController;
use App\View\HTMLView;
use App\Lib\UnitTestFactory;
use App\Common\Configuration;
use App\Core\Session;
use App\Core\RepositoryException;
use App\Service\Service;
use App\Service\ServiceFactory;
use App\Service\ServiceRepository;
use App\Common\ParamsName;
use App\Common\RoutesName;
use App\Common\TemplatesName;

//
// Controlleur pour les services
//
class ServiceController extends BasicController
{
    private $merchantRepo = null;

    public function setMerchantRepository($merchantRepo)
    {
        $this->merchantRepo = $merchantRepo;
    }

    public function loadDomains()
    {
        $domains = $this->getRepository()->getDomains();

        return $this->getView()->renderJson($domains);
    }

    public function loadCategories()
    {
        $catergories = $this->getRepository()->getCategories();

        return $this->getView()->renderJson($catergories);
    }
        
    protected function updateServiceInformation($request, $service)
    {                
        $service->name = $request->getParam(ParamsName::SERVICE_NAME, $service->name);
        $service->description = $request->getParam(ParamsName::SERVICE_DESCRIPTION, $service->description);        
        $service->price = $request->getParam(ParamsName::SERVICE_PRICE, $service->price);
        $service->categoryId = $request->getParam(ParamsName::SERVICE_CATEGORY, $service->categoryId);
        $service->dateBegin = $request->getParam(ParamsName::SERVICE_DATE_BEGIN, $service->dateBegin);
        $service->dateEnd = $request->getParam(ParamsName::SERVICE_DATE_END, $service->dateEnd);
        $service->length = $request->getParam(ParamsName::SERVICE_LENGTH, $service->length);
        $service->hourBegin = $request->getParam(ParamsName::SERVICE_HOUR_BEGIN, $service->hourBegin);
        $service->hourEnd = $request->getParam(ParamsName::SERVICE_HOUR_END, $service->hourEnd);
        $service->monday = $request->getParam(ParamsName::SERVICE_MONDAY, '0');
        $service->tuesday = $request->getParam(ParamsName::SERVICE_THUESDAY, '0');
        $service->wednesday = $request->getParam(ParamsName::SERVICE_WEDNESDAY, '0');
        $service->thursday = $request->getParam(ParamsName::SERVICE_THURSDAY, '0');
        $service->friday = $request->getParam(ParamsName::SERVICE_FRIDAY, '0');
        $service->saturday = $request->getParam(ParamsName::SERVICE_SATURDAY, '0');
        $service->sunday = $request->getParam(ParamsName::SERVICE_SUNDAY, '0');
    }
    
    //
    // Affichage de la page de creation d'un service
    //
    public function showCreationPage($request) 
    {
        $this->data['registerRoute'] = RoutesName::MERCHANT.RoutesName::SERVICE_REGISTER;
        $this->data['homeRoute'] = RoutesName::MERCHANT.RoutesName::USER_HOME;
        $this->data['modifyRoute'] = RoutesName::MERCHANT.RoutesName::USER_MODIFICATION;
            
        return $this->render(TemplatesName::CREATE_SERVICE, $this->data);
    }

    //
    // Enregistrer le nouveau service
    //
    public function registerService($request, $response) 
    {
            
        $service = ServiceFactory::buildDtoRequest($request);

        try {

            $user = $this->session()->getUser();
            $merchant = $this->merchantRepo->getInfoFromId($user->id);
            $service->merchantId = $merchant->merchantId;
            // Conserver l'information sur le nouveau service

            $this->getRepository()->add($service);

            // Rediriger de nouveau sur la page de creation
            $route = RoutesName::MERCHANT.RoutesName::USER_HOME;

            $this->setMsgSuccess('Service enregistré avec succès.');

            return $response->withRedirect("$route?service_added=1");

        } catch (RepositoryException $e) {

            // Message d'erreur provenant du repository
            $this->setMsgError($e->getMessage());

            $this->data['service'] = $service;
            $this->data['registerRoute'] = RoutesName::MERCHANT.RoutesName::SERVICE_REGISTER;
            $this->data['homeRoute'] = RoutesName::MERCHANT.RoutesName::USER_HOME;
            $this->data['modifyRoute'] = RoutesName::MERCHANT.RoutesName::USER_MODIFICATION;

            return $this->render(TemplatesName::CREATE_SERVICE, $this->data);
        }        
       
    }

    //
    // Affichage de la page de modification du service
    //
    public function showModificationPage($request) 
    {
        // Retrouver l'identifiant du service.
        $serviceId = $request->getParam(ParamsName::SERVICE_ID, "");

        try {

            // Indicateur utilisé suivant la mise a jour d'un service
            $updated = $request->getParam('updated');               
            // Retrouver l'information du service existant
            $service = $this->getRepository()->getInfoFromId($serviceId);

            // Afficher la page de modification
            $this->data['service'] = $service;
            $this->data['homeRoute'] = RoutesName::MERCHANT.RoutesName::USER_HOME;
            $this->data['modifyRoute'] = RoutesName::MERCHANT.RoutesName::USER_MODIFICATION;
            $this->data['updateServiceRoute'] = RoutesName::MERCHANT.RoutesName::SERVICE_MODIFY;
            $this->data['ratingsRoute'] = RoutesName::MERCHANT.RoutesName::SERVICE_RATINGS;
            $this->data['calendarRoute'] = RoutesName::MERCHANT.RoutesName::CALENDAR;
            $this->data['serviceUpdated'] = (int)$updated > 0;

            return $this->render(TemplatesName::MODIFY_SERVICE, $this->data);

        } catch (RepositoryException $e) {
         
            $this->data[ParamsName::SERVICE_ID] = $serviceId;
            
            $this->setMsgError($e->getMessage());

            return $this->render(TemplatesName::MODIFY_SERVICE, $this->data);
        }    
    }

    //
    // Mettre a jour du service
    //
    public function updateService($request, $response)
    {
        // Retrouver l'identifiant du service.
        $serviceId = $request->getParam(ParamsName::SERVICE_ID, "");

        try {

            // Retrouver l'information du service existant
            $service = $this->getRepository()->getInfoFromId($serviceId);                
            // Mettre a jour l'information du service
            $this->updateServiceInformation($request, $service);

            $this->getRepository()->update($service);

            $route = RoutesName::MERCHANT.RoutesName::SERVICE_MODIFICATION;

            $this->setMsgSuccess('Service modifié avec succès.');

            return $response->withRedirect("$route?id=$serviceId&updated=1");

        } catch (RepositoryException $e) {

            $this->data[ParamsName::SERVICE_ID] = $serviceId;
            
            $this->setMsgError($e->getMessage());

            return $this->render(TemplatesName::MODIFY_SERVICE, $this->data);  
        }
        
    }
    
    //
    // Supprimer un service
    //
    public function removeService($request, $response)
    {
   
        // Retrouver l'identifiant du service.
        $serviceId = $request->getParam(ParamsName::SERVICE_ID, "");

        try {

            // Retrouver l'information du service existant
            $service = $this->getRepository()->getInfoFromId($serviceId);

            if($service != null){
                // Seulement modifier l'état du service
                $service->state = Service::STATE_SUSPENDED;

                $this->getRepository()->update($service);

                $this->setMsgSuccess('Service supprimé avec succès.');

                // Rediriger sur la liste de services du commercant
                $route = RoutesName::MERCHANT.RoutesName::SERVICES;
                return $response->withRedirect($route);
            }else{
                
                $this->data[ParamsName::SERVICE_ID] = $serviceId;

                $this->setMsgError("Service inconnu");

                return $this->getView()->render(TemplatesName::MERCHANT_SERVICES, $this->data);
            }

        } catch (RepositoryException $e) {

            $this->data[ParamsName::SERVICE_ID] = $serviceId;
            
            $this->setMsgError($e->getMessage());

            return $this->render(TemplatesName::MERCHANT_SERVICES, $this->data);

        }
        
    }

    public function showCalendarPage($args)
    {     
        $serviceId = null;

        if($args != null) {
            $serviceId = $args['service_id'];
        }

        try {

            // Retrouver la liste de services
            $user = $this->session()->getUser();
            $merchant = $this->merchantRepo->getInfoFromId($user->id);
            $services = $this->getRepository()->getServices($merchant->merchantId);

            if($serviceId != null) {

                $settings = [
                    'loadRoute' => RoutesName::MERCHANT.RoutesName::CALENDAR_LOAD."/$serviceId",
                    'editRoute' => RoutesName::MERCHANT.RoutesName::CALENDAR_EDIT."/$serviceId",
                    'service_id' => $serviceId
                ];

                $this->data['settings'] = $settings;
                $this->data['calendarRoute'] = RoutesName::MERCHANT.RoutesName::CALENDAR;
                $this->data['services'] = $services;

            } else {

                $settings = [
                    'loadRoute' => RoutesName::MERCHANT.RoutesName::CALENDAR_LOAD,
                    'editRoute' => RoutesName::MERCHANT.RoutesName::CALENDAR_EDIT
                ];

                $this->data['settings'] = $settings;
                $this->data['calendarRoute'] = RoutesName::MERCHANT.RoutesName::CALENDAR;
                $this->data['services'] = $services;

            }

        } catch (RepositoryException $e) {

            $this->setMsgError($e->getMessage());

        }    
        

        return $this->render(TemplatesName::CALENDAR, $this->data);
    }

    public function loadCalendar($args, $scheduler)
    {
        $serviceId = (int)$args["service_id"];

        $sql = "SELECT 
                    *
                FROM 
                    appointment
                WHERE 
                    service_id = $serviceId";

        $scheduler->render_sql($sql, "id", "start_date,end_date,text,service_id,consumer_id,status");
    }

    public function editCalendar($args, $scheduler)
    {
        $scheduler->render_table("appointment");
    }
    
    //
    // Affichage des services du commercant
    //
    public function showServicesPage($request)
    {

        try {

            $user = $this->session()->getUser();
            $merchant = $this->merchantRepo->getInfoFromId($user->id);

            // Retrouver la liste de services
            $services = $this->getRepository()->getServices($merchant->merchantId);

            // Afficher la page de modification
            $this->data['services'] = $services;
            $this->data['homeRoute'] = RoutesName::MERCHANT.RoutesName::USER_HOME;
            $this->data['modifyRoute'] = RoutesName::MERCHANT.RoutesName::USER_MODIFICATION;
            $this->data['modifyServiceRoute'] = RoutesName::MERCHANT.RoutesName::SERVICE_MODIFICATION;
            $this->data['deleteServiceRoute'] = RoutesName::MERCHANT.RoutesName::SERVICE_DELETE;

            // Afficher la page des services
            return $this->render(TemplatesName::MERCHANT_SERVICES, $this->data);

        } catch (RepositoryException $e) {

            $this->setMsgError($e->getMessage());

            // Afficher la page des services
            return $this->render(TemplatesName::MERCHANT_SERVICES, $this->data);

        }    
        
    }  
    
    //
    // Affichage du sommaire des services pour lesquels un rendez-vous est prévu.
    //
    public function showServicesSummary($request)
    {
        try {

            // Retrouver la liste des services pour lesquels un rendez-vous est prévu dans la journée
            $user = $this->session()->getUser();  
            $merchant = $this->merchantRepo->getInfoFromId($user->id);
            $services = $this->getRepository()->getServicesSummary($merchant->merchantId);

            $this->data['services'] = $services;

            return $this->render(TemplatesName::MERCHANT_SUMMARY, $this->data);

        } catch (RepositoryException $e) {

            $this->setMsgError($e->getMessage());

            return $this->render(TemplatesName::MERCHANT_SUMMARY, $this->data);

        }    
        
    }
     
    //
    // Affichage des évaluations d'un service ou de tous les services commercant
    //
    public function showServiceRatingPage($request)
    {    
        // Retrouver l'identifiant du service (optionnel)
        $serviceId = $request->getParam(ParamsName::SERVICE_ID, "");                

        try {

            $user = $this->session()->getUser();
            if($serviceId != null) {
                // Retrouver la liste des évaluations du service commercant
                $ratings = $this->getRepository()->getServiceRatings($serviceId);
            } else {
                // Retrouver la liste des évaluations de tous les services commercant
                $merchant = $this->merchantRepo->getInfoFromId($user->id);
                $ratings = $this->getRepository()->getRatings($merchant->merchantId);
            }

            // Afficher la page
            $this->data['ratings'] = $ratings;
            $this->data['homeRoute'] = RoutesName::MERCHANT.RoutesName::USER_HOME;
            $this->data['modifyRoute'] = RoutesName::MERCHANT.RoutesName::USER_MODIFICATION;

            // Afficher la page des évaluations
            return $this->render(TemplatesName::SERVICE_RATINGS, $this->data);

        } catch (RepositoryException $e) {

            if($serviceId != null) {

                $this->data[ParamsName::SERVICE_ID] = $serviceId;

                $this->setMsgError($e->getMessage());

            } else {

                $this->setMsgError($e->getMessage());

            }

            // Afficher la page des évaluations
            return $this->render(TemplatesName::SERVICE_RATINGS, $this->data);
        }    
    }
    
    //
    // Affichage des services populaires
    //
    public function showPopularServicesPage($request)
    {    
               
        try {

            // Retrouver la liste des services populaires
            $services = $this->getRepository()->getPopularServices();

            $this->data['services'] = $services;

            return $this->render(TemplatesName::POPULAR_SERVICES, $this->data);

        } catch (RepositoryException $e) {

            $this->setMsgError($e->getMessage());

            return $this->render(TemplatesName::POPULAR_SERVICES, $this->data);
        }    
    }
}