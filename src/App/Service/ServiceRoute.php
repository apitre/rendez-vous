<?php

namespace App\Service;

use App\Service\ServiceFactory;
use App\Common\RoutesName;
use App\Core\Middleware;
use App\Merchant\Merchant;

$app->any(RoutesName::MERCHANT.RoutesName::SERVICE_CREATION, function ($request, $response, $args) {

    // Afficher la page de création d'un service
    $controller = ServiceFactory::buildController($this, $response);
    return $controller->showCreationPage($request);	
})->add(Middleware::loginAccountVerify(Merchant::ACCOUNT_MERCHANT));

$app->post(RoutesName::MERCHANT.RoutesName::SERVICE_REGISTER, function ($request, $response, $args) {
    
    // Enregistrer le service commercant
    $controller = ServiceFactory::buildController($this, $response);
    return $controller->registerService($request, $response);
})->add(Middleware::loginAccountVerify(Merchant::ACCOUNT_MERCHANT));

$app->any(RoutesName::MERCHANT.RoutesName::SERVICE_MODIFICATION, function ($request, $response, $args) {

    // Afficher la page de modification d'un service
    $controller = ServiceFactory::buildController($this, $response);
    return $controller->showModificationPage($request);	
})->add(Middleware::loginAccountVerify(Merchant::ACCOUNT_MERCHANT));

$app->post(RoutesName::MERCHANT.RoutesName::SERVICE_MODIFY, function ($request, $response, $args) {
    
    // Mettre a jour le service commercant
    $controller = ServiceFactory::buildController($this, $response);
    return $controller->updateService($request, $response);
})->add(Middleware::loginAccountVerify(Merchant::ACCOUNT_MERCHANT));

$app->any(RoutesName::MERCHANT.RoutesName::SERVICE_DELETE, function ($request, $response, $args) {
    
    // Supprimer le service commercant
    $controller = ServiceFactory::buildController($this, $response);
    return $controller->removeService($request, $response);
})->add(Middleware::loginAccountVerify(Merchant::ACCOUNT_MERCHANT));

$app->any(RoutesName::MERCHANT.RoutesName::SERVICES, function ($request, $response, $args) {

    // Afficher la page des services du commercant
    $controller = ServiceFactory::buildController($this, $response);
    return $controller->showServicesPage($request);	
})->add(Middleware::loginAccountVerify(Merchant::ACCOUNT_MERCHANT));

$app->any(RoutesName::MERCHANT.RoutesName::SERVICE_DOMAINS, function ($request, $response, $args) {

    $controller = ServiceFactory::buildController($this, $response);
    return $controller->loadDomains(); 
});

$app->any(RoutesName::MERCHANT.RoutesName::SERVICE_CATEGORIES, function ($request, $response, $args) {

    $controller = ServiceFactory::buildController($this, $response);
    return $controller->loadCategories(); 
});

$app->any(RoutesName::MERCHANT.RoutesName::SERVICE_APPOINTMENTS_SUMMARY, function ($request, $response, $args) {

    $controller = ServiceFactory::buildController($this, $response);
    return $controller->showServicesSummary($request); 
})->add(Middleware::loginAccountVerify(Merchant::ACCOUNT_MERCHANT));

$app->any(RoutesName::MERCHANT.RoutesName::CALENDAR, function ($request, $response, $args) {

    // Afficher le calendrier du commercant
    $controller = ServiceFactory::buildController($this, $response);
    return $controller->showCalendarPage($args);	
})->add(Middleware::loginAccountVerify(Merchant::ACCOUNT_MERCHANT));

$app->any(RoutesName::MERCHANT.RoutesName::CALENDAR.'/{service_id}', function ($request, $response, $args) {

    $controller = ServiceFactory::buildController($this, $response);
    return $controller->showCalendarPage($args);
})->add(Middleware::loginAccountVerify(Merchant::ACCOUNT_MERCHANT));

$app->any(RoutesName::MERCHANT.RoutesName::CALENDAR_LOAD.'/{service_id}', function ($request, $response, $args) {

    $controller = ServiceFactory::buildController($this, $response);
    $controller->loadCalendar($args, $this->calendar);
    exit;
})->add(Middleware::loginAccountVerify(Merchant::ACCOUNT_MERCHANT));

$app->any(RoutesName::MERCHANT.RoutesName::CALENDAR_EDIT.'/{service_id}', function ($request, $response, $args) {
    
    $controller = ServiceFactory::buildController($this, $response);
    $controller->editCalendar($args, $this->calendar);
    exit;
})->add(Middleware::loginAccountVerify(Merchant::ACCOUNT_MERCHANT));

$app->any(RoutesName::MERCHANT.RoutesName::SERVICE_RATINGS, function ($request, $response, $args) {

    // Afficher la page des évaluations d'un service ou de tous les services du commercant
    $controller = ServiceFactory::buildController($this, $response);
    return $controller->showServiceRatingPage($request);	
})->add(Middleware::loginAccountVerify(Merchant::ACCOUNT_MERCHANT));


$app->any(RoutesName::POPULAR_SERVICES, function ($request, $response, $args) {

    // Afficher la page des services les plus populaires
    $controller = ServiceFactory::buildController($this, $response);
    return $controller->showPopularServicesPage($request);	
});


