<?php

namespace App\Service;


//
// Évaluation pour un service
//
class ServiceRating
{   
    const UNKNOWN = 0;  
    const STAR_1 = 1;           
    const STAR_2 = 2;  
    const STAR_3 = 3;
    const STAR_4 = 4;
    const STAR_5 = 5;             
    
    public $id = null;                                              // Identifiant unique de l'évaluation
    public $service = null;                                         // Information service
    public $comment = "";                                           // Commentaire du consommateur
    public $score = ServiceRating::UNKNOWN;                         // Niveau d'appréciation du service
    public $userImage;
}