<?php

namespace App\Service;

use App\Core\BasicRepository;
use App\Core\SQLDatabase;
use App\Service\Service;
use App\Service\ServiceFactory;
use App\Service\ServiceFilters;
use App\Service\ServiceAppointment;
use App\Service\ServiceAppointmentStatistics;
use App\Service\ServiceRating;
use App\Service\ServiceInformation;
use App\Core\RepositoryException;

//
// Dépot de données du service
//
class ServiceRepository extends BasicRepository
{
    const PASSED_APPOINTMENTS = 0;
    const COMING_APPOINTMENTS = 1;
    const ALL_APPOINTMENTS = 2;
    
    public function add($service)
    {
        $this->db->transaction(function() use ($service){

            if (!$this->isValidMerchant($service->merchantId)) {
                throw new RepositoryException(8);
            }

            if (!$this->isValidCategory($service->categoryId, $service->domainId)) {
                throw new RepositoryException(10);
            }

            $this->insertService($service);

        });
    }

    private function isValidCategory($categoryId, $domainId)
    {
        $sql = "SELECT
                    count(*) AS total
                FROM
                    service_category
                INNER JOIN
                    service_domain
                    ON service_category.domain_id = service_domain.domain_id 
                WHERE 
                    service_category.category_id = :category_id AND 
                    service_domain.domain_id = :domain_id";

        $rows = $this->db->query($sql, [
            'category_id' => $categoryId,
            'domain_id' => $domainId
        ]);

        return $rows[0]->total > 0;   
    }

    private function isValidMerchant($merchantId)
    {
        $sql = "SELECT
                    count(*) AS total
                FROM
                    merchant 
                WHERE 
                    merchant_id = :merchant_id";

        $rows = $this->db->query($sql, [
            'merchant_id' => $merchantId
        ]);

        return $rows[0]->total > 0;
    }

    private function insertService($service)
    {
        $sql = "INSERT INTO 
                    service 
                VALUES (
                    :service_id,
                    :category_id,
                    :merchant_id,
                    :name,
                    :description,
                    :price,
                    :date_from,
                    :date_to,
                    :length,
                    :hour_begin,
                    :hour_end,
                    :monday,
                    :tuesday,
                    :wednesday,
                    :thursday,
                    :friday,
                    :saturday,
                    :sunday,
                    :status
                )";

        $rowCount = $this->db->querySimple($sql, [
            ":service_id"   => $service->id,
            ":category_id"  => $service->categoryId,
            ":merchant_id"  => $service->merchantId,
            ":name"         => $service->name,
            ":description"  => $service->description,
            ":price"        => $service->price,
            ":date_from"    => $service->dateBegin,
            ":date_to"      => $service->dateEnd,
            ":length"       => $service->length,
            ":hour_begin"   => $service->hourBegin,
            ":hour_end"     => $service->hourEnd,
            ":monday"       => $service->monday,
            ":tuesday"     => $service->tuesday,
            ":wednesday"    => $service->wednesday,
            ":thursday"     => $service->thursday,
            ":friday"       => $service->friday,
            ":saturday"     => $service->saturday,
            ":sunday"       => $service->sunday,
            ":status"       => $service->state
        ]);

        $service->id = $this->db->getLastInsertId();

        if ($service->id == null) {
            throw new RepositoryException(1);
        }
    }
    
    public function update($service)
    {
        $sql = "UPDATE
                    service
                SET
                    category_id = :category_id,
                    name = :name,
                    description = :description,
                    price = :price,
                    date_from = :date_from,
                    date_to = :date_to,
                    length = :length,
                    hour_begin = :hour_begin,
                    hour_end = :hour_end,
                    monday = :monday,
                    tuesday = :tuesday,
                    wednesday = :wednesday,
                    thursday = :thursday,
                    friday = :friday,
                    saturday = :saturday,
                    sunday = :sunday,
                    status = :status
                WHERE
                    service_id = :service_id";

        $rowCount = $this->db->querySimple($sql, [
            ":service_id"   => $service->id,
            ":category_id"  => $service->categoryId,
            ":name"         => $service->name,
            ":description"  => $service->description,
            ":price"        => $service->price,
            ":date_from"    => $service->dateBegin,
            ":date_to"      => $service->dateEnd,
            ":length"       => $service->length,
            ":hour_begin"   => $service->hourBegin,
            ":hour_end"     => $service->hourEnd,
            ":monday"       => $service->monday,
            ":tuesday"      => $service->tuesday,
            ":wednesday"    => $service->wednesday,
            ":thursday"     => $service->thursday,
            ":friday"       => $service->friday,
            ":saturday"     => $service->saturday,
            ":sunday"       => $service->sunday,
            ":status"       => $service->state
        ]);
        
        return $rowCount > 0;
    }

    public function getDomains() 
    {
        $sql = "SELECT 
                    *
                FROM 
                    service_domain";

        $rows = $this->db->query($sql, []);

        $domains = [];

        foreach ($rows as $row) {
            $domains[$row->domain_id] = ServiceFactory::buildDtoDomain($row);
        }

        return $domains;  
    }
    
    public function getCategories() 
    {
        $sql = "SELECT 
                    *
                FROM 
                    service_category";

        $rows = $this->db->query($sql, []);

        $categories = [];

        foreach ($rows as $row) {
            $categories[$row->domain_id][$row->category_id] = ServiceFactory::buildDtoCategory($row);
        }

        return $categories;  
    }

    public function getInfoFromIds($service_ids) 
    {
        if (empty($service_ids)) {
            return [];
        }

        $sql = "SELECT
                    service.*,
                    domain_id
                FROM
                    service
                INNER JOIN
                    service_category
                    ON service_category.category_id = service.category_id
                WHERE
                    service_id IN :service_ids";

        $rows = $this->db->query($sql, [
            ':service_ids' => $service_ids
        ]);

        $services = [];

        foreach ($rows as $row) {
            $services[] = ServiceFactory::buildDtoDatabase($row);
        }

        return $services;
    }
    
    public function getInfoFromId($service_id) 
    {
        $sql = "SELECT
                    service.*,
                    domain_id
                FROM
                    service
                INNER JOIN
                    service_category
                    ON service_category.category_id = service.category_id
                WHERE
                    service_id = :service_id";

        $row = $this->db->query($sql, [
            ':service_id' => $service_id
        ]);

        if (empty($row)) {
            return null;
        }

        return ServiceFactory::buildDtoDatabase($row[0]);
    }
    
    public function getServices($merchantId) 
    {
        $sql = "SELECT
                    service.*,
                    domain_id
                FROM
                    service
                INNER JOIN
                    service_category
                    ON service_category.category_id = service.category_id
                WHERE 
                    merchant_id = :merchant_id 
                    AND service.status = 1";

        $rows = $this->db->query($sql, [
            ':merchant_id' => $merchantId
        ]);

        $services = [];

        foreach ($rows as $row) {
            $services[] = ServiceFactory::buildDtoDatabase($row);
        }

        return $services;
    }
             
    public function getServicesSummary($merchantId) 
    {
        $sql = "SELECT
                    service.*,
                    domain_id,
                    merchant.name AS merchant_name,
                    address.city AS merchant_city,
                    COUNT(appointment.service_id) AS appointments_count
                FROM
                    service
                INNER JOIN
                    service_category
                    ON service_category.category_id = service.category_id
                INNER JOIN
                    merchant
                    ON merchant.merchant_id = service.merchant_id
                INNER JOIN
                    address
                    ON address.address_id = merchant.address_id
                INNER JOIN
                    appointment
                    ON appointment.service_id = service.service_id
                WHERE 
                    DAYOFYEAR(start_date) = DAYOFYEAR(CURRENT_TIMESTAMP) 
                    AND service.merchant_id = :merchant_id
                    AND service.status = 1
                GROUP BY 
                    service.service_id";

        $rows = $this->db->query($sql, [
            ':merchant_id' => $merchantId
        ]);

        $serviceInformation = [];
        
        foreach ($rows as $row) {
            $serviceInformation[] = ServiceFactory::buildDtoServiceInformation($row);
        }

        return $serviceInformation;
    }
       
    public function getServiceAppointmentFromId($appointmentId) {

        $sql = "SELECT
                    appointment.*,
                    service.service_id AS service_name,
                    service.name AS service_name,
                    merchant.name AS merchant_name
                FROM
                    appointment
                INNER JOIN
                    service
                    ON appointment.service_id = service.service_id
                INNER JOIN
                    merchant
                    ON merchant.merchant_id = service.merchant_id
                WHERE appointment.id = :appointment_id";

        $row = $this->db->query($sql, [ 'appointment_id' => $appointmentId ]);
        
       if (empty($row)) {
            return null;
        }

        return ServiceFactory::buildDtoServiceAppointment($row[0]);
    }
    
    public function getAppointments($consumerId, $whichAppointments) {
        
        $dateFilter = null;
        $ordering = null;

        if ($whichAppointments == ServiceRepository::PASSED_APPOINTMENTS) {
            $dateFilter = "appointment.end_date < CURRENT_TIMESTAMP";
            $ordering = "appointment.start_date DESC";
        } else if ($whichAppointments == ServiceRepository::COMING_APPOINTMENTS) {
            $dateFilter = "appointment.end_date >= CURRENT_TIMESTAMP";
            $ordering = "appointment.start_date ASC";
        }
        
        $sql = "SELECT
                    appointment.*,
                    service.service_id AS service_name,
                    service.name AS service_name,
                    merchant.name AS merchant_name,
                    user.user_id
                FROM
                    appointment
                INNER JOIN
                    service
                    ON appointment.service_id = service.service_id
                INNER JOIN
                    merchant
                    ON merchant.merchant_id = service.merchant_id
                INNER JOIN
                    user
                    ON user.user_id = merchant.user_id
                WHERE 
                    ".$dateFilter." AND appointment.consumer_id = :consumer_id
                ORDER BY 
                    ".$ordering;

        $rows = $this->db->query($sql, [ ':consumer_id' => $consumerId ]);
        
        $appointments = [];
        
        foreach ($rows as $row) {
            $appointments[] = ServiceFactory::buildDtoServiceAppointment($row);
        }
        
        return $appointments;
    }

     public function getAppointmentStatistics($consumerId){
         
        $stats = new ServiceAppointmentStatistics();
         
        $sql = "SELECT COUNT(*) AS appointmentsCount
                FROM
                    appointment
                WHERE 
                    appointment.end_date < CURRENT_TIMESTAMP AND appointment.consumer_id = :consumer_id";

        $row = $this->db->query($sql, [ ':consumer_id' => $consumerId ]);
        
        if (!empty($row)) {
            $stats->passedAppointmentsCount = $row[0]->appointmentsCount;
        }
        
        $sql = "SELECT COUNT(*) AS appointmentsCount
                FROM
                    appointment
                WHERE 
                    appointment.status != '2' AND appointment.start_date >= CURRENT_TIMESTAMP AND appointment.consumer_id = :consumer_id";

        $row = $this->db->query($sql, [ ':consumer_id' => $consumerId ]);
        
        if (!empty($row)) {
            $stats->comingAppointmentsCount = $row[0]->appointmentsCount;
        }
        
        $sql = "SELECT COUNT(*) AS appointmentsCount
                FROM
                    appointment
                WHERE 
                    appointment.status = '2' AND appointment.consumer_id = :consumer_id";

        $row = $this->db->query($sql, [ ':consumer_id' => $consumerId ]);
        
        if (!empty($row)) {
            $stats->cancelledAppointmentsCount = $row[0]->appointmentsCount;
        }
        
        $sql = "SELECT COUNT(*) AS appointmentsCount
                FROM
                    appointment
                WHERE 
                    appointment.consumer_id = :consumer_id";

        $row = $this->db->query($sql, [ ':consumer_id' => $consumerId ]);
        
        if (!empty($row)) {
            $stats->appointmentsCount = $row[0]->appointmentsCount;
        }
        
        $sql = "SELECT COUNT(*) AS ratingCount
                FROM
                    rating
                WHERE 
                    rating.consumer_id = :consumer_id";

        $row = $this->db->query($sql, [ ':consumer_id' => $consumerId ]);
        
        if (!empty($row)) {
            $stats->ratingCount = $row[0]->ratingCount;
        }
   
        $sql = "SELECT
                    TIMESTAMPDIFF(DAY, CURRENT_TIMESTAMP, appointment.start_date) AS diffDays,
                    TIMESTAMPDIFF(HOUR, CURRENT_TIMESTAMP, appointment.start_date) AS diffHours,
                    TIMESTAMPDIFF(MINUTE, CURRENT_TIMESTAMP, appointment.start_date) AS diffMinutes
                FROM
                    appointment
                INNER JOIN
                    service
                    ON appointment.service_id = service.service_id
                WHERE 
                    appointment.start_date > CURRENT_TIMESTAMP AND appointment.consumer_id = :consumer_id
                ORDER BY appointment.start_date ASC 
                LIMIT 1";

        $row = $this->db->query($sql, [ ':consumer_id' => $consumerId ]);
        
         if (!empty($row)) {
             
            if($row[0]->diffDays > 1){
                $stats->timeBeforeNextAppointment = $row[0]->diffDays." jours";
            }else if($row[0]->diffHours > 1){
                $stats->timeBeforeNextAppointment = $row[0]->diffHours." heures";
            }else {
                $stats->timeBeforeNextAppointment = $row[0]->diffMinutes." minutes";
            }
        }
        
        return $stats; 
     }
    
    public function getServiceAppointments($serviceIds, ServiceFilters $filters) 
    {
        if (!$filters->hasValidDates()) {
            return [];
        }

        $sql = "SELECT
                    appointment.*,
                    service.service_id AS service_name,
                    service.name AS service_name,
                    merchant.name AS merchant_name,
                    user.user_id
                FROM
                    appointment
                INNER JOIN
                    service
                    ON appointment.service_id = service.service_id
                INNER JOIN
                    merchant
                    ON merchant.merchant_id = service.merchant_id
                INNER JOIN
                    user
                    ON user.user_id = merchant.user_id
                WHERE 
                    appointment.start_date >= :start_date AND
                    appointment.end_date < ADDDATE(:end_date, INTERVAL 1 DAY) AND
                    appointment.service_id IN :service_ids
                ORDER BY 
                    appointment.start_date DESC";

        $rows = $this->db->query($sql, [ 
            ':start_date' => $filters->getDateBegin(),
            ':end_date' => $filters->getDateEnd(),
            ':service_ids' => $serviceIds     
        ]);
        
        $appointments = [];
        
        foreach ($rows as $row) {
            $appointments[] = ServiceFactory::buildDtoServiceAppointment($row);
        }
        
        return $appointments;
    }
            
    private function isValidService($serviceId)
    {
        $sql = "SELECT
                    count(*) AS total
                FROM
                    service 
                WHERE 
                    service_id = :service_id";

        $rows = $this->db->query($sql, [ 'service_id' => $serviceId ]);

        return $rows[0]->total > 0;
    }
    
    public function addServiceAppointment($appointment)
    {
        if (!$this->isValidService($appointment->serviceId)) {
            throw new RepositoryException(9);
        }

        $sql = "INSERT INTO 
                    appointment 
                VALUES (
                    :id,
                    :start_date,
                    :end_date,
                    :text,
                    :service_id,
                    :consumer_id,
                    :status
                )";

        $rowCount = $this->db->querySimple($sql, [
            ":id"           => $appointment->id,
            ":start_date"   => $appointment->datetimeStart,
            ":end_date"     => $appointment->datetimeEnd,
            ":text"         => $appointment->comment,
            ":service_id"   => $appointment->serviceId,
            ":consumer_id"  => $appointment->consumerId,
            ":status"       => $appointment->state
        ]);

        $appointment->id = $this->db->getLastInsertId();

        if ($appointment->id == null) {
            throw new RepositoryException(1);
        }
    }
    
    public function updateServiceAppointment($appointment) {
        
         $sql = "UPDATE (start_date, end_date, text, status)
                    appointment
                SET
                    start_date = :start_date,
                    end_date = :end_date,
                    text = :text,
                    status = :status
                WHERE
                    id = :appointment_id";

        $rowCount = $this->db->querySimple($sql, [
            ":start_date"       => $appointment->datetimeStart,
            ":end_date"         => $appointment->datetimeEnd,
            ":text"             => $appointment->comment,
            ":status"           => $appointment->state
        ]);
        
        return $rowCount > 0;
    }
         
    public function getRatings($merchantId) 
    {
         $sql = "SELECT
                    rating.*,
                    service.*,
                    user.user_id AS user_id,
                    service_category.domain_id
                FROM
                    rating
                INNER JOIN
                    consumer
                    ON rating.consumer_id = consumer.consumer_id
                INNER JOIN
                    user
                    ON user.user_id = consumer.user_id
                INNER JOIN
                    service
                    ON service.service_id = rating.service_id
                INNER JOIN
                    service_category
                    ON service_category.category_id = service.category_id
                WHERE service.merchant_id = :merchant_id
                ORDER BY 
                    rating.rating_id DESC
                LIMIT 50";

        $rows = $this->db->query($sql, [ 'merchant_id' => $merchantId ]);
        
        $ratings = [];
        
        foreach ($rows as $row) {
            $ratings[] = ServiceFactory::buildDtoServiceRating($row);
        }
        
        return $ratings;
    }

    public function getServicesIdsFromFilters(ServiceFilters $filters)
    {
        $filters->generate();

        $where = $filters->getWhere();

        $sql = "SELECT
                    service_id
                FROM
                    service
                WHERE
                    $where AND service.status = 1";

        $rows = $this->db->query($sql, $filters->getBinds());
               
        
        $ids = [];
        
        foreach ($rows as $row) {
            $ids[] = $row->service_id;
        }

        return $ids;
    }
    
    public function getServiceRatings($serviceId) 
    {        
         $sql = "SELECT
                    rating.*,
                    service.*,
                    user.user_id AS user_id,
                    service_category.domain_id
                 FROM
                    rating
                INNER JOIN
                    consumer
                    ON rating.consumer_id = consumer.consumer_id
                INNER JOIN
                    user
                    ON user.user_id = consumer.user_id
                INNER JOIN
                    service
                    ON service.service_id = rating.service_id
                INNER JOIN
                    service_category
                    ON service_category.category_id = service.category_id
                WHERE rating.service_id = :service_id
                ORDER BY 
                    rating.rating_id DESC
                LIMIT 50";

        $rows = $this->db->query($sql, [ 'service_id' => $serviceId ]);
        
        $ratings = [];
        
        foreach ($rows as $row) {
            $ratings[] = ServiceFactory::buildDtoServiceRating($row);
        }
        
        return $ratings;
    }
    
    public function getPopularServices(){
         $sql = "SELECT
                    service.*,
                    domain_id,
                    merchant.user_id AS user_id,
                    merchant.name AS merchant_name,
                    address.city AS merchant_city,
                    COUNT(appointment.service_id) AS appointments_count
                FROM
                    service
                INNER JOIN
                    service_category
                    ON service_category.category_id = service.category_id
                INNER JOIN
                    merchant
                    ON merchant.merchant_id = service.merchant_id
                INNER JOIN
                    address
                    ON address.address_id = merchant.address_id
                INNER JOIN
                    appointment
                    ON appointment.service_id = service.service_id
                WHERE service.status = 1
                GROUP BY 
                    service.service_id
                ORDER BY
                    appointments_count DESC 
                LIMIT 10";

        $rows = $this->db->query($sql, []);
        
        foreach ($rows as $row) {
            $serviceInformation[] = ServiceFactory::buildDtoPopularServiceInformation($row);
        }

        return $serviceInformation;
    }
}