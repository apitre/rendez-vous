<?php

namespace App\Service;

//
// Domaine du service commercant
//
class ServiceDomain
{   
    public $id = null;                          // Identifiant unique
    public $name = "";                          // Nom
    public $description = "";                   // Description détaillée 
}