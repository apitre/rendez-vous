<?php

namespace App\Service;

use App\Service\Service;

//
// Information détaillée d'un service
//
class ServiceInformation
{           
    public $service = null;             // Information de base
    public $appointmentsCount = null;   // Nombre de rendez-vous
    public $merchantName = "";   
    public $merchantCity = "";   
    public $merchantImage = ""; 
}