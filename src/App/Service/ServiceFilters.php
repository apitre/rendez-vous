<?php

namespace App\Service;

use Psr\Http\Message\ServerRequestInterface;
use App\Common\ParamsName;

class ServiceFilters
{
    private $request = null;
    private $where = [];
    private $binds = [];

    public function setRequest(ServerRequestInterface $request)
    {
        $this->request = $request;
    }

    public function generate() 
    {
        $this->prepareHours();
        $this->prepareDates();
        $this->prepareDays();
        $this->preparePrice();
        $this->prepareCategory();
    }

    public function getWhere()
    {
        return implode(" AND ", $this->where);
    }

    public function getBinds()
    {
        return $this->binds;
    }

    private function prepareCategory()
    {
        $categoryId = (float)$this->request->getParam(ParamsName::SERVICE_CATEGORY, '0');

        if ((int)$categoryId > 0) {
            $this->where[] = "category_id = :category_id";
            $this->binds[':category_id'] = $categoryId;
        }
    }

    private function preparePrice()
    {
        $price = $this->request->getParam(ParamsName::SERVICE_PRICE, '0.00');

        if ($this->isValidPrice($price)) {
            $this->where[] = "price <= :price";
            $this->binds[':price'] = (double)$price;
        }
    }

    public function getSelectedDays()
    {
        $days = [
            ParamsName::SERVICE_MONDAY => $this->request->getParam(ParamsName::SERVICE_MONDAY, '0'), 
            ParamsName::SERVICE_THUESDAY => $this->request->getParam(ParamsName::SERVICE_THUESDAY, '0'), 
            ParamsName::SERVICE_WEDNESDAY => $this->request->getParam(ParamsName::SERVICE_WEDNESDAY, '0'), 
            ParamsName::SERVICE_THURSDAY => $this->request->getParam(ParamsName::SERVICE_THURSDAY, '0'), 
            ParamsName::SERVICE_FRIDAY => $this->request->getParam(ParamsName::SERVICE_FRIDAY, '0'), 
            ParamsName::SERVICE_SATURDAY => $this->request->getParam(ParamsName::SERVICE_SATURDAY, '0'), 
            ParamsName::SERVICE_SUNDAY => $this->request->getParam(ParamsName::SERVICE_SUNDAY, '0')
        ];

        foreach ($days as $day => $active) {
            if ((int)$active != 1) {
                unset($days[$day]);
            }
        }

        return array_keys($days);
    }

    private function prepareDays()
    {
        $selectedDays = $this->getSelectedDays();

        $or = "";

        foreach ($selectedDays as $day) {
            $this->binds[$day] = 1;
            $or .= " $day = :$day OR";
        }

        $this->where[] = "($or 1=0)";
    }   

    private function prepareHours()
    {
        if ($this->hasValidTimes()) {

            $begin = $this->getTimeBegin();
            $end = $this->getTimeEnd();

            $this->where[] = "((hour_begin BETWEEN :hour_begin1 AND :hour_end1)
                                OR (hour_end BETWEEN :hour_begin2 AND :hour_end2)
                                OR (hour_begin <= :hour_begin3 AND hour_end >= :hour_end3))";

            $this->binds[':hour_begin1'] = $begin;
            $this->binds[':hour_begin2'] = $begin;
            $this->binds[':hour_begin3'] = $begin;
            $this->binds[':hour_end1'] = $end;
            $this->binds[':hour_end2'] = $end;
            $this->binds[':hour_end3'] = $end;
        }
    }

    private function prepareDates()
    {
        if ($this->hasValidDates()) {

            $begin = $this->getDateBegin();
            $end = $this->getDateEnd();

            $this->where[] = "((date_from BETWEEN :date_begin1 AND :date_end1) 
                                OR (date_to BETWEEN :date_begin2 AND :date_end2)
                                OR (date_from <= :date_begin3 AND date_to >= :date_end3))";

            $this->binds[':date_begin1'] = $begin;
            $this->binds[':date_begin2'] = $begin;
            $this->binds[':date_begin3'] = $begin;
            $this->binds[':date_end1'] = $end;
            $this->binds[':date_end2'] = $end;
            $this->binds[':date_end3'] = $end;

        }
    }

    public function getTimeBegin()
    {
        return $this->request->getParam(ParamsName::SERVICE_HOUR_BEGIN, "");
    }

    public function getTimeEnd()
    {   
        return $this->request->getParam(ParamsName::SERVICE_HOUR_END, "");
    }

    public function hasValidTimes()
    {
        $begin = $this->getTimeBegin();
        $end = $this->getTimeEnd();

        return $this->isValidTime($begin) && $this->isValidTime($end);
    }

    public function getDateBegin()
    {
        return $this->request->getParam(ParamsName::SERVICE_DATE_BEGIN, "");
    }

    public function getDateEnd()
    {
        return $this->request->getParam(ParamsName::SERVICE_DATE_END, "");
    }

    public function hasValidDates()
    {
        $begin = $this->getDateBegin();
        $end = $this->getDateEnd();

        return $this->isValidDate($begin) && $this->isValidDate($end);
    }

    private function isValidPrice($price)
    {
        return is_numeric($price);
    }

    private function isValidTime($time)
    {
        return strtotime($time) != false && $time != "00:00:00";
    }

    private function isValidDate($date)
    {
        return preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date);
    }

}
