<?php

namespace App\Service;

//
// Statitiques des rendez-vous d'un membre
//
class ServiceAppointmentStatistics
{       
    public $passedAppointmentsCount = 0;                   
    public $comingAppointmentsCount = 0;    
    public $cancelledAppointmentsCount = 0;
    public $appointmentsCount = 0;
    public $ratingCount = 0;
    public $timeBeforeNextAppointment = 0;
}