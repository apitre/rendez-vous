<?php

namespace App\Service;

//
// Information sur un service commercant
//
class Service
{       
    // Liste des états du service
    const STATE_ACTIVE = 1;           
    const STATE_SUSPENDED = 2;  
    
    public $id = null;                  // Identifiant unique
    public $name = "";                  // Nom
    public $description = "";           // Description détaillée 
    public $price = 0.00;               // Prix pour ce service    
    public $domainId = null;            // Domain du service 
    public $categoryId = null;          // Catégorie du service 
    public $dateBegin = null;           // Date de début du service
    public $dateEnd = null;                 // Date de fin du service
    public $merchantId = 0;                 // Date de fin du service
    public $length = "00:00:00";
    public $hourBegin = "00:00:00";
    public $hourEnd = "00:00:00";
    public $monday = 0;
    public $tuesday = 0;
    public $wednesday = 0;
    public $thursday = 0;
    public $friday = 0;
    public $saturday = 0;
    public $sunday = 0;
    public $state = self::STATE_ACTIVE;
    public $color = "#cccccc";
}