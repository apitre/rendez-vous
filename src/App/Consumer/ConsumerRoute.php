<?php

namespace App\Consumer;

use \App\Consumer\ConsumerFactory;
use App\Core\Middleware;
use App\Common\RoutesName;


$app->get(RoutesName::CONSUMER.RoutesName::USER_REGISTRATION, function ($request, $response, $args) {
    
    // Afficher la page d'enregistrement d'un consommateur
    $controller = ConsumerFactory::buildController($this, $response);
    return $controller->showRegistrationPage($request);
});

$app->get(RoutesName::CONSUMER.RoutesName::USER_HOME, function ($request, $response, $args) {
    
    // Afficher la page d'accueil du consommateur
    $controller = ConsumerFactory::buildController($this, $response);
    return $controller->showHomePage($request, $response);
})->add(Middleware::loginAccountVerify(Consumer::ACCOUNT_CONSUMER));

$app->post(RoutesName::CONSUMER.RoutesName::USER_REGISTER, function ($request, $response, $args) {

    // Enregistrer le consommateur sur le site
    $controller = ConsumerFactory::buildController($this, $response);
    return $controller->register($request, $response);	
});

$app->get(RoutesName::CONSUMER.RoutesName::USER_CONFIRMATION, function ($request, $response, $args) {

    // Afficher la confirmation de l'enregistrement du consommateur
    $controller = ConsumerFactory::buildController($this, $response);
    return $controller->showRegistrationConfirmation($request, $response);  
});

$app->get(RoutesName::CONSUMER.RoutesName::USER_MODIFICATION, function ($request, $response) {
    
    // Afficher la page de modification des paramètres d'un consommateur
    $controller = ConsumerFactory::buildController($this, $response);
    return $controller->showModificationPage($request, $response);
})->add(Middleware::loginAccountVerify(Consumer::ACCOUNT_CONSUMER));

$app->post(RoutesName::CONSUMER.RoutesName::USER_MODIFY, function ($request, $response, $args) {
    
    // Mise a jour des paramètres d'un consommateur
    $controller = ConsumerFactory::buildController($this, $response);
    return $controller->updateParameters($request, $response);
})->add(Middleware::loginAccountVerify(Consumer::ACCOUNT_CONSUMER));

$app->any(RoutesName::CONSUMER.RoutesName::SERVICE_SEARCH, function ($request, $response, $args) {

    // Afficher la page de recherche d'un service pour le consommateur
    $controller = ConsumerFactory::buildController($this, $response);
    return $controller->showServiceSearchPage($request);    
})->add(Middleware::loginAccountVerify(Consumer::ACCOUNT_CONSUMER));

$app->any(RoutesName::CONSUMER.RoutesName::SERVICE_VIEW, function ($request, $response, $args) {

    // Afficher la page de consultation d'un service pour le consommateur
    $controller = ConsumerFactory::buildController($this, $response);
    return $controller->showServiceAvailabilityPage($request);  
})->add(Middleware::loginAccountVerify(Consumer::ACCOUNT_CONSUMER));

$app->any(RoutesName::CONSUMER.RoutesName::SERVICE_APPOINTMENTS_SUMMARY, function ($request, $response, $args) {

    $controller = ConsumerFactory::buildController($this, $response);
    return $controller->showAppointmentsSummary($request); 
})->add(Middleware::loginAccountVerify(Consumer::ACCOUNT_CONSUMER));

$app->any(RoutesName::CONSUMER.RoutesName::SERVICE_BOOKING, function ($request, $response, $args) {

    $controller = ConsumerFactory::buildController($this, $response);
    return $controller->takeBooking($request, $response); 
})->add(Middleware::loginAccountVerify(Consumer::ACCOUNT_CONSUMER));