<?php

namespace App\Consumer;

use App\Service\Service;
use App\Service\ServiceAppointment;
use App\Service\ServiceFactory;
use App\Service\ServiceFilters;
use App\Service\ServiceRepository;
use App\Service\ServiceAvailabilities;
use App\Factory\FactoryDto;
use App\Member\MemberController;
use App\View\HTMLView;
use App\Common\Configuration;
use App\Core\Session;
use App\Core\RepositoryException;
use App\Contact\Address;
use App\Consumer\Consumer;
use App\Consumer\ConsumerFactory;
use App\Consumer\ConsumerRepository;
use App\Merchant\MerchantRepository;
use App\User\User;
use App\Common\ParamsName;
use App\Common\RoutesName;
use App\Common\TemplatesName;

//
// Controlleur pour le consommateur
//
class ConsumerController extends MemberController
{
    private $serviceRepository = null;
    private $serviceAvailabilities = null;
    private $merchantRepository = null;

    public function setServiceFilters(ServiceFilters $serviceFilters)
    {
        $this->serviceFilters = $serviceFilters;
    }

    public function setServiceRepository(ServiceRepository $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    public function setServiceAvailabilities(ServiceAvailabilities $serviceAvailabilities)
    {
        $this->serviceAvailabilities = $serviceAvailabilities;
    }

    public function setMerchantRepository(MerchantRepository $merchantRepository)
    {
        $this->merchantRepository = $merchantRepository;
    }
    
    protected function getAccountType()
    {
        return User::ACCOUNT_CONSUMER;
    }
    
    protected function getHomePageName()
    {
        return TemplatesName::HOME_CONSUMER;
    }    
    
    protected function getRegistrationPageName()
    {
        return TemplatesName::REGISTRATION_CONSUMER;
    }
    
    protected function getConfirmationPageName()
    {
        return TemplatesName::CONFIRMATION_CONSUMER;
    }
    
    protected function getModificationPageName()
    {
        return TemplatesName::MODIFY_PARAMETERS_CONSUMER;
    }
 
    protected function getNewMemberInformation($request)
    {
        $consumer = ConsumerFactory::buildDtoRequest($request);
        $consumer->password = $request->getParam(ParamsName::REGISTER_PASSWORD, ''); 

        return $consumer;
    }
        
    protected function updateMemberInformation($request, $member)
    {                
        $member->firstName = $request->getParam(ParamsName::REGISTER_FIRSTNAME, $member->firstName);
        $member->lastName = $request->getParam(ParamsName::REGISTER_LASTNAME, $member->lastName);        
        $member->phone = $request->getParam(ParamsName::REGISTER_HOMEPHONE, $member->phone);
        $member->birthdate = $request->getParam(ParamsName::REGISTER_BIRTHDATE, $member->birthdate);
        
        $this->updateMemberAddress($request, $member->address); 
        
        // Update le member et retourne un object de type AppStatus
        $this->getRepository()->update($member);
    }
    
    //
    // Afficher la page d'accueil du membre
    //
    public function showHomePage($request, $response) 
    {
        try {
            
            // Indicateur utilisé suivant la prise de rendez-vous
            $bookingConfirmed = $request->getParam('booking_confirmed');
            $accountType = $this->getAccountType();

            $member = $this->getMemberFromSessionsUser();
            $statistics = $this->serviceRepository->getAppointmentStatistics($member->consumerId);

            $this->data['member'] = $member;
            $this->data['statistics'] = $statistics;
            $this->data['modifyRoute'] = $this->getUserRoute($accountType, RoutesName::USER_MODIFICATION);
            $this->data['searchServiceRoute'] = $this->getUserRoute($accountType, RoutesName::SERVICE_SEARCH);
            $this->data['viewServiceRoute'] = $this->getUserRoute($accountType, RoutesName::SERVICE_VIEW);
            $this->data['summaryRoute'] = $this->getUserRoute($accountType, RoutesName::SERVICE_APPOINTMENTS_SUMMARY);
            $this->data['bookingConfirmed'] = (int)$bookingConfirmed > 0;

            // Afficher la page d'accueil
            return $this->render($this->getHomePageName(), $this->data);
                
          } catch (RepositoryException $e) {
            
            $this->setMsgError($e->getMessage());
            
            // Afficher la page d'accueil
            return $this->render($this->getHomePageName(), $this->data);
        }
    }

    //
    // Affichage des recherches d'un service pour le consommateur
    //
    public function showServiceSearchPage($request) 
    {
        $this->data['member'] = $this->getMemberFromSessionsUser();

        try {

            $this->serviceFilters->setRequest($request);

            $availabalities = [];

            if (!empty($request->getParams())) {
                $availabalities = $this->prepareAvailabilities($request);
            }

            $this->data['homeRoute'] = RoutesName::CONSUMER.RoutesName::USER_HOME;
            $this->data['modifyRoute'] = RoutesName::CONSUMER.RoutesName::USER_MODIFICATION;
            $this->data['searchingRoute'] = RoutesName::CONSUMER.RoutesName::SERVICE_SEARCHING;
            $this->data['bookingRoute'] = RoutesName::CONSUMER.RoutesName::SERVICE_BOOKING;
            $this->data['availabalities'] = $availabalities;
            $this->data['searching'] = !empty($request->getParams());
            $this->data['filters'] = ServiceFactory::buildDtoRequest($request);

            return $this->render(TemplatesName::SEARCH_SERVICE, $this->data);

        } catch (RepositoryException $e) {

            $this->setMsgError($e->getMessage());

            return $this->render(TemplatesName::SEARCH_SERVICE, $this->data);
        }        
    } 

    //
    // Affichage de l'information du disponibilité pour un service
    //
    public function showServiceAvailabilityPage($request)
    {
        $serviceId = -1;
        $dateBegin = "";
        $dateEnd = "";
        $serviceId = $request->getParam(ParamsName::APPOINTMENT_SERVICE_ID, ""); 
        $dateBegin = $request->getParam(ParamsName::APPOINTMENT_DATE_BEGIN, "");
        $dateEnd = $request->getParam(ParamsName::APPOINTMENT_DATE_END, "");
        
        $this->data['member'] = $this->getMemberFromSessionsUser(); 

        try {
            
            $service      = $this->serviceRepository->getInfoFromId($serviceId);
            $serviceIds[] = $serviceId;
            $merchants    = $this->merchantRepository->getMerchantFromServices($serviceIds);
            $availability = ServiceFactory::buildDtoServiceAvailability($service, $merchants[$serviceId], $dateBegin, $dateEnd);

            $this->data['service'] = $service;
            $this->data['merchant'] = $merchants[$serviceId];
            $this->data['availability'] = $availability;

            return $this->render(TemplatesName::VIEW_SERVICE, $this->data);

        } catch (RepositoryException $e) {

            $this->setMsgError($e->getMessage());

            return $this->render(TemplatesName::VIEW_SERVICE, $this->data);

        }    

    }

    //
    // Affichage du sommaire du consommateur
    //
    public function showAppointmentsSummary($request)
    {
        $this->data['member'] = $this->getMemberFromSessionsUser();

        try {

            // Retrouver la liste des rendez-vous passé et futurs
            $user = $this->session()->getUser(); 
            $consumer = $this->getRepository()->getInfoFromId($user->id);
            $comingAppointments = $this->serviceRepository->getAppointments($consumer->consumerId, ServiceRepository::COMING_APPOINTMENTS);
            $passedAppointments = $this->serviceRepository->getAppointments($consumer->consumerId, ServiceRepository::PASSED_APPOINTMENTS);

            $this->data['comingAppointments'] = $comingAppointments;
            $this->data['passedAppointments'] = $passedAppointments;

            return $this->render(TemplatesName::CONSUMER_SUMMARY, $this->data);

        } catch (RepositoryException $e) {

            $this->setMsgError($e->getMessage());

            return $this->render(TemplatesName::CONSUMER_SUMMARY, $this->data);

        }     
    }

    protected function sendEmailConfirmationBooking(User $member, $params)
    {
        $this->data['params'] = $params;
        $this->data['member'] = $member;

        $this->mailService->send(TemplatesName::CONFIRMATION_BOOKING, $this->data, function($message) use ($member) {
            $message->to($member->email);
            $message->subject('Confirmation Rendez-Vous');
            $message->from('applicationrendezvous@gmail.com');
            $message->fromName('Rendez Vous');
        });
    }
    
    //
    // Prendre rendez-vous pour le service spécifié
    //
    public function takeBooking($request, $response)
    {
        try {
            
            $member = $this->getMemberFromSessionsUser();

            $appointment = ServiceFactory::buildDtoServiceAppointmentRequest($request);
            $appointment->consumerId = $member->consumerId;
            $appointment->state = ServiceAppointment::STATE_CONFIRMED;  // Autoconfirmé

            $this->serviceRepository->addServiceAppointment($appointment);

            $this->sendEmailConfirmationBooking($member, $request->getParams());

            // Rediriger sur la page d'acceuil
            $route = RoutesName::CONSUMER.RoutesName::USER_HOME;

            return $this->getView()->renderJson([
                'redirect' => "$route?booking_confirmed=1"
            ]);

        } catch (RepositoryException $e) {

            $this->setMsgError($e->getMessage());

            return $this->getView()->renderJson();

        }      
    }

    private function prepareAvailabilities($request)
    {
        $serviceIds = $this->serviceRepository->getServicesIdsFromFilters($this->serviceFilters);

        if (empty($serviceIds)) {
            return [];
        }

        $services       = $this->serviceRepository->getInfoFromIds($serviceIds);
        $merchants      = $this->merchantRepository->getMerchantFromServices($serviceIds);
        $appointments   = $this->serviceRepository->getServiceAppointments($serviceIds, $this->serviceFilters);

        $this->serviceAvailabilities->setServiceFilters($this->serviceFilters);
        $this->serviceAvailabilities->setMerchants($merchants);
        $this->serviceAvailabilities->setServices($services);
        $this->serviceAvailabilities->setUnavailableDates($appointments);

        $this->serviceAvailabilities->generate();

        return $this->serviceAvailabilities->getListing();
    }

}