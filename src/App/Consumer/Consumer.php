<?php

namespace App\Consumer;

use App\User\User;

//
// Information sur un consommateur
//
class Consumer extends User
{      
	public $consumerId = null;
}