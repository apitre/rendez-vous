<?php

namespace App\Consumer;

use App\Core\BasicRepository;
use App\Core\SQLDatabase;
use App\Consumer\ConsumerFactory;
use App\Consumer\Consumer;
use App\User\UserRepository;
use App\Contact\Address;
use App\Core\RepositoryException;

//
// Dépot de données du consommateur
//
class ConsumerRepository extends UserRepository
{
    public function add($consumer)
    { 
        $this->db->transaction(function() use ($consumer){

            parent::addAddress($consumer->address);
            
            parent::add($consumer);

            $sql = "INSERT INTO consumer (
                    consumer_id,
                    user_id
                )
                VALUES (
                    :consumer_id,
                    :user_id
                )";

            $rowCount = $this->db->querySimple($sql, [
                ':consumer_id'   => $consumer->consumerId,
                ':user_id'       => $consumer->id
            ]);

            $consumer->consumerId = $this->db->getLastInsertId();

        });
    }

    public function update($consumer)
    {
        return $this->db->transaction(function() use ($consumer){

            parent::update($consumer);

            parent::updateAdress($consumer->address);

        });
    }
    
    public function getInfoFromId($id) 
    {
        $sql = "SELECT 
                    user.*,
                    address.*,
                    consumer.consumer_id
                FROM
                    user
                INNER JOIN
                    consumer
                    ON consumer.user_id = user.user_id
                LEFT JOIN
                    address
                    ON address.address_id = user.address_id
                WHERE
                    user.user_id = :user_id";

        $rows = $this->db->query($sql, [
            ':user_id' => $id
        ]);

        if (empty($rows)) {
            throw new RepositoryException(8);
        }

        return ConsumerFactory::buildDtoDatabase($rows[0]);
    }
  
}