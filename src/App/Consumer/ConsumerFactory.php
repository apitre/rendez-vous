<?php

namespace App\Consumer;

use Psr\Http\Message\ServerRequestInterface;
use App\Core\BasicFactory;
use App\Core\SQLRow;
use App\Contact\Address;
use App\User\User;
use App\User\UserFactory;
use App\Consumer\Consumer;
use App\Consumer\ConsumerController;
use App\Consumer\ConsumerRepository;
use App\View\HTMLView;
use App\Common\ParamsName;
use App\Service\ServiceFilters;
use App\Service\ServiceRepository;
use App\Service\ServiceAvailabilities;
use App\Merchant\MerchantRepository;

class ConsumerFactory implements BasicFactory {

    public static function buildController($app, $response) 
    {
        $view                   = new HTMLView($app->view, $response);
        $repository             = new ConsumerRepository($app->db);
        $controller             = new ConsumerController($repository, $view, $app->session, $app->configuration);
        $serviceRepository      = new ServiceRepository($app->db);
        $serviceAvailabilities  = new ServiceAvailabilities();
        $serviceFilters         = new ServiceFilters();
        $merchantRepository     = new MerchantRepository($app->db);

        $controller->setServiceFilters($serviceFilters);
        $controller->setMerchantRepository($merchantRepository);
        $controller->setMailService($app->mailService);
        $controller->setServiceAvailabilities($serviceAvailabilities);
        $controller->setServiceRepository($serviceRepository);

        return $controller;
    }

    public static function buildDtoRequest(ServerRequestInterface $request) 
    {
        $consumer = new Consumer();
        $address = new Address();

        $address->number = $request->getParam(ParamsName::REGISTER_HOMEADDRNUM, "");
        $address->street = $request->getParam(ParamsName::REGISTER_HOMEADDRSTREET, "");
        $address->appartment = $request->getParam(ParamsName::REGISTER_HOMEADDRAPPART, "");
        $address->town = $request->getParam(ParamsName::REGISTER_HOMEADDRTOWN, "");
        $address->code = $request->getParam(ParamsName::REGISTER_HOMEADDRCODE, "");
        $address->province = $request->getParam(ParamsName::REGISTER_HOMEADDRPROVINCE, "");

        $consumer->accountType = User::ACCOUNT_CONSUMER;
        $consumer->firstName = $request->getParam(ParamsName::REGISTER_FIRSTNAME, "");
        $consumer->lastName = $request->getParam(ParamsName::REGISTER_LASTNAME, "");
        $consumer->phone = $request->getParam(ParamsName::REGISTER_HOMEPHONE, "");
        $consumer->email = $request->getParam(ParamsName::REGISTER_EMAIL, "");
        $consumer->userName = $request->getParam(ParamsName::REGISTER_USERNAME, "");
        $consumer->password = $request->getParam(ParamsName::REGISTER_PASSWORD, "");
        $consumer->birthdate = $request->getParam(ParamsName::REGISTER_BIRTHDATE, "");
        $consumer->address = $address;
        $consumer->image = UserFactory::loadUserImage($consumer->id, User::ACCOUNT_CONSUMER);

        return $consumer;
    }

    public static function buildDtoConsumer(SQLRow $row)
    {
        $consumer = new Consumer();

        $consumer->id = $row->user_id;
        $consumer->firstName = $row->firstname;
        $consumer->lastName = $row->lastname;
        $consumer->userName = $row->username;
        $consumer->accountState = $row->status;
        $consumer->accountType = $row->type;
        $consumer->memberDate = $row->creation_ts;
        $consumer->phone = $row->phone;
        $consumer->birthdate = $row->birthdate;
        $consumer->email = $row->email;
        $consumer->consumerId = $row->consumer_id;
        $consumer->image = UserFactory::loadUserImage($row->user_id, User::ACCOUNT_CONSUMER);

        return $consumer;
    }

    public static function buildDtoDatabase(SQLRow $row) 
    {
        $consumer = self::buildDtoConsumer($row);

        $address = new Address();

        $address->id = $row->address_id;
        $address->number = $row->address;
        $address->street = $row->street;
        $address->appartment = $row->appartment;
        $address->town = $row->city;
        $address->code = $row->zip_code;
        $address->province = $row->province;

        $consumer->address = $address;

        return $consumer;
    }

}
