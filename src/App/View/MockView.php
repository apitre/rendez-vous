<?php

namespace App\View;

use App\View\HTMLView;

class MockView extends HTMLView
{
	public function render($template, $data)
    {        
        return [
        	'template' 	=> $template,
        	'data'		=> $data
        ];
    } 
}