<?php

namespace App\View;

use \Slim\Views\Twig;
use App\View\BasicView;

//
// Vue HTML
//
class HTMLView extends BasicView
{    
    protected $httpResponse;

    //
    // Constructeur
    //
    public function __construct(Twig $viewRenderer, $httpResponse)
    {
        parent::__construct($viewRenderer);

        $this->httpResponse = $httpResponse;
    }
    
    //
    // Génère le contenu HTML
    //
    public function render($template, $data)
    {        
        return $this->viewRenderer->render($this->httpResponse, $template, $data);
    } 

    //
    // Génère le resultat JSON
    //
    public function renderJson($data)
    {        
        return $this->httpResponse
                            ->withHeader('Content-Type', 'application/json')
                            ->withStatus(200)
                            ->withJson($data, null, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK);
    } 

    //
    // Génère le contenu JSON avec un index html
    //
    public function renderHtmlJson($template, $data)
    {        
        return $this->json_encode([
            'html' => $this->viewRenderer->render($this->httpResponse, $template, $data)
        ]);
    } 
}