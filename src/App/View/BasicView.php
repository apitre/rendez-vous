<?php

namespace App\View;

use \Slim\Views\Twig;

//
// Classe de base d'une vue
//
abstract class BasicView
{
    protected $viewRenderer;    

    //
    // Constructeur
    //
    public function __construct(Twig $viewRenderer)
    {
        $this->viewRenderer = $viewRenderer;
    }

}