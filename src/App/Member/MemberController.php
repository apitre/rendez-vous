<?php

namespace App\Member;

use App\Common\Configuration;
use App\Core\Session;
use App\Core\MailService;
use App\Core\BasicController;
use App\User\User;
use App\User\UserFactory;
use App\Contact\Address;
use App\View\HTMLView;
use App\Core\RepositoryException;
use App\Common\RoutesName;
use App\Common\TemplatesName;
use App\Common\ParamsName;

//
// Controlleur pour le membre du site
//
abstract class MemberController extends BasicController {

    protected $mailService = null;

    //
    // Constructeur
    //
    public function __construct($repository, HTMLView $view, Session $session, Configuration $configuration) {
        parent::__construct($repository, $view, $session, $configuration);
    }

    public function setMailService(MailService $mailService) {
        $this->mailService = $mailService;
    }

    abstract protected function getNewMemberInformation($request);

    abstract protected function getAccountType();

    abstract protected function getHomePageName();

    abstract protected function getRegistrationPageName();

    abstract protected function getConfirmationPageName();

    abstract protected function getModificationPageName();

    abstract protected function updateMemberInformation($request, $member);

    abstract public function showHomePage($request, $response);
    
    protected function updateMemberAddress($request, Address $address) {
        $address->number = $request->getParam(ParamsName::REGISTER_HOMEADDRNUM, $address->number);
        $address->street = $request->getParam(ParamsName::REGISTER_HOMEADDRSTREET, $address->street);
        $address->appartment = $request->getParam(ParamsName::REGISTER_HOMEADDRAPPART, $address->appartment);
        $address->town = $request->getParam(ParamsName::REGISTER_HOMEADDRTOWN, $address->town);
        $address->code = $request->getParam(ParamsName::REGISTER_HOMEADDRCODE, $address->code);
        $address->province = $request->getParam(ParamsName::REGISTER_HOMEADDRPROVINCE, $address->province);
    }

    protected function sendEmailConfirmation(User $member) {
        $token = $this->getRepository()->getPendingUniqueKey($member->id);
        $config = $this->getConfiguration();

        $this->data['href'] = $config->getDomain().RoutesName::ACTIVATE_USER."?token=".$token;
        $this->data['member'] = $member;

        $this->mailService->send(TemplatesName::CONFIRMATION_MEMBER, $this->data, function($message) use ($member) {
            $message->to($member->email);
            $message->subject('Confirmation Enregistrement');
            $message->from('applicationrendezvous@gmail.com');
            $message->fromName('Rendez Vous');
        });
    }

    //
    // Affichage de la page d'enregistrement d'un membre
    //
    public function showRegistrationPage($request) {
        $accountType = $this->getAccountType();

        $this->data['accountType'] = $accountType;
        $this->data['member'] = $this->getNewMemberInformation($request);
        $this->data['registerRoute'] = $this->getUserRoute($accountType, RoutesName::USER_REGISTER);

        return $this->render($this->getRegistrationPageName(), $this->data);
    }

    //
    // Enregistrer le nouvel utilisateur sur le site
    //
    public function register($request, $response) {
        
        $member = $this->getNewMemberInformation($request);
        
        try {
            
            // Conserver l'information sur le nouveau membre
            $this->getRepository()->add($member);
            
        } catch (RepositoryException $e) {

            $accountType = $this->getAccountType();

            $this->setMsgError($e->getMessage());

            // Message d'erreur provenant du repository
            $this->data['accountType'] = $accountType;
            $this->data['member'] = $member;
            $this->data['registerRoute'] = $this->getUserRoute($accountType, RoutesName::USER_REGISTER);

            return $this->render($this->getRegistrationPageName(), $this->data);
        }

        $this->setUserOnline($member);

        $route = $this->getUserRoute($member->accountType, RoutesName::USER_CONFIRMATION);

        return $response->withRedirect($route);
    }

    // 
    // Afficher la page de confirmation de l'enregistrement
    //
    public function showRegistrationConfirmation($request, $response) {
        
        try {

            $member = $this->getMemberFromSessionsUser();
            
            if ($member->accountState == User::ACCOUNT_NOT_CONFIRMED) {

                if ($this->getConfiguration()->isEmailConfirmationEnabled() == false) {
                    $member->accountState = User::ACCOUNT_ACTIVATED;
                    $this->getRepository()->update($member);
                    $this->setUserOnline($member);
                } else {
                    //Envoi d'email pour confirmation du compte via token
                    $this->sendEmailConfirmation($member);
                }

                $accountType = $this->getAccountType();

                $this->data['member'] = $member;
                $this->data['homeRoute'] = $this->getUserRoute($accountType, RoutesName::USER_HOME);

                // Afficher la page de confirmation
                $this->render($this->getConfirmationPageName(), $this->data);

            } else if ($member->accountState == User::ACCOUNT_ACTIVATED) {

                // Rediriger l'usager sur sa page personnel lorsque son compte est confirmé
                $route = $this->getUserRoute($member->accountType, RoutesName::USER_HOME);
                return $response->withRedirect($route);

            }
        
        } catch (RepositoryException $e) {

            $accountType = $this->getAccountType();

            $this->setMsgError($e->getMessage());

            $this->data['registerRoute'] = $this->getUserRoute($accountType, RoutesName::USER_REGISTER);

            return $this->render($this->getRegistrationPageName(), $this->data);

        } 
    }

    //
    // Affichage de la page de modification des paramètres du membre
    //
    public function showModificationPage($request, $response) {
       
        try {
            
            $userUpdated = (int) $request->getParam('updated');
            $member = $this->getMemberFromSessionsUser();
            $accountType = $this->getAccountType();

            $this->data['member'] = $member;
            $this->data['updateRoute'] = $this->getUserRoute($accountType, RoutesName::USER_MODIFY);
            $this->data['homeRoute'] = $this->getUserRoute($accountType, RoutesName::USER_HOME);
            $this->data['userUpdated'] = (int) $userUpdated > 0;
            
            // Afficher la page de modification
            return $this->render($this->getModificationPageName(), $this->data);
            
        } catch (RepositoryException $e) {
            
            $accountType = $this->getAccountType();

            $this->setMsgError($e->getMessage());

            $this->data['updateRoute'] = $this->getUserRoute($accountType, RoutesName::USER_MODIFY);
            $this->data['homeRoute'] = $this->getUserRoute($accountType, RoutesName::USER_HOME);

            return $this->render($this->getModificationPageName(), $this->data);

        }
          
    }

    //
    // Mettre a jour les paramètres du membre
    //
    public function updateParameters($request, $response) {
        $member = null;
        try {
            
            $member = $this->getMemberFromSessionsUser();
            $this->updateMemberInformation($request, $member);
            $this->updateMemberImage($request, $member);
            
            $route = $this->getUserRoute($member->accountType, RoutesName::USER_MODIFICATION);

            $this->setMsgSuccess('Votre profil a été mis à jour');

            return $response->withRedirect("$route?updated=1");
            
        } catch (RepositoryException $e) {
            
            $accountType = $this->getAccountType();

            $this->data['member'] = $member;
            $this->data['updateRoute'] = $this->getUserRoute($accountType, RoutesName::USER_MODIFY);
            $this->data['homeRoute'] = $this->getUserRoute($accountType, RoutesName::USER_HOME);
            
            $this->setMsgError($e->getMessage());

            return $this->render($this->getModificationPageName(), $this->data);

        }  
    }

    //
    // Modifier l'image de l'utilisateur
    //
    protected function updateMemberImage($request, $member) {
        if ($_FILES[ParamsName::MODIFY_PARAM_USER_IMAGE]['error'] === UPLOAD_ERR_OK){
            $files = $request->getUploadedFiles();
            $imageFile = $files[ParamsName::MODIFY_PARAM_USER_IMAGE];
            $imageUser = User::IMAGE_PATH."/{$member->id}.png";            
            $imageFile->moveTo(__PUBLIC__.$imageUser);
            $member->image = $imageUser;
            return true;
        }
        return false;
    }

}