<?php

namespace App\Lib;

use App\User\UserFactory;
use App\Service\ServiceFactory;
use App\Core\Session;
use App\Core\SQLRow;
use App\Core\MockPDO;
use App\Core\SQLDatabase;
use App\Common\Configuration;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class UnitTestFactory extends \PHPUnit_Framework_TestCase
{
    public function buildHTTPQuery($params){
        $query = [];

        foreach($params as $key => $value ){

            $query[] = urlencode($key).'='.urlencode($value);

        }

        return implode('&', $query);
    }

    public function request($path, $params = [])
    {
        $environment = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI' => $path,
            'QUERY_STRING'=> $this->buildHTTPQuery($params)
        ]);

        return Request::createFromEnvironment($environment);
    }

    public function getAccess()
    {
        $row = new SQLRow();

        $row->id = 1;
        $row->salt = 'd618cc7.43622690';
        $row->token = '$2y$10$ASnU6MnKDh6HVi9ZJO9kwOuiJhVhXD1ZgIi8c2E8r/HHCE2029RGm';

        return $row;
    }

    public static function getUser()
    {
        $row = new SQLRow();

        $row->user_id = '1';
        $row->firstname = "Alain";
        $row->lastname = "Pitree";
        $row->username = "pita";
        $row->status = '1';
        $row->type = '1';
        $row->creation_ts = "2017-03-05 14:26:59";   
        $row->phone = null;
        $row->birthdate = null;
        $row->email = "test@gmail.com";

        return UserFactory::buildDtoDatabase($row);
    }

    public static function getServiceList()
    {
        $row1 = new SQLRow();

        $row1->service_id = '1';
        $row1->name = "Coupe de cheveux";
        $row1->description = "À venir";
        $row1->price = "14.99";
        $row1->category = null;

        $row2 = new SQLRow();

        $row2->service_id = '2';
        $row2->name = "Coupe de cheveux";
        $row2->description = "À venir";
        $row2->price = "14.99";
        $row2->category = null;

        $row3 = new SQLRow();

        $row3->service_id = '3';
        $row3->name = "Coupe de cheveux";
        $row3->description = "À venir";
        $row3->price = "14.99";
        $row3->category = null;

        return [
            0 => ServiceFactory::buildDtoDatabase($row1),
            1 => ServiceFactory::buildDtoDatabase($row2),
            2 => ServiceFactory::buildDtoDatabase($row3)
        ];
    }

    public function buildDic()
    {   
        $pdo = new MockPDO();

        $container = [
            'mailService'   => null,
            'view'          => new Twig('', []),
            'db'            => $this->getMock('\App\Core\SQLDatabase', [], [$pdo]),
            'session'       => new Session(),
            'configuration' => new Configuration()
        ];

        return (object)$container;
    }
}