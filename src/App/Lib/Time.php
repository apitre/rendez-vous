<?php

namespace App\Lib;

class Time
{
	private $seconds = 0;

	public function __construct($time)
	{
		$t = date_parse($time);
		$this->seconds = $t['hour'] * 3600 + $t['minute'] * 60 + $t['second'];
	}

	public function add(Time $time)
	{
		$this->seconds += $time->seconds;
	}

	public function subtract(Time $time)
	{
		$this->seconds -= $time->seconds;
	}

	public function seconds()
	{
		return $this->seconds;
	}

	public function toString()
	{
		return sprintf(
			'%02d:%02d:%02d', 
			floor($this->seconds / 3600), 
			floor($this->seconds / 60 % 60), 
			floor($this->seconds % 60)
		);
	}

}