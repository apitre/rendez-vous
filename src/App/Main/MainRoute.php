<?php

namespace App\Main;

use App\Common\RoutesName;

$app->get(RoutesName::HOME, function ($request, $response, $args) {
    
    $controller = MainFactory::buildController($this, $response);
    // Affichage de la page d'authentification                                  
    return $controller->showLoginPage();
});

$app->any(RoutesName::REGISTRATION_TYPE, function ($request, $response, $args) {
    
    $controller = MainFactory::buildController($this, $response);
    // Afficher la page de type d'inscription                                   
    return $controller->showRegistrationTypePage();

});

$app->any(RoutesName::TEST_SARAH, function ($request, $response, $args) {
    
    $controller = MainFactory::buildController($this, $response);
    // Afficher la page de type d'inscription                                   
    return $controller->showSarahTest();

});


