<?php

namespace App\Main;

use Psr\Http\Message\ServerRequestInterface;
use App\View\HTMLView;
use App\Main\MainRepository;
use App\Main\MainController;
use App\Core\BasicFactory;
use App\Core\SQLRow;

class MainFactory implements BasicFactory {

    public static function buildController($app, $response) 
    {
        $view = new HTMLView($app->view, $response);
        $repository = new MainRepository($app->db);
        $controller = new MainController($repository, $view, $app->session, $app->configuration);

        return $controller;
    }

    public static function buildDtoRequest(ServerRequestInterface $request)
    {
        return null;
    }

    public static function buildDtoDatabase(SQLRow $row) 
    {
        return null;
    }
}
