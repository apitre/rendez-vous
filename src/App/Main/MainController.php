<?php

namespace App\Main;

use App\Common\TemplatesName;
use App\Core\BasicController;

class MainController extends BasicController
{

    public function showRegistrationTypePage()
    {
        $this->render(TemplatesName::REGISTRATION_TYPE, $this->data);
    }

    public function showSarahTest()
    {
        $this->render(TemplatesName::TEST_SARAH, $this->data);
    }

    public function showLoginPage()
    {
        $this->render(TemplatesName::LOGIN, $this->data);
    }

}