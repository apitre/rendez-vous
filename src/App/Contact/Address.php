<?php

namespace App\Contact;

//
// Information sur une addresse
//
class Address
{    
    public $id = null;
    public $number = 0;          // numéro de porte
    public $street = '';         // rue
    public $appartment = 0;      // numéro appartement
    public $town = '';           // ville
    public $province = '';       // province
    public $code = '';           // code postal
}