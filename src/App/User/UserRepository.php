<?php

namespace App\User;

use App\User\UserFactory;
use App\Core\BasicRepository;
use App\Core\SQLDatabase;
use App\User\User;
use App\Core\Token;
use App\Core\AppStatusException;
use App\Contact\Address;
use App\Core\RepositoryException;

//
// Dépot de données du consommateur
//
class UserRepository extends BasicRepository 
{
    public function add($user)
    {
        if ($this->getInfoFromName($user->userName) != null) {
            throw new RepositoryException(2);
        }

        if ($this->getInfoFromName($user->email) != null) {
            throw new RepositoryException(7);
        }

        $sql = "INSERT INTO 
                    user 
                VALUES (
                    :user_id,
                    :lastname,
                    :firstname,
                    :username,
                    :email,
                    :type,
                    now(),
                    :status,
                    :phone,
                    :birthdate,
                    :address_id
                )";

        $rowCount = $this->db->querySimple($sql, [
            ':user_id'      => $user->id,
            ':lastname'     => $user->lastName,  
            ':firstname'    => $user->firstName,   
            ':username'     => $user->userName,  
            ':email'        => $user->email,   
            ':type'         => $user->accountType,
            ':status'       => $user->accountState,
            ':phone'        => $user->phone,
            ':birthdate'    => $user->birthdate,
            ':address_id'   => $user->address->id
        ]);

        $user->id = $this->db->getLastInsertId();

        if ($user->accountState != User::ACCOUNT_NOT_CONFIRMED) {
            throw new RepositoryException(3);
        }

        if (!$this->addAccess($user)) {
            throw new RepositoryException(1);
        }

        if (!$this->addPending($user)) {
            throw new RepositoryException(1);
        }
    }

    public function updateAdress(Address $address)
    {
        $sql = "UPDATE
                    address
                SET
                    address = :address,
                    street = :street,
                    appartment = :appartment,
                    city = :city,
                    province = :province,
                    zip_code = :zip_code
                WHERE
                    address_id = :address_id";

        $appStatus = $this->db->querySimple($sql, [
            ':address_id'   => $address->id,
            ':address'      => $address->number,  
            ':street'       => $address->street,   
            ':appartment'   => $address->appartment,  
            ':city'         => $address->town,
            ':province'     => $address->province,
            ':zip_code'     => $address->code
        ]);

        $address->id = $this->db->getLastInsertId();
        
        return $appStatus;
    }

    public function addAddress(Address $address)
    {
        $sql = "INSERT INTO 
                    address
                VALUES (
                    :address_id,
                    :address,
                    :street,
                    :appartment,
                    :city,
                    :province,
                    :zip_code
                )";

        $appStatus = $this->db->querySimple($sql, [
            ':address_id'   => $address->id,
            ':address'      => $address->number,  
            ':street'       => $address->street,   
            ':appartment'   => $address->appartment,  
            ':city'         => $address->town,
            ':province'     => $address->province,
            ':zip_code'     => $address->code
        ]);

        $address->id = $this->db->getLastInsertId();
        
        return $appStatus;
    }

    public function getPendingUserId($token)
    {
        $sql = "SELECT 
                    user_id
                FROM 
                    user_pending
                WHERE
                    unique_key = :unique_key";

        $row = $this->db->query($sql, [
            ':unique_key' => $token
        ]);

        if (empty($row)) {
            throw new RepositoryException(6);
        }

        return $row[0]->user_id;      
    }

    public function activateAccount(User $user)
    {
        $this->db->transaction(function() use ($user) {

            $sql = "DELETE FROM 
                        user_pending
                    WHERE
                        user_id = :user_id";

            $rowCount = $this->db->querySimple($sql, [
                ':user_id' => $user->id
            ]);

            $user->accountState = User::ACCOUNT_ACTIVATED;

            if ($rowCount == 0 || !$this->update($user)) {
                throw new RepositoryException(1);
            }

        });
    }

    public function getPendingUniqueKey($userId)
    {
        $sql = "SELECT 
                    unique_key
                FROM 
                    user_pending
                WHERE
                    user_id = :user_id";

        $row = $this->db->query($sql, [
            ':user_id' => $userId
        ]);

        if (empty($row)) {
            return "";
        }

        return $row[0]->unique_key;      
    }

    public function addPending(User $user)
    {
        $sql = "INSERT INTO user_pending (
                    unique_key,
                    user_id
                ) VALUES (
                    :unique_key,
                    :user_id
                )";

        $salt = Token::salt();

        $unique_key = md5($user->email.$user->id.$salt);

        $rowCount = $this->db->query($sql, [
            ':user_id' => $user->id,
            ':unique_key' => $unique_key
        ]);  

        return $rowCount > 0;      
    }

    public function deactivateAccount(User $user)
    {
        // Mettre a jour le statut du compte comme étant désactivé
         if (!$user instanceof User) {
            throw new RepositoryException(1);
        }

        $sql = "UPDATE 
                    user
                SET
                    status = :status
                WHERE
                    user_id = :user_id";


        $rowCount = $this->db->querySimple($sql, [
            ':user_id'      => $user->id,  
            ':status'       => User::ACCOUNT_DESACTIVATED
        ]);

        return $rowCount > 0;
    }
    
    public function getAccess($userId) 
    {
        $sql = "SELECT
                    *
                FROM
                    user_access
                WHERE
                    user_id = :user_id";

        $binds = [
            ':user_id' => $userId
        ];

        $row = $this->db->query($sql, $binds);

        if (empty($row)) {
            return null;
        }

        return $row[0];
    }

    public function addAccess(User $user)
    {
        if (empty($user->password)) {
            throw new RepositoryException(1);
        }

        $sql = "INSERT INTO user_access (
                    user_id,
                    salt,
                    token
                ) VALUES (
                    :user_id,
                    :salt,
                    :token
                )";

        $salt = Token::salt();

        $token = Token::hash($salt, $user->password);

        $rowCount = $this->db->query($sql, [
            ':user_id'  => $user->id,
            ':salt'     => $salt,  
            ':token'    => $token
        ]);

        return $rowCount > 0;
    }
    
    
    public function update($user)
    {        
        if (!$user instanceof User) {
            throw new RepositoryException(1);
        }

        $sql = "UPDATE 
                    user
                SET
                    lastname = :lastname,
                    firstname = :firstname,
                    status = :status,
                    phone = :phone,
                    birthdate = :birthdate
                WHERE
                    user_id = :user_id";


        $rowCount = $this->db->querySimple($sql, [
            ':user_id'      => $user->id,
            ':lastname'     => $user->lastName,  
            ':firstname'    => $user->firstName,   
            ':status'       => $user->accountState,
            ':phone'        => $user->phone,
            ':birthdate'    => $user->birthdate
        ]);

        return $rowCount > 0;
    }

    public function getInfoFromId($id) 
    {
        $sql = "SELECT
                    *
                FROM
                    user
                WHERE
                    user_id = :user_id";

        $binds = [
            ':user_id' => $id
        ];

        $row = $this->db->query($sql, $binds);

        if (empty($row)) {
            return null;
        }

        return UserFactory::buildDtoDatabase($row[0]);
    }

    public function getInfoFromName($userName) 
    {
        $sql = "SELECT
                    *
                FROM
                    user
                WHERE
                    username = :username
                    OR email = :email";

        $binds = [
            ':username' => $userName,
            ':email'    => $userName
        ];

        $rows = $this->db->query($sql, $binds);

        if (empty($rows)) {
            return null;
        }

        return UserFactory::buildDtoDatabase($rows[0]);
    }
    
    public function authentify($user, $password) 
    {
        if (!$user instanceof User) {
            throw new RepositoryException(4);
        }

        $access = $this->getAccess($user->id);

        if (empty($access) || empty($password)) {
            throw new RepositoryException(1);
        }

        if (!Token::validate($access->salt, $password, $access->token)) {
            throw new RepositoryException(4);
        }

        if ($user->accountState != User::ACCOUNT_ACTIVATED) {
            throw new RepositoryException(5);
        }
    }
}