<?php

namespace App\User;

use App\Core\BasicController;
use App\Common\Configuration;
use App\Core\Session;
use App\View\HTMLView;
use App\User\UserRepository;
use App\Core\RepositoryException;
use App\Common\TemplatesName;
use App\Common\RoutesName;
use App\Common\ParamsName;

//
// Controlleur pour l'utilisateur
//
class UserController extends BasicController
{
    //
    // Constructeur
    //
    public function __construct(UserRepository $repository, HTMLView $view, Session $session, Configuration $configuration) 
    {
         parent::__construct($repository, $view, $session, $configuration);
    }  

    //
    // Authentifier l'utilisateur
    //
    public function signin($request, $response)
    {
        // Récupérer les informations d'authentification dans le body de la requete.
        try {

            $username = strval($request->getParam(ParamsName::AUTH_USERNAME, ''));
            $user = $this->getRepository()->getInfoFromName($username);
            $password = $request->getParam(ParamsName::AUTH_PASSWORD, '');
            $this->getRepository()->authentify($user, $password);
            $this->setUserOnline($user);

            // Rediriger sur la page d'accueil de l'utilisateur
            $route = $this->getUserRoute($user->accountType, RoutesName::USER_HOME);
            return $response->withRedirect($route);

        } catch (RepositoryException $e) {

            // Retourner l'erreur à l'usagé
            $this->setMsgError($e->getMessage());
            return $this->render(TemplatesName::LOGIN, $this->data);

        }
    }

    public function activate($request, $response)
    {
        $token = $request->getParam(ParamsName::AUTH_TOKEN, "");
        
        try {

            //Activation du compte via Token from email
            $userId = $this->getRepository()->getPendingUserId($token);
            $user = $this->getRepository()->getInfoFromId($userId);
            $this->getRepository()->activateAccount($user);

        } catch (RepositoryException $e) {
            
            //$this->setMsgError($e->getMessage());
            
        }

        $this->data['user'] = isset($user) && $user->id > 0 ? $user : null;

        return $this->render(TemplatesName::ACTIVATE, $this->data);
    }

    //
    // Mettre fin a la session de l'utilisateur
    //
    public function signout($request, $response)
    {
        $this->setUserOffline();

        // Rediriger sur la page d'accueil du site
        return $response->withRedirect(RoutesName::HOME);
    }
    //
    // Désactivation du compte de l'utilisateur
    //
    public function deactivate($request, $response)
    {
        $user = $this->session()->getUser();
        if ($user != null) {

            try {

                $user = $this->getRepository()->getInfoFromId($user->id);
                // Mettre a jour le statut du compte comme étant désactivé
                if ($this->getRepository()->deactivateAccount($user)) {
                    // Mettre a la session de l'utilisateur lorsque le compte est désactivé
                    return $this->signout($request, $response);
                }

            } catch (RepositoryException $e) {
                $this->setMsgError($e->getMessage());
                $this->render(TemplatesName::LOGIN, []);
            }
        }
    } 
}