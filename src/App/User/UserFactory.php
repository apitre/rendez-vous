<?php

namespace App\User;

use Psr\Http\Message\ServerRequestInterface;
use App\Core\BasicFactory;
use App\Core\SQLRow;
use App\User\UserController;
use App\User\UserRepository;
use App\View\HTMLView;
use App\Common\ParamsName;

class UserFactory implements BasicFactory {

    public static function buildController($app, $response) {
        $view = new HTMLView($app->view, $response);
        $repository = new UserRepository($app->db);
        $controller = new UserController($repository, $view, $app->session, $app->configuration);
        return $controller;
    }

    public static function buildDtoRequest(ServerRequestInterface $row) {
        return null;
    }

    public static function loadUserImage($userId, $accountType) {
        $customImagePath = User::IMAGE_PATH . "/$userId.png";

        if (file_exists(__PUBLIC__ . $customImagePath)) {
            return $customImagePath;
        } else {
            return User::IMAGE_PATH."/default_$accountType.png";
        }
    } 

    public static function buildDtoDatabase(SQLRow $row) {
        $user = new User();

        $user->id = $row->user_id;
        $user->firstName = $row->firstname;
        $user->lastName = $row->lastname;
        $user->userName = $row->username;
        $user->accountState = $row->status;
        $user->accountType = $row->type;
        $user->memberDate = $row->creation_ts;
        $user->phone = $row->phone;
        $user->birthdate = $row->birthdate;
        $user->email = $row->email;

        return $user;
    }

}
