<?php

namespace App\User;

use App\Contact\Address;

//
// Information sur un utilisateur
//
class User
{
    // Liste des états du compte
    const ACCOUNT_NOT_CONFIRMED = 0;            // nouveau compte, enregistrement non confimé
    const ACCOUNT_ACTIVATED = 1;                // compte actif
    const ACCOUNT_DESACTIVATED = 2;             // compte inactif
    const IMAGE_PATH = "/media/image/user";
    const HOME_ROUTE_MERCHANT = "/merchant/home";
    const HOME_ROUTE_CONSUMER = "/consumer/home";
    const EDIT_ROUTE_MERCHANT = "/merchant/modification";
    const EDIT_ROUTE_CONSUMER = "/consumer/modification";
    const IMAGE_EXTENSION = ".png";
    
    // Type de compte
    const ACCOUNT_NONE = 0;                     // compte inconnu
    const ACCOUNT_CONSUMER = 1;                 // compte consommateur
    const ACCOUNT_MERCHANT = 2;                 // compte marchant
    
    public $id = 0;                             // identifiant unique de l'utilisateur
    public $userName = '';                      // nom utilisateur
    public $lastName = '';                      // nom de famille
    public $firstName = '';                     // prenom
    public $email = '';                         // courriel
    public $memberDate = '0000-00-00 00:00:00'; // date d'inscription
    public $phone = 0;                          // numéro téléphone résidentiel
    public $address = null;                     // adresse résidentielle complete
    public $password = '';
    public $birthdate = '';
    public $accountType = User::ACCOUNT_CONSUMER;         // type de compte
    public $accountState = User::ACCOUNT_NOT_CONFIRMED;   // état du compte
    public $image = "";
}