<?php

namespace App\User;

use App\User\UserFactory;
use App\Common\RoutesName;
use App\Core\Middleware;

$app->any(RoutesName::SIGNIN_USER, function ($request, $response, $args) {
              
    // Authentifier l'utilisateur
    $controller = UserFactory::buildController($this, $response);
    return $controller->signin($request, $response);               
});

$app->any(RoutesName::SIGNOUT_USER, function ($request, $response, $args) {
               
    // Mettre fin a la session de l'utilisateur
    $controller = UserFactory::buildController($this, $response);
    return $controller->signout($request, $response);               
})->add(Middleware::loginVerify());

$app->any(RoutesName::ACTIVATE_USER, function ($request, $response, $args) {
               
    $controller = UserFactory::buildController($this, $response);
    return $controller->activate($request, $response);               
});

$app->any(RoutesName::DEACTIVATE_ACCOUNT, function ($request, $response, $args) {
               
    // Désactivation du compte de l'utilisateur
    $controller = UserFactory::buildController($this, $response);
    return $controller->deactivate($request, $response);               
})->add(Middleware::loginVerify());


