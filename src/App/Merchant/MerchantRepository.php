<?php

namespace App\Merchant;

use App\Core\BasicRepository;
use App\Core\SQLDatabase;
use App\Merchant\Merchant;
use App\Merchant\MerchantFactory;
use App\Contact\Address;
use App\User\UserRepository;
use App\Core\RepositoryException;

//
// Dépot de données du commercant
//
class MerchantRepository extends UserRepository
{
    public function add($merchant)
    { 
        $this->db->transaction(function() use ($merchant){

            parent::addAddress($merchant->address);
            
            parent::addAddress($merchant->businessAddress);

            parent::add($merchant);

            $sql = "INSERT INTO merchant (
                    merchant_id,
                    user_id,
                    name,
                    address_id,
                    email,
                    phone
                )
                VALUES (
                    :merchant_id,
                    :user_id,
                    :name,
                    :address_id,
                    :email,
                    :phone
                )";

            $rowCount = $this->db->querySimple($sql, [
                ':merchant_id' => $merchant->merchantId,
                ':user_id'     => $merchant->id,
                ':name'         => $merchant->businessName,
                ':email'        => $merchant->businessEmail,
                ':address_id'  => $merchant->businessAddress->id,
                ':phone'        => $merchant->businessPhone
            ]);

            $merchant->merchantId = $this->db->getLastInsertId();   

        });
    }
    
    public function update($merchant)
    {
        return $this->db->transaction(function() use ($merchant){
        
            parent::update($merchant);

            parent::updateAdress($merchant->address);
            
            parent::updateAdress($merchant->businessAddress);
       
            $sql = "UPDATE
                    merchant
                SET
                    name  = :name,
                    email = :email,
                    phone = :phone
                WHERE
                    merchant_id = :merchant_id";           
        
            $rowCount = $this->db->querySimple($sql, [
                ':name'           => $merchant->businessName,
                ':email'          => $merchant->businessEmail,  
                ':phone'          => $merchant->businessPhone,
                ':merchant_id'  => $merchant->merchantId
            ]);
           
            return $rowCount > 0 ;
        });
    }

    public function getMerchantFromServices($serviceIds)
    {
        $sql = "SELECT 
                    user.*,
                    address.*,
                    service_id,
                    merchant.merchant_id,
                    merchant.phone AS business_phone,
                    merchant.email AS business_email,
                    merchant.name AS business_name,
                    merchant.address_id AS business_address_id,
                    MerchantAddress.address AS business_address,
                    MerchantAddress.street AS business_street,
                    MerchantAddress.appartment AS business_appartment,
                    MerchantAddress.city AS business_city,
                    MerchantAddress.zip_code AS business_zip_code,
                    MerchantAddress.province AS business_province
                FROM
                    service
                INNER JOIN
                    merchant
                    ON merchant.merchant_id = service.merchant_id
                INNER JOIN
                    user
                    ON user.user_id = merchant.user_id
                LEFT JOIN
                    address
                    ON address.address_id = user.address_id
                LEFT JOIN
                    address AS MerchantAddress
                    ON MerchantAddress.address_id = merchant.address_id
                WHERE
                    service_id IN :service_ids";

        $rows = $this->db->query($sql, [
            ':service_ids' => $serviceIds
        ]);

        $merchants = [];

        foreach ($rows as $row) {
            $merchants[$row->service_id] = MerchantFactory::buildDtoDatabase($row);
        }

        return $merchants;
    }
    
    public function getInfoFromId($id) 
    {
        $sql = "SELECT 
                    user.*,
                    address.*,
                    merchant.merchant_id,
                    merchant.phone AS business_phone,
                    merchant.email AS business_email,
                    merchant.name AS business_name,
                    merchant.address_id AS business_address_id,
                    MerchantAddress.address AS business_address,
                    MerchantAddress.street AS business_street,
                    MerchantAddress.appartment AS business_appartment,
                    MerchantAddress.city AS business_city,
                    MerchantAddress.zip_code AS business_zip_code,
                    MerchantAddress.province AS business_province
            
                FROM
                    user
                INNER JOIN
                    merchant
                    ON merchant.user_id = user.user_id
                LEFT JOIN
                    address 
                    ON address.address_id = user.address_id
                LEFT JOIN
                    address AS MerchantAddress
                    ON MerchantAddress.address_id = merchant.address_id
                WHERE
                    user.user_id = :user_id";

        $rows = $this->db->query($sql, [
            ':user_id' => $id
        ]);

        if (empty($rows)) {
            throw new RepositoryException(8);
        }

        return MerchantFactory::buildDtoDatabase($rows[0]);
    }
       
}