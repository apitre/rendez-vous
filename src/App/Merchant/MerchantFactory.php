<?php

namespace App\Merchant;

use Psr\Http\Message\ServerRequestInterface;
use App\Core\BasicFactory;
use App\Core\SQLRow;
use App\Contact\Address;
use App\User\User;
use App\User\UserFactory;
use App\Merchant\Merchant;
use App\Merchant\MerchantController;
use App\Merchant\MerchantRepository;
use App\View\HTMLView;
use App\Common\ParamsName;

class MerchantFactory 
{

    public static function buildController($app, $response) 
    {
        $view = new HTMLView($app->view, $response);
        $repository = new MerchantRepository($app->db);
        $controller = new MerchantController($repository, $view, $app->session, $app->configuration);
        $controller->setMailService($app->mailService);

        return $controller;
    }

    public static function buildDtoRequest(ServerRequestInterface $request)
    {
        $merchant = new Merchant();
        $address = new Address();
        $businessAddress = new Address();

        $address->number = $request->getParam(ParamsName::REGISTER_HOMEADDRNUM, "");
        $address->street = $request->getParam(ParamsName::REGISTER_HOMEADDRSTREET, "");
        $address->appartment = $request->getParam(ParamsName::REGISTER_HOMEADDRAPPART, "");
        $address->town = $request->getParam(ParamsName::REGISTER_HOMEADDRTOWN, "");
        $address->code = $request->getParam(ParamsName::REGISTER_HOMEADDRCODE, "");
        $address->province = $request->getParam(ParamsName::REGISTER_HOMEADDRPROVINCE, "");

        $merchant->accountType = User::ACCOUNT_MERCHANT;
        $merchant->firstName = $request->getParam(ParamsName::REGISTER_FIRSTNAME, "");
        $merchant->lastName = $request->getParam(ParamsName::REGISTER_LASTNAME, "");
        $merchant->phone = $request->getParam(ParamsName::REGISTER_HOMEPHONE, "");
        $merchant->email = $request->getParam(ParamsName::REGISTER_EMAIL, "");
        $merchant->birthdate = $request->getParam(ParamsName::REGISTER_BIRTHDATE, "");
        $merchant->userName = $request->getParam(ParamsName::REGISTER_USERNAME, "");
        $merchant->address = $address;
        $merchant->image = UserFactory::loadUserImage($merchant->id, User::ACCOUNT_MERCHANT);

        $businessAddress->number = $request->getParam(ParamsName::REGISTER_BUSINESSADDRNUM, "");
        $businessAddress->street = $request->getParam(ParamsName::REGISTER_BUSINESSADDRSTREET, "");
        $businessAddress->appartment = $request->getParam(ParamsName::REGISTER_BUSINESSADDRAPPART, "");
        $businessAddress->town = $request->getParam(ParamsName::REGISTER_BUSINESSADDRTOWN, "");
        $businessAddress->code = $request->getParam(ParamsName::REGISTER_BUSINESSADDRCODE, "");
        $businessAddress->province = $request->getParam(ParamsName::REGISTER_BUSINESSADDRPROVINCE, "");

        $merchant->businessName = $request->getParam(ParamsName::REGISTER_BUSINESSNAME, "");
        $merchant->businessEmail = $request->getParam(ParamsName::REGISTER_BUSINESSMAIL, "");
        $merchant->businessPhone = $request->getParam(ParamsName::REGISTER_BUSINESSPHONE, "");
        $merchant->businessAddress = $businessAddress;

        return $merchant;
    }

    public static function buildDtoDatabase(SQLRow $row) 
    {
        $merchant = new Merchant();
        $address = new Address();
        $businessAddress = new Address();

        $address->id = $row->address_id;
        $address->number = $row->address;
        $address->street = $row->street;
        $address->appartment = $row->appartment;
        $address->town = $row->city;
        $address->code = $row->zip_code;
        $address->province = $row->province;

        $merchant->id = $row->user_id;
        $merchant->firstName = $row->firstname;
        $merchant->lastName = $row->lastname;
        $merchant->userName = $row->username;
        $merchant->accountState = $row->status;
        $merchant->accountType = $row->type;
        $merchant->memberDate = $row->creation_ts;
        $merchant->phone = $row->phone;
        $merchant->birthdate = $row->birthdate;
        $merchant->email = $row->email;
        $merchant->address = $address;
        $merchant->merchantId = $row->merchant_id;
        $merchant->image = UserFactory::loadUserImage($row->user_id, User::ACCOUNT_MERCHANT);

        $businessAddress->id = $row->business_address_id;
        $businessAddress->number = $row->business_address;
        $businessAddress->street = $row->business_street;
        $businessAddress->appartment = $row->business_appartment;
        $businessAddress->town = $row->business_city;
        $businessAddress->code = $row->business_zip_code;
        $businessAddress->province = $row->business_province;

        $merchant->businessAddress = $businessAddress;
        $merchant->businessPhone = $row->business_phone;
        $merchant->businessEmail = $row->business_email;
        $merchant->businessName = $row->business_name;

        return $merchant;
    }
   
}
