<?php

namespace App\Merchant;

use App\User\User;
use App\Contact\Address;

//
// Information sur un commercant
//
class Merchant extends User
{   
    public $merchantId = null;
    public $businessPhone = 0;                // numéro téléphone du commerce
    public $businessEmail = "";               // courriel du commerce
    public $businessName = "";                // nom du commerce
    public $businessAddress = null;           // adresse du commerce complete
}