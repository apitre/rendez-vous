<?php

namespace App\Merchant;

use App\Member\MemberController;
use App\View\HTMLView;
use App\Common\Configuration;
use App\Core\RepositoryException;
use App\Core\Session;
use App\Contact\Address;
use App\Merchant\Merchant;
use App\Merchant\MerchantFactory;
use App\Merchant\MerchantRepository;
use App\User\User;
use App\Common\ParamsName;
use App\Common\RoutesName;
use App\Common\TemplatesName;

//
// Controlleur pour le commercant
//
class MerchantController extends MemberController
{
    protected function getAccountType()
    {
        return User::ACCOUNT_MERCHANT;
    }
    
    protected function getHomePageName()
    {
        return TemplatesName::HOME_MERCHANT;
    }    
    
    protected function getRegistrationPageName()
    {
        return TemplatesName::REGISTRATION_MERCHANT;
    }
    
    protected function getConfirmationPageName()
    {
        return TemplatesName::CONFIRMATION_MERCHANT;
    }
    
    protected function getModificationPageName()
    {
        return TemplatesName::MODIFY_PARAMETERS_MERCHANT;
    }
    
    protected function getNewMemberInformation($request)
    {
        $merchant = MerchantFactory::buildDtoRequest($request);
        $merchant->password = $request->getParam(ParamsName::REGISTER_PASSWORD, ''); 
        
        return $merchant;
    }
    
    protected function updateMemberInformation($request, $member)
    {                
        $member->firstName = $request->getParam(ParamsName::REGISTER_FIRSTNAME, $member->firstName);
        $member->lastName = $request->getParam(ParamsName::REGISTER_LASTNAME, $member->lastName);        
        $member->phone = $request->getParam(ParamsName::REGISTER_HOMEPHONE, $member->phone);
        $member->email = $request->getParam(ParamsName::REGISTER_EMAIL, $member->email);
        $member->birthdate = $request->getParam(ParamsName::REGISTER_BIRTHDATE, $member->birthdate);
        $this->updateMemberAddress($request, $member->address);  
        
        $member->businessName = $request->getParam(ParamsName::REGISTER_BUSINESSNAME, $member->businessName);
        $member->businessEmail = $request->getParam(ParamsName::REGISTER_BUSINESSMAIL, $member->businessEmail);
        $member->businessPhone = $request->getParam(ParamsName::REGISTER_BUSINESSPHONE, $member->businessPhone);
        $this->updateMemberBusinessAddress($request, $member->businessAddress);
        
        // Conserver l'information sur le nouveau membre
        return $this->getRepository()->update($member);
    }
    
    protected function updateMemberBusinessAddress($request, Address $address)
    {
        $address->number = $request->getParam(ParamsName::REGISTER_BUSINESSADDRNUM, $address->number);
        $address->street = $request->getParam(ParamsName::REGISTER_BUSINESSADDRSTREET, $address->street);
        $address->appartment = $request->getParam(ParamsName::REGISTER_BUSINESSADDRAPPART, $address->appartment);
        $address->town = $request->getParam(ParamsName::REGISTER_BUSINESSADDRTOWN, $address->town);
        $address->code = $request->getParam(ParamsName::REGISTER_BUSINESSADDRCODE, $address->code);
        $address->province = $request->getParam(ParamsName::REGISTER_BUSINESSADDRPROVINCE, $address->province);
    }
    
    //
    // Afficher la page d'accueil du membre
    //
    public function showHomePage($request, $response) {

        try {
            
            // Indicateur utilisé suivant la creation d'un service
            $added = $request->getParam('service_added');
            $accountType = $this->getAccountType();
            
            $member = $this->getMemberFromSessionsUser();

            $this->data['member'] = $member;
            $this->data['modifyRoute'] = $this->getUserRoute($accountType, RoutesName::USER_MODIFICATION);
            $this->data['createServiceRoute'] = $this->getUserRoute($accountType, RoutesName::SERVICE_CREATION);
            $this->data['servicesListRoute'] = $this->getUserRoute($accountType, RoutesName::SERVICES);
            $this->data['servicesRatingsRoute'] = $this->getUserRoute($accountType, RoutesName::SERVICE_RATINGS);
            $this->data['summaryRoute'] = $this->getUserRoute($accountType, RoutesName::SERVICE_APPOINTMENTS_SUMMARY);
            $this->data['calendarRoute'] = $this->getUserRoute($accountType, RoutesName::CALENDAR);
            $this->data['serviceAdded'] = (int)$added > 0;

            // Afficher la page d'accueil
            return $this->render($this->getHomePageName(), $this->data);
            
        } catch (RepositoryException $e) {
            
            $this->setMsgError($e->getMessage());
            
            // Afficher la page d'accueil
            return $this->render($this->getHomePageName(), $this->data);
            
        }
    }
}