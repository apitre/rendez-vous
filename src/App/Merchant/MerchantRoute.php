<?php

namespace App\Merchant;

use App\Core\Middleware;
use App\Merchant\MerchantFactory;
use App\Common\RoutesName;


$app->get(RoutesName::MERCHANT.RoutesName::USER_HOME, function ($request, $response, $args) {

    // Afficher la page d'accueil du commercant
    $controller = MerchantFactory::buildController($this, $response);
    return $controller->showHomePage($request, $response);	
})->add(Middleware::loginAccountVerify(Merchant::ACCOUNT_MERCHANT));

$app->get(RoutesName::MERCHANT.RoutesName::USER_REGISTRATION, function ($request, $response, $args) {

    // Afficher la page d'enregistrement d'un commercant
    $controller = MerchantFactory::buildController($this, $response);
    return $controller->showRegistrationPage($request);	
});

$app->post(RoutesName::MERCHANT.RoutesName::USER_REGISTER, function ($request, $response, $args) {
    
    // Enregistrer le commercant sur le site
    $controller = MerchantFactory::buildController($this, $response);
    return $controller->register($request, $response);
});

$app->get(RoutesName::MERCHANT.RoutesName::USER_CONFIRMATION, function ($request, $response, $args) {
    
    // Afficher la confirmation de l'enregistrement du commercant
    $controller = MerchantFactory::buildController($this, $response);
    return $controller->showRegistrationConfirmation($request, $response);	
});

$app->get(RoutesName::MERCHANT.RoutesName::USER_MODIFICATION, function ($request, $response) { 
  
    // Afficher la page de modification des paramètres d'un commercant
    $controller = MerchantFactory::buildController($this, $response);
    return $controller->showModificationPage($request, $response);
})->add(Middleware::loginAccountVerify(Merchant::ACCOUNT_MERCHANT));

$app->post(RoutesName::MERCHANT.RoutesName::USER_MODIFY, function ($request, $response, $args) {
   
    // Mise a jour des paramètres d'un commercant
    $controller = MerchantFactory::buildController($this, $response);
    return $controller->updateParameters($request, $response);
})->add(Middleware::loginAccountVerify(Merchant::ACCOUNT_MERCHANT));