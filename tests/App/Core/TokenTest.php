<?php

namespace App\Core;

use \App\Core\Token;

class TokenTest extends \PHPUnit_Framework_TestCase
{
	const SALT = 'ee91eb0.55177230';
	const PASSWORD = '123456';

	public function testSalt()
	{
		$salt1 = Token::salt();

		$salt2 = Token::salt();

		$notEgual = $salt1 != $salt2;

		$sameLenght = strlen($salt1) == strlen($salt2);

		$notEmpty = !empty($salt1) && !empty($salt2);

		$this->assertEquals(true, $notEgual && $sameLenght && $notEmpty);
	}

    public function testAssembly()
    {
        $this->assertEquals("ee91eb0.12345655177230", Token::assembly(self::SALT, self::PASSWORD));
    }

    public function testValidate()
    {
    	$token = '$2y$10$B5bNb5V1o7XbWtqKuWqc5uTGkUoCkt9eu6Tpz6ds2z6tTKC6iHBjC';

        $this->assertEquals(true, Token::validate(self::SALT, self::PASSWORD, $token));
    }

    public function testHash()
    {	
    	$token = Token::hash(self::SALT, self::PASSWORD);

        $this->assertEquals(true, Token::validate(self::SALT, self::PASSWORD, $token));
    }
}