<?php

namespace App\User;

use Slim\Http\Response;

use App\Lib\UnitTestFactory;
use App\User\UserController;
use App\View\MockView;
use App\Core\Session;
use App\Core\RepositoryException;
use App\Core\SQLDatabase;
use App\Common\Configuration;

class UserControllerTest extends \PHPUnit_Framework_TestCase
{
	private $controller = null;
    private $factory = null;
    private $repo = null;
    private $response = null;
    private $db = null;
    private $credential = [
        'username'  => "alain",
        'password'  => "123456"
    ];

	public function __construct()
    {	
    	$this->factory = new UnitTestFactory();

    	$dic = $this->factory->buildDic();

        $this->db = $dic->db;

        $this->response = new Response();

        $view = new MockView($dic->view, $this->response);

        $methods = ['getInfoFromName', 'getAccess'];

        $this->repo = $this->getMock('\App\User\UserRepository', $methods, [$this->db]);

        $this->controller = new UserController($this->repo, $view, $dic->session, $dic->configuration);
    }

    public function testSigninSuccess()
    { 
        $request = $this->factory->request("/", $this->credential);

        $user = $this->factory->getUser();

        $access = $this->factory->getAccess();

        $this->repo
                ->expects($this->any())
                ->method('getInfoFromName')
                ->willReturn($user);

        $this->repo
                ->expects($this->any())
                ->method('getAccess')
                ->willReturn($access);

        $responseA = $this->controller->signin($request, $this->response);

        $user->accountType = 2;

        $responseB = $this->controller->signin($request, $this->response);

        $this->assertEquals("/consumer/home/", $responseA->getHeaders()['Location'][0]);
        $this->assertEquals("/merchant/home/", $responseB->getHeaders()['Location'][0]);

    }

    public function testInvalidUserInstance()
    { 
        $request = $this->factory->request("/", $this->credential);

        $this->repo
                ->expects($this->any())
                ->method('getInfoFromName')
                ->willReturn(null);

        $response = $this->controller->signin($request, $this->response);

        $this->assertEquals(RepositoryException::EX_4, $response['data']['errorMessage']);
        $this->assertEquals("login.html", $response['template']);
    }

    public function testInvalidTokenActivation()
    { 
        $request = $this->factory->request("/activate/", [
            'token' => "123456"
        ]);

        $this->db
                ->expects($this->once())
                ->method('query')
                ->willReturn([]);

        $response = $this->controller->activate($request, $this->response);

        $this->assertEquals(RepositoryException::EX_6, $response['data']['errorMessage']);
        $this->assertEquals("activate.html", $response['template']);
    }

    public function testSignOut()
    { 
        $request = $this->factory->request("/signout/", $this->credential);

        $response = $this->controller->signout($request, $this->response);

        $this->assertEquals('/', $response->getHeaders()['Location'][0]);
    }

}